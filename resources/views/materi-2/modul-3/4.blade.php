@extends('layouts.layout')

@section('content')
<section class="ftco-section bg-white">
    <div class="container">
        <br>
        <div class="row  mb-3 pb-md-3">
            <div class="col-md-12 heading-section ftco-animate">
                    <img src={{ asset("images/open-book.png") }} height="100px" style="margin: 40px" />
                    <img src={{ asset("images/server_sq.png") }} height="140px" />
                    @include('layouts.modul-breadcrumbs')
                <!-- <span class="subheading">Materi 1</span> -->
                <h2 class="mb-4">Modul 3 - Pembuatan list minimal</h2>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 ftco-animate">
                @include('materi-'.request()->segment(2).'.modul-'.request()->segment(3).'.sidebar')
            </div>
                <div class="col-md-9 ftco-animate">
                    <h2 style="font-weight: 700" class="mb-4 text-center">List Kombinasi</h2>
                    <p>Perhatikan daftar berikut ini :</p>
                    <p>
                        <ol>
                            <li>Makanan</li>
                            <ul>
                                <li>Bakso</li>
                                <li>Soto</li>
                            </ul>
                            <li>Minuman</li>
                            <ul>
                                <li>Es Jeruk</li>
                                <li>Kopi</li>
                            </ul>
                        </ol>
                    </p>
                    <p class="text-justify">Dapat dilihat bahwa daftar di atas adalah gabungan daftar berurutan dan daftar tidak berurutan. Untuk membuat daftar diatas pada dokumen HTML maka cukup menggabungkan &lt;ol&gt; dangan &lt;ul&gt;.  Perhatikan penggabungan &lt;ol&gt; dengan &lt;ul&gt; berikut ini :</p>
                    <div class="highlight">
                        <xmp><ol>
<li>Makanan</li>
    <ul>
        <li>Bakso</li>
        <li>Soto</li>
    </ul>
<li>Minuman</li>
    <ul>
        <li>Es Jeruk</li>
        <li>Kopi</li>
    </ul>
</ol></xmp>
                    </div>
                <!-- <div class="highlight">
                    <xmp><table>
<tr>
    <th>Month</th>
    <th>Savings</th>
</tr>
<tr>
    <td>January</td>
    <td>$100</td>
</tr>
</table></xmp>
                </div> -->
                
                <br>
                <br>
                <div class="row ftco-animate">
                    <div class="col-md-3"></div>
                    <div class="col-md-3">
                        <center><p><a href={{ url("materi/2/3/3") }} class="btn btn-primary py-3">< Sebelumnya</a></p></center>
                    </div>
                    <div class="col-md-3">
                        <center><p><a href={{ url("materi/status/2/3/".((int)request()->segment(4)+1)) }} class="btn btn-primary py-3">Selanjutnya ></a></p></center>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection