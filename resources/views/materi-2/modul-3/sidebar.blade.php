<div class="list-group">
    <h3 style="font-weight: 600">Tahapan Modul</h3>
    @php $count = 7 @endphp
    <a href={{ url("materi/2/3/1") }} class="list-group-item list-group-item-action {{ request()->segment(4) == '1' ? 'active' : '' }}">Pengertian List</a>
    @php $count++ @endphp
    <a href={{ url("materi/2/3/2") }} class="list-group-item list-group-item-action {{ request()->segment(4) == '2' ? 'active' : '' }} {{ $status->materi2 < $count ? 'disabled bg-light' : '' }}">Daftar Berurutan</a>
    @php $count++ @endphp
    <a href={{ url("materi/2/3/3") }} class="list-group-item list-group-item-action {{ request()->segment(4) == '3' ? 'active' : '' }} {{ $status->materi2 < $count ? 'disabled bg-light' : '' }}">Daftar Tidak Berurutan</a>
    @php $count++ @endphp
    <a href={{ url("materi/2/3/4") }} class="list-group-item list-group-item-action {{ request()->segment(4) == '4' ? 'active' : '' }} {{ $status->materi2 < $count ? 'disabled bg-light' : '' }}">List Kombinasi</a>
    {{-- @php $count++ @endphp --}}
    <!-- <a href="materi-1-modul-1-latihan.html" class="list-group-item list-group-item-action"><b>Latihan</b></a> -->
    {{-- <a href={{ url("materi/2/3/latihan") }} class="list-group-item list-group-item-action {{ request()->segment(4) == 'latihan' ? 'active' : '' }} {{ $status->materi2 < $count ? 'disabled bg-light' : '' }}"><b>Latihan</b></a> --}}
    @php $count++ @endphp
    <a href={{ url("materi/2/3/kuis") }} class="list-group-item list-group-item-action {{ request()->segment(4) == 'kuis' ? 'active' : '' }} {{ $status->materi2 < $count ? 'disabled bg-light' : '' }}"><b>Kuis</b></a>
</div>
