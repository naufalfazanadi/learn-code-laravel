@extends('layouts.layout')

@section('content')
<section class="ftco-section bg-white">
    <div class="container">
        <br>
        <div class="row  mb-3 pb-md-3">
            <div class="col-md-12 heading-section ftco-animate">
                    <img src={{ asset("images/open-book.png") }} height="100px" style="margin: 40px" />
                    <img src={{ asset("images/article_sq.png") }} height="140px" />
                    @include('layouts.modul-breadcrumbs')
                <!-- <span class="subheading">Materi 1</span> -->
                <h2 class="mb-4">Modul 3 - Pembuatan list minimal</h2>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 ftco-animate">
                @include('materi-'.request()->segment(2).'.modul-'.request()->segment(3).'.sidebar')
            </div>
            <div class="col-md-9 ftco-animate">
                @if ($kuis == "")
                    <form action="/kuis" method="POST">
                        {{ csrf_field() }}
                        <h2 style="font-weight: 700" class="mb-4 text-center">Kuis</h2>
                        @if (session('status_repeat'))
                            <div class="alert alert-info">
                                {{ session('status_repeat') }}
                            </div>
                        @endif
                        <p>1. Perintah untuk memberikan list a, b, c secara berurutan adalah . . .</p>
                        <div class="highlight">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no1" id="1a" value="a" required>
                                <label class="form-check-label" for="1a">
                                    a. &lt;ul type = “A”&gt;
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no1" id="1b" value="b">
                                <label class="form-check-label" for="1b">
                                    b. &lt;ul type = “a”&gt;
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no1" id="1c" value="c">
                                <label class="form-check-label" for="1c">
                                    c. &lt;ol type = “A”&gt;
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no1" id="1d" value="d">
                                <label class="form-check-label" for="1d">
                                    d. &lt;ol type = “a”&gt;
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no1" id="1e" value="e">
                                <label class="form-check-label" for="1e">
                                    e. &lt;ul type = “1”&gt;
                                </label>
                            </div>
                        </div>
                        <br>
                        <p>2. Tag &lt;ol&gt; memiliki fungsi untuk . . .</p>
                        <div class="highlight">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no2" id="2a" value="a" required>
                                <label class="form-check-label" for="2a">
                                    a. Membuat list berurutan
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no2" id="2b" value="b">
                                <label class="form-check-label" for="2b">
                                    b. Membuat list tidak berurutan
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no2" id="2c" value="c">
                                <label class="form-check-label" for="2c">
                                    c. Membuat list baris
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no2" id="2d" value="d">
                                <label class="form-check-label" for="2d">
                                    d. Mengganti baris
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no2" id="2e" value="e">
                                <label class="form-check-label" for="2e">
                                    e. Membuat baris
                                </label>
                            </div>
                        </div>
                        <br>
                        <p>3. Struktur yang benar dalam membuat listing unordered adalah . . .</p>
                        <div class="highlight">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no3" id="3a" value="a" required>
                                <label class="form-check-label" for="3a">
                                    a. &lt;ul&gt;<br>&emsp;&emsp;&emsp;&lt;li&gt;&lt;/li&gt;<br>&lt;ul&gt;
                                    
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no3" id="3b" value="b">
                                <label class="form-check-label" for="3b">
                                    b. &lt;ul&gt;<br>&emsp;&emsp;&emsp;&lt;li&gt;&lt;/li&gt;<br>&lt;/ul&gt;
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no3" id="3c" value="c">
                                <label class="form-check-label" for="3c">
                                    c. &lt;ul&gt;<br>&emsp;&emsp;&emsp;&lt;li&gt;&lt;li&gt;<br>&lt;ul&gt;
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no3" id="3d" value="d">
                                <label class="form-check-label" for="3d">
                                    d. &lt;ol&gt;<br>&emsp;&emsp;&emsp;&lt;li&gt;&lt;/li&gt;<br>&lt;ol&gt;
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no3" id="3e" value="e">
                                <label class="form-check-label" for="3e">
                                    e. &lt;ol&gt;<br>&emsp;&emsp;&emsp;&lt;li&gt;&lt;li&gt;<br>&lt;/ol&gt;
                                </label>
                            </div>
                        </div>
                        <br>
                        <p>4. Tag &lt;ul&gt; memiliki fungsi sebagai berikut . . .</p>
                        <div class="highlight">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no4" id="4a" value="a" required>
                                <label class="form-check-label" for="4a">
                                    a.	Membuat daftar tanpa nomor urut
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no4" id="4b" value="b">
                                <label class="form-check-label" for="4b">
                                    b.	Membuat daftar nomor urut
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no4" id="4c" value="c">
                                <label class="form-check-label" for="4c">
                                    c.	Mendefinisikan awal dan akhir list
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no4" id="4d" value="d">
                                <label class="form-check-label" for="4d">
                                    d.	Mengganti paragraf
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no4" id="4e" value="e">
                                <label class="form-check-label" for="4e">
                                    e.	Mengatur paragraph
                                </label>
                            </div>
                        </div>
                        <br>
                        <p>5. Perhatikan kode di bawah ini! . . .</p>
                        <div class="highlight">
                            <xmp><ol type = ‘a’>
    <li>RPL</li>
    <li>TKJ</li>
</ol></xmp>                              
                        </div>
                        <p>Keluaran yang benar dari kode tersebut adalah . . .</p>
                        <div class="highlight">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no5" id="5a" value="a" required>
                                <label class="form-check-label" for="5a">
                                    a.	a.RPL<br>&emsp;b.TKJ
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no5" id="5b" value="b">
                                <label class="form-check-label" for="5b">
                                    b.	a.RPL<br>&emsp;a.TKJ
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no5" id="5c" value="c">
                                <label class="form-check-label" for="5c">
                                    c.	A.RPL<br>&emsp;B.TKJ
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no5" id="5d" value="d">
                                <label class="form-check-label" for="5d">
                                    d.	A.RPL<br>&emsp;A.TKJ
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no5" id="5e" value="e">
                                <label class="form-check-label" for="5e">
                                    e.	A.RPL<br>&emsp;b.TKJ                                        
                                </label>
                            </div>
                        </div>
                        <br>
                        <br>
                        <div class="row ftco-animate">
                            <div class="col-md-3"></div>
                            <div class="col-md-3">
                                <center><p><a href={{ url("materi/2/3/4") }} class="btn btn-primary py-3">< Sebelumnya</a></p></center>
                                <!-- <center><a href='plasma-flame.html' class="btn btn-primary btn-lg"><< Sebelumnya</a></center> -->
                            </div>
                            <div class="col-md-3">
                                <input type="hidden" id="id" name="jumlahSoal" value="5">
                                <input type="hidden" id="id" name="materi" value={{ request()->segment(2) }}>
                                <input type="hidden" id="id" name="modul" value={{ request()->segment(3) }}>

                                <center><input type="submit" value="Kirim Jawaban" class="btn btn-success py-3 px-5"></center>
                                <!-- <center><a href='persiapan-alat.html' class="btn btn-primary btn-lg">Materi Selanjutnya ></a></center> -->
                            </div>
                            <div class="col-md-3"></div>
                        </div>
                    </form>
                @else
                    <div class="alert alert-success">
                        <h3 class="text-center">Kamu telah menyelesaikan kuis</h2>
                        <h2 style="font-weight: 700" class="mb-4 text-center">Nilai kamu : <b>{{ $kuis->nilai }}</b></h3>
                    </div>
                    <br>
                    <br>
                    <div class="row ftco-animate">
                        <div class="col-md-3"></div>
                        <div class="col-md-3">
                            <center><p><a href={{ url("materi/2/3/latihan") }} class="btn btn-primary py-3">< Sebelumnya</a></p></center>
                        </div>
                        <div class="col-md-3">
                            <center><p><a href={{ url("materi/2") }} class="btn btn-success py-3">Daftar modul</a></p></center>
                        </div>
                        <div class="col-md-3"></div>
                    </div>
                @endif
            </div>
        </div>
    </div>
</section>
@endsection