@extends('layouts.layout')

@section('content')
<section class="ftco-section bg-white">
    <div class="container">
        <br>
        <div class="row  mb-3 pb-md-3">
            <div class="col-md-12 heading-section ftco-animate">
                    <img src={{ asset("images/open-book.png") }} height="100px" style="margin: 40px" />
                    <img src={{ asset("images/article_sq.png") }} height="140px" />
                    @include('layouts.modul-breadcrumbs')
                <!-- <span class="subheading">Materi 1</span> -->
                <h2 class="mb-4">Modul 3 - Pembuatan list minimal</h2>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 ftco-animate">
                @include('materi-'.request()->segment(2).'.modul-'.request()->segment(3).'.sidebar')
            </div>
            <div class="col-md-9 ftco-animate">
                    <h2 style="font-weight: 700" class="mb-4 text-center">Daftar Berurutan</h2>
                    <!-- <p>Tag HTML untuk paragrap adalah &lt;p&gt;, dengan tag penutup &lt;/p&gt;. Berikut ini contohnya:</p> -->
                    <p class="text-justify">Daftar yang berurutan biasanya ditandai dengan penggunaan penomoran tertentu. Penomoran ini bias menggunakan angka (1, 2, 3, …)  maupun karakter alphabet terntentu (a, b, c, …  atau i, ii, iii, …).  Daftar yang berurutan umumnya dipakai untuk item-item yang saling berhubungan satu sama lain, atau untuk menuliskan langkah-langkah atau prosedur dari kegiatan terntentu yang tidak dapat dilakukan secara acak.</p>
                    <p class="text-justify">Untuk membuat suatu daftar yang berurutan dalam dokumen HTML, digunakan tag &lt;ol&gt; (ordered list), yang berpasangan dengan tag &lt;/ol&gt;. Tag &lt;ol&gt; digunakan untuk memulai suatu daftar berurutan tertentu, sedangkan &lt;/ol&gt; berfungsi untuk menandakan bahwa daftar tersebut sudah berakhir. Masing-masing item di dalam daftar harus dibuat menggunakan tag &lt;li&gt; (list item, yang kemudian perlu ditutup dengan tag &lt;/li&gt;.</p>
                    <p>Contoh penulisan ordered list :</p>
                    <div class="highlight">
                        <xmp><ol>
    <li>Urutan Pertama</li>
    <li>Urutan Kedua</li>
</ol></xmp>
                    </div>
                    <br>
                    <p>Adapun atribut untuk tag &lt;ol&gt; adalah "type" yang menunjukan jenis penomorannnya.</p>
                    <p>
                        <ul>
                            <li>Type A (Penomoran berupa karakter A, B, C, dst)</li>
                            <li>Type a (Penomoran berupa karakter a, b, c, dst)</li>
                            <li>Type I (Penomoran berupa karakter I, II, III, dst)</li>
                            <li>Type i (Penomoran berupa karakter i, ii, iii, dst)</li>
                        </ul>
                    </p>                <!-- <div class="highlight">
                    <xmp><table>
<tr>
    <th>Month</th>
    <th>Savings</th>
</tr>
<tr>
    <td>January</td>
    <td>$100</td>
</tr>
</table></xmp>
                </div> -->
                
                <br>
                <br>
                <div class="row ftco-animate">
                    <div class="col-md-3"></div>
                    <div class="col-md-3">
                        <center><p><a href={{ url("materi/2/3/1") }} class="btn btn-primary py-3">< Sebelumnya</a></p></center>
                    </div>
                    <div class="col-md-3">
                        <center><p><a href={{ url("materi/status/2/3/".((int)request()->segment(4)+1)) }} class="btn btn-primary py-3">Selanjutnya ></a></p></center>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection