@extends('layouts.layout')

@section('content')
<section class="ftco-section bg-white">
    <div class="container">
        <br>
        <div class="row  mb-3 pb-md-3">
            <div class="col-md-12 heading-section ftco-animate">
                    <img src={{ asset("images/open-book.png") }} height="100px" style="margin: 40px" />
                    <img src={{ asset("images/article_sq.png") }} height="140px" />
                    @include('layouts.modul-breadcrumbs')
                <!-- <span class="subheading">Materi 1</span> -->
                <h2 class="mb-4">Modul 3 - Pembuatan list minimal</h2>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 ftco-animate">
                @include('materi-'.request()->segment(2).'.modul-'.request()->segment(3).'.sidebar')
            </div>
            <div class="col-md-9 ftco-animate">
                    <h2 style="font-weight: 700" class="mb-4 text-center">Daftar Tidak Berurutan</h2>
                    <p class="text-justify">Daftar yang tidak berurutan adalah daftar yang item-itemnya dapat diubah posisinya secara acak. Daftar seperti ini tidak menggunakan penomoran seperti di atas, melainkan hanya dengan menggunakan tanda atau symbol tertentu, misalnya gambar kotak atau bulat (bullet).</p>
                    <p class="text-justify">Untuk membuat suatu daftar yang tidak berurutan dalam dokumen HTML, digunakan tag &lt;ul&gt; (unordered list), yang berpasangan dengan tag &lt;/ul&gt;. Tag &lt;ul&gt; digunakan untuk memulai suatu daftar berurutan tertentu, sedangkan &lt;/ul&gt; berfungsi untuk menandakan bahwa daftar tersebut sudah berakhir. Masing-masing item di dalam daftar harus dibuat menggunakan tag &lt;ul&gt; (list item, yang kemudian perlu ditutup dengan tag &lt;/ul&gt;. </p>
                    <p>Contoh penulisan unordered list :</p>
                    <div class="highlight">
                        <xmp><ul>
    <li>Item Pertama</li>
    <li>Item Kedua</li>
</ul></xmp>
                    </div>
                    <br>
                    <p>Adapun atribut untuk tag &lt;ul&gt; adalah "type" yang menunjukan jenis penomorannnya.</p>
                    <p>
                        <ul>
                            <li>Type Disk (Tanda bulatan hitam)</li>
                            <li>Type Circle (Tanda bulatan putih)</li>
                            <li>Type Square (Tanda kotak)</li>
                        </ul>
                    </p>
                <!-- <div class="highlight">
                    <xmp><table>
<tr>
    <th>Month</th>
    <th>Savings</th>
</tr>
<tr>
    <td>January</td>
    <td>$100</td>
</tr>
</table></xmp>
                </div> -->
                
                <br>
                <br>
                <div class="row ftco-animate">
                    <div class="col-md-3"></div>
                    <div class="col-md-3">
                        <center><p><a href={{ url("materi/2/3/2") }} class="btn btn-primary py-3">< Sebelumnya</a></p></center>
                    </div>
                    <div class="col-md-3">
                        <center><p><a href={{ url("materi/status/2/3/".((int)request()->segment(4)+1)) }} class="btn btn-primary py-3">Selanjutnya ></a></p></center>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection