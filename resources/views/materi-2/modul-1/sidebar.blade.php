<div class="list-group">
    <h3 style="font-weight: 600">Tahapan Modul</h3>
    @php $count = 0 @endphp
    <a href={{ url("materi/2/1/1") }} class="list-group-item list-group-item-action {{ request()->segment(4) == '1' ? 'active' : '' }}">Dasar-Dasar HTML</a>
    @php $count++ @endphp
    <a href={{ url("materi/2/1/2") }} class="list-group-item list-group-item-action {{ request()->segment(4) == '2' ? 'active' : '' }} {{ $status->materi2 < $count ? 'disabled bg-light' : '' }}">Struktur Dasar Dokumen HTML</a>
    @php $count++ @endphp
    <a href={{ url("materi/2/1/3") }} class="list-group-item list-group-item-action {{ request()->segment(4) == '3' ? 'active' : '' }} {{ $status->materi2 < $count ? 'disabled bg-light' : '' }}">Pengaturan Properti Dokumen</a>
    {{-- @php $count++ @endphp
    <a href={{ url("materi/2/1/latihan") }} class="list-group-item list-group-item-action {{ request()->segment(4) == 'latihan' ? 'active' : '' }} {{ $status->materi2 < $count ? 'disabled bg-light' : '' }}"><b>Latihan</b></a> --}}
    @php $count++ @endphp
    <a href={{ url("materi/2/1/kuis") }} class="list-group-item list-group-item-action {{ request()->segment(4) == 'kuis' ? 'active' : '' }} {{ $status->materi2 < $count ? 'disabled bg-light' : '' }}"><b>Kuis</b></a>
</div>
