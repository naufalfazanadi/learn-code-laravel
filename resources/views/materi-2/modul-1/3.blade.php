@extends('layouts.layout')

@section('content')
<section class="ftco-section bg-white">
    <div class="container">
        <br>
        <div class="row  mb-3 pb-md-3">
            <div class="col-md-12 heading-section ftco-animate">
                <img src={{ asset("images/open-book.png") }} height="100px" style="margin: 40px" />
                <img src={{ asset("images/article_sq.png") }} height="140px" />
                @include('layouts.modul-breadcrumbs')
                <!-- <span class="subheading">Materi 1</span> -->
                <h2 class="mb-4">Modul 1 - Anatomi Dokumen Web</h2>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 ftco-animate">
                @include('materi-'.request()->segment(2).'.modul-'.request()->segment(3).'.sidebar')
            </div>
                <div class="col-md-9 ftco-animate">
                    <h2 style="font-weight: 700" class="mb-4 text-center">Pengaturan Properti Dokumen</h2>
                    <p class="text-justify">Properti document diatur melalui atribut-atribut yang terdapat dalam elemen &lt;body&gt;. Sebagai contoh adalah pengaturan warna latar belakng halaman, wana teks, warna link dan lain-lain.</p>
                    <p><b>Kode Warna</b></p>
                    <p class="text-justify">Dalam pengaturan warna menggunakan kode RGB yang mana ditampilkan dalam nilai heksadesimal. Setiap bagian dua gigit kode menunjukkan banyaknya intensitas dari kombinasi warna merah, hijau dan biru.</p>
                    <center><img src={{ asset("images/color.png") }} alt="" width="800px" class="img-responsive"></center><br>
                    <p><b> Atribut elemen &lt;body&gt; </b></p>
                    <p>
                        <ul>
                            <li>Background (memasukan gambar)</li>
                            <div class="highlight">
                                <xmp><body background="home.jpg"></body></xmp>
                            </div>
                            <li>Bgcolor (memberi warna pada background)</li>
                            <div class="highlight">
                                <xmp><body bgcolor="#ecf0f1"></body></xmp>
                            </div>
                            <li>Text (memberi warna pada text)</li>
                            <div class="highlight">
                                <xmp><body text="#ecf0f1"></body></xmp>
                            </div>
                            <li>Link (memberi warna pada teks link)</li>
                            <div class="highlight">
                                <xmp><body link="#ecf0f1"></body></xmp>
                            </div>
                            <li>Vlink (memberi warna ketika mengklik link)</li> 
                            <div class="highlight">
                                <xmp><body vlink="#ecf0f1"></body></xmp>
                            </div>
                            <li>Alink (memberi warna ketika mengklik link)</li>
                            <div class="highlight">
                                <xmp><body alink="#ecf0f1"></body></xmp>
                            </div>
                        </ul>
                    </p>
                <!-- <div class="highlight">
                    <xmp><table>
<tr>
    <th>Month</th>
    <th>Savings</th>
</tr>
<tr>
    <td>January</td>
    <td>$100</td>
</tr>
</table></xmp>
                </div> -->
                
                <br>
                <br>
                <div class="row ftco-animate">
                    <div class="col-md-3"></div>
                    <div class="col-md-3">
                        <center><p><a href={{ url("materi/2/1/2") }} class="btn btn-primary py-3">< Sebelumnya</a></p></center>
                    </div>
                    <div class="col-md-3">
                        <center><p><a href={{ url("materi/status/2/1/".((int)request()->segment(4)+1)) }} class="btn btn-primary py-3">Selanjutnya ></a></p></center>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection