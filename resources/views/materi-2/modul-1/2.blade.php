@extends('layouts.layout')

@section('content')
<section class="ftco-section bg-white">
    <div class="container">
        <br>
        <div class="row  mb-3 pb-md-3">
            <div class="col-md-12 heading-section ftco-animate">
                <img src={{ asset("images/open-book.png") }} height="100px" style="margin: 40px" />
                <img src={{ asset("images/article_sq.png") }} height="140px" />
                @include('layouts.modul-breadcrumbs')
                <!-- <span class="subheading">Materi 1</span> -->
                <h2 class="mb-4">Modul 1 - Anatomi Dokumen Web</h2>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 ftco-animate">
                @include('materi-'.request()->segment(2).'.modul-'.request()->segment(3).'.sidebar')
            </div>
                <div class="col-md-9 ftco-animate">
                    <h2 style="font-weight: 700" class="mb-4 text-center">Struktur Dasar Dokumen HTML</h2>
                    <p class="text-justify">Setiap dokumen HTML memiliki struktur dasar atau susunan file sebagai berikut :</p>
                    <div class="highlight">
                        <xmp><html>      
    <head>            
        <title>Judul Halaman</title>      
    </head>      
    <body>           
        Berisi tentang text, gambar atau apapun yang tampil pada dokumen web.
    </body>
</html></xmp>
                    </div>
                    <br>
                    <p class="text-justify">Struktur file HTML diawali dengan sebuah tag &lt;html&gt; dan ditutup dengan tag &lt;/html&gt;. Di dalam tag ini terdapat dua buah bagian besar yaitu yang diapit oleh tag &lt;head&gt;…&lt;/head&gt; dan tag &lt;body&gt;…&lt;/body&gt;. Bagian yang diapit oleh tag HEAD merupakan header dari halaman HTML dan tidak ditampilkan pada browser. Bagian ini berisi tag-tag header seperti &lt;title&gt;…&lt;/title&gt; yang berfungsi untuk mengeluarkan judul pada tittle bar window web browser.</p>
                    <p class="text-justify">Bagian kedua, yang diapit oleh tag &lt;body&gt; merupakan bagian yang akan ditampilkan pada halaman web browser nantinya. Pada bagian ini anada akan menuliskan semua jenis informasi berupa teks dengan bermacam format maupun gambar yang ingin sampaikan pada pengguna nantinya.</p>
                <!-- <div class="highlight">
                    <xmp><table>
<tr>
    <th>Month</th>
    <th>Savings</th>
</tr>
<tr>
    <td>January</td>
    <td>$100</td>
</tr>
</table></xmp>
                </div> -->
                
                <br>
                <br>
                <div class="row ftco-animate">
                    <div class="col-md-3"></div>
                    <div class="col-md-3">
                        <center><p><a href={{ url("materi/2/1/1") }} class="btn btn-primary py-3">< Sebelumnya</a></p></center>
                    </div>
                    <div class="col-md-3">
                        <center><p><a href={{ url("materi/status/2/1/".((int)request()->segment(4)+1)) }} class="btn btn-primary py-3">Selanjutnya ></a></p></center>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection