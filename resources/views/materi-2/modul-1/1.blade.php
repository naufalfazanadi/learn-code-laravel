@extends('layouts.layout')

@section('content')
<section class="ftco-section bg-white">
    <div class="container">
        <br>
        <div class="row  mb-3 pb-md-3">
            <div class="col-md-12 heading-section ftco-animate">
                <img src={{ asset("images/open-book.png") }} height="100px" style="margin: 40px" />
                <img src={{ asset("images/article_sq.png") }} height="140px" />
                @include('layouts.modul-breadcrumbs')
                <!-- <span class="subheading">Materi 1</span> -->
                <h2 class="mb-4">Modul 1 - Anatomi Dokumen Web</h2>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 ftco-animate">
                @include('materi-'.request()->segment(2).'.modul-'.request()->segment(3).'.sidebar')
            </div>
                <div class="col-md-9 ftco-animate">
                    <h2 style="font-weight: 700" class="mb-4 text-center">Dasar-Dasar HTML</h2>

                    <p class="text-justify">HTTP (Hypertext transfer protocol) merupakan protokol yang digunakan untuk mentransfer data antara web server ke web browser. Protocol ini mentransfer dokumen-dokumen web yang ditulis atau berformat HTML (Hypertext Markup Language). Dikatakan markup language karena HTML berfungsi untuk memperindah file teks biasa untuk ditampilkan pada program web browser hal ini dilakukan dengan menambahkan elemen atau sering disebut sebagai tag-tag pada file teks biasa tersebut.</p>
                    <p class="text-justify">
                        Secara prinsip, fitur pada HTML dapat dikelompokkan ke dalam :
                        <ul>
                            <li>Struktur halaman</li>
                            <li>Presentasi visual</li>
                            <li>Peranti penyaji gambar</li>
                            <li>Pendukung media</li>
                            <li>Peningkatan koneksi dengan JavaScript</li>
                        </ul> 

                        Tag HTML biasanya berupa tag-tag yang berpasangan dan ditandai dengan symbol lebih besar (&lt;) dan (&gt;). Pasangan dari sebuah tag ditandai dengan symbol garis miring (/). Misalnya pasangan dari tag &lt;contoh&gt; adalah &lt;/contoh&gt;. Dalam hal ini &lt;contoh&gt; kita sebut sebagai elemen dan biasanya dalam suatu elemen terdapat atribut atribut untuk mengatur elemen itu. Jadi misalnya elemen &lt;contoh&gt; bila ditulis dengan atributnya adalah sebagai berikut :  
                    </p>
                    <div class="highlight">
                        <xmp><contoh atribut1 = "nilai_atribut1" . . .></contoh></xmp>
                    </div>
                    <br>
                    <p>Dalam penulisan tag HTML tidaklah case sensitive artinya pengguna huruf kecil ataupun kapital tidaklah menjadi masalah.</p>
                <!-- <div class="highlight">
                    <xmp><table>
<tr>
    <th>Month</th>
    <th>Savings</th>
</tr>
<tr>
    <td>January</td>
    <td>$100</td>
</tr>
</table></xmp>
                </div> -->
                
                <br>
                <br>
                <div class="row ftco-animate">
                    <!-- <div class="col-md-3"></div> -->
                    <!-- <div class="col-md-3"> -->
                        <!-- <center><p><a href="materi.html" class="btn btn-primary py-3">< Daftar materi</a></p></center> -->
                        <!-- <center><a href='plasma-flame.html' class="btn btn-primary btn-lg"><< Sebelumnya</a></center> -->
                    <!-- </div> -->
                    <div class="col-md-12">
                        <center><p><a href={{ url("materi/status/2/1/".((int)request()->segment(4)+1)) }} class="btn btn-primary py-3">Selanjutnya ></a></p></center>
                        <!-- <center><a href='persiapan-alat.html' class="btn btn-primary btn-lg">Materi Selanjutnya ></a></center> -->
                    </div>
                    <div class="col-md-3"></div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection