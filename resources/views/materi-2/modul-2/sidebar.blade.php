<div class="list-group">
    <h3 style="font-weight: 600">Tahapan Modul</h3>
    @php $count = 4 @endphp
    <a href={{ url("materi/2/2/1") }} class="list-group-item list-group-item-action {{ request()->segment(4) == '1' ? 'active' : '' }}">Pemformatan Teks</a>
    @php $count++ @endphp
    <a href={{ url("materi/2/2/2") }} class="list-group-item list-group-item-action {{ request()->segment(4) == '2' ? 'active' : '' }} {{ $status->materi2 < $count ? 'disabled bg-light' : '' }}">Pemformatan Paragraf</a>    
    {{-- @php $count++ @endphp --}}
    <!-- <a href="materi-1-modul-1-latihan.html" class="list-group-item list-group-item-action"><b>Latihan</b></a> -->
    {{-- <a href={{ url("materi/2/2/latihan") }} class="list-group-item list-group-item-action {{ request()->segment(4) == 'latihan' ? 'active' : '' }} {{ $status->materi2 < $count ? 'disabled bg-light' : '' }}"><b>Latihan</b></a> --}}
    @php $count++ @endphp
    <a href={{ url("materi/2/2/kuis") }} class="list-group-item list-group-item-action {{ request()->segment(4) == 'kuis' ? 'active' : '' }} {{ $status->materi2 < $count ? 'disabled bg-light' : '' }}"><b>Kuis</b></a>
</div>
