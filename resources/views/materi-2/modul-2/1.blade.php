@extends('layouts.layout')

@section('content')
<section class="ftco-section bg-white">
    <div class="container">
        <br>
        <div class="row  mb-3 pb-md-3">
            <div class="col-md-12 heading-section ftco-animate">
                <img src={{ asset("images/open-book.png") }} height="100px" style="margin: 40px" />
                <img src={{ asset("images/article_sq.png") }} height="140px" />
                @include('layouts.modul-breadcrumbs')
                <!-- <span class="subheading">Materi 1</span> -->
                <h2 class="mb-4">Modul 2 - Pemformatan teks dan peragraf</h2>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 ftco-animate">
                @include('materi-'.request()->segment(2).'.modul-'.request()->segment(3).'.sidebar')
            </div>
            <div class="col-md-9 ftco-animate">
                <h2 style="font-weight: 700" class="mb-4 text-center">Pemformatan Teks</h2>
                <p class="text-justify">HTML menggunakan tag seperti &lt;b&gt; dan &lt;i&gt; untuk memformat output, seperti teks tebal atau miring.</p>
                <p><b>Tag HTML Pemformatan Teks</b></p>
                <p>
                    <ul>
                        <li>&lt;b&gt; : Mendefinisikan teks tebal</li>
                        <div class="highlight">
                            <xmp><b>Teks tebal</b></xmp>
                        </div>
                        <br>
                        <li>&lt;i&gt; : Mendefinisikan teks miring</li>
                        <div class="highlight">
                            <xmp><b>Teks miring</b></xmp>
                        </div>
                        <br>
                        <li>&lt;small&gt; : Mendefinisikan teks kecil</li>
                        <div class="highlight">
                            <xmp><small>Teks kecil</small></xmp>
                        </div>
                        <br>
                        <li>&lt;sub&gt; : Mendefinisikan teks dibawah garis</li>
                        <div class="highlight">
                            <xmp><sub>Teks dibawah</sub></xmp>
                        </div>
                        <br>
                        <li>&lt;sup&gt; : Mendefinisikan teks diatas garis</li>
                        <div class="highlight">
                            <xmp><sup>Teks diatas garis</sup></xmp>
                        </div>
                        <br>
                        <li>&lt;mark&gt; : Mendefinisikan teks ditandai</li>
                        <div class="highlight">
                            <xmp><mark>Teks ditandai</mark></xmp>
                        </div>
                    </ul>
                </p>
                <!-- <div class="highlight">
                    <xmp><table>
<tr>
    <th>Month</th>
    <th>Savings</th>
</tr>
<tr>
    <td>January</td>
    <td>$100</td>
</tr>
</table></xmp>
                </div> -->
                
                <br>
                <br>
                <div class="row ftco-animate">
                    <!-- <div class="col-md-3"></div> -->
                    <!-- <div class="col-md-3"> -->
                        <!-- <center><p><a href="materi.html" class="btn btn-primary py-3">< Daftar materi</a></p></center> -->
                        <!-- <center><a href='plasma-flame.html' class="btn btn-primary btn-lg"><< Sebelumnya</a></center> -->
                    <!-- </div> -->
                    <div class="col-md-12">
                        <center><p><a href={{ url("materi/status/2/2/".((int)request()->segment(4)+1)) }} class="btn btn-primary py-3">Selanjutnya ></a></p></center>
                        <!-- <center><a href='persiapan-alat.html' class="btn btn-primary btn-lg">Materi Selanjutnya ></a></center> -->
                    </div>
                    <div class="col-md-3"></div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection