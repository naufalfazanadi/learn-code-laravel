@extends('layouts.layout')

@section('content')
<section class="ftco-section bg-white">
    <div class="container">
        <br>
        <div class="row  mb-3 pb-md-3">
            <div class="col-md-12 heading-section ftco-animate">
                <img src={{ asset("images/open-book.png") }} height="100px" style="margin: 40px" />
                <img src={{ asset("images/article_sq.png") }} height="140px" />
                @include('layouts.modul-breadcrumbs')
                <!-- <span class="subheading">Materi 1</span> -->
                <h2 class="mb-4">Modul 2 - Pemformatan teks dan peragraf</h2>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 ftco-animate">
                @include('materi-'.request()->segment(2).'.modul-'.request()->segment(3).'.sidebar')
            </div>
            <div class="col-md-9 ftco-animate">
                <form action={{ $latihan == "" ? "/latihan" : "/ubah-latihan" }} method="POST">
                    {{ csrf_field() }}
                    <h2 style="font-weight: 700" class="mb-4 text-center">Latihan</h2>
                    <p>Buatlah struktur dasar HTML dengan ketentuan sebagai berikut :</p>
                    <ol>
                        <li>Berikan nama “Latihan 2” pada judul halaman !</li>
                        <li>Pada bagian isi tuliskan kalimat dibawah ini :</li>
                    </ol>
                    <p class="text-justify">
                        <b>Sejarah HTML</b><br>Markup language atau yang kini dikenal sebagai html awalnya diciptakan oleh suatu perusahaan penjual perangkat keras dan lunak yakni <i>IBM atau International Bussiness Machines.</i>
                        <br>
                        Sekitar tahun 1980, <mark>IBM menciptakan sebuah bahasa yang menggabungkan tag atau simbol dengan teks dalam sebuah dokumen.</mark>
                    </p>
                    <div id="box">
                        <div class="row bg-light" style="padding: 10px">
                            <div class="col-md-6 text-center">
                                <h4 class="heading ftco-animate" style="font-weight: 600">Kode HTML</h4>
                                @if ($latihan == "")
                                    <textarea name="code" id="" rows="19" class="textarea-live1 form-control innerbox html" placeholder="HTML" style="font-size:16px;font-family:Consolas;color:#003399;white-space:pre-wrap;tab-size: 2;" required></textarea>
                                @else
                                    <textarea name="code" id="" rows="19" class="textarea-live1 form-control innerbox html" placeholder="HTML" style="font-size:16px;font-family:Consolas;color:#003399;white-space:pre-wrap;tab-size: 2;" required>{{ $latihan->code }}</textarea>
                                @endif
                                <br>
                            </div>
                            <!-- <div class="col-md-3 text-center">
                                <h4 class="heading ftco-animate" style="font-weight: 600">Kode CSS</h4>
                                <textarea name="" id="" rows="16" class="textarea-live2 form-control innerbox css" placeholder="CSS" style="font-family:Consolas;color:#003399;white-space:pre-wrap;tab-size: 2;"></textarea>
                            </div> -->
                            <div class="col-md-6 text-center">
                                <h4 class="heading ftco-animate" style="font-weight: 600">Keluaran</h4>
                                <div class="innerbox preview">
                                    <iframe id="live-update" src="" class="form-control" frameborder="0.3" style="width: 100%; min-height: 470px">
                                        <!DOCTYPE html>
                                        <html>
                                            <head>
                                                <meta charset="utf-8">
                                            </head>
                                            <body>
                                            </body>
                                        </html>
                                    </iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <br>
                    <div class="row ftco-animate">
                        @if ($latihan == "")
                            <div class="col-md-3"></div>
                            <div class="col-md-3">
                                <center><p><a href={{ url("materi/2/1/3") }} class="btn btn-primary py-3">< Sebelumnya</a></p></center>
                                <!-- <center><a href='plasma-flame.html' class="btn btn-primary btn-lg"><< Sebelumnya</a></center> -->
                            </div>
                            <div class="col-md-3">
                                {{-- <input type="hidden" id="id" name="jumlahSoal" value="5"> --}}
                                <input type="hidden" id="id" name="materi" value={{ request()->segment(2) }}>
                                <input type="hidden" id="id" name="modul" value={{ request()->segment(3) }}>
                                <center><input type="submit" value="Kirim Kode" class="btn btn-success py-3 px-5"></center>
                                <!-- <center><a href='persiapan-alat.html' class="btn btn-primary btn-lg">Materi Selanjutnya ></a></center> -->
                            </div>
                            <div class="col-md-3"></div>
                        @else
                            <div class="col-md-4">
                                <center><p><a href={{ url("materi/2/1/3") }} class="btn btn-primary py-3">< Sebelumnya</a></p></center>
                                <!-- <center><a href='plasma-flame.html' class="btn btn-primary btn-lg"><< Sebelumnya</a></center> -->
                            </div>
                            <div class="col-md-4">
                                {{-- <input type="hidden" id="id" name="jumlahSoal" value="5"> --}}
                                <input type="hidden" id="id" name="materi" value={{ request()->segment(2) }}>
                                <input type="hidden" id="id" name="modul" value={{ request()->segment(3) }}>
                                <center><input type="submit" value="Ubah Kode" class="btn btn-success py-3 px-5"></center>
                                <!-- <center><a href='persiapan-alat.html' class="btn btn-primary btn-lg">Materi Selanjutnya ></a></center> -->
                            </div>
                            <div class="col-md-4">
                                <center><p><a href={{ url("materi/2/1/kuis") }} class="btn btn-primary py-3">Selanjutnya ></a></p></center>
                            </div>
                        @endif
                    </div>
                    <div class="row">
                            <div class="col-md-12">
                                <br>
                                <h2 style="font-weight: 700" class="mb-4 text-center">Diskusi</h2>
                                <!-- <a href="http://foo.com/bar.html#disqus_thread">Link</a> -->
                                <div id="disqus_thread"></div>
                                <script>
                                    
                                    /**
                                    *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
                                    *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
                                    
                                    var disqus_config = function () {
                                        this.page.url = "{{ Request::url() }}";  // Replace PAGE_URL with your page's canonical URL variable
                                        this.page.identifier = 22; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                                    };
                                    
                                    (function() { // DON'T EDIT BELOW THIS LINE
                                        var d = document, s = d.createElement('script');
                                        s.src = 'https://learncode-diskusi-2.disqus.com/embed.js';
                                        s.setAttribute('data-timestamp', +new Date());
                                        (d.head || d.body).appendChild(s);
                                    })();
                                </script>
                                <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
                            </div>
                        </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection