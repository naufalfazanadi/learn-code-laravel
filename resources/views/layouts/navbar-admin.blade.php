<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
    <div class="container">
        <a class="navbar-brand" href={{ url("/admin") }}>LearnCode Admin</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="oi oi-menu"></span> Menu
        </button>
        
        <div class="collapse navbar-collapse" id="ftco-nav">
            <ul class="navbar-nav ml-auto">
                {{-- <li class="nav-item"><a href={{ url("/admin/tugas-guru") }} class="{{ Request::is('/admin/tugas-guru') ? 'nav-link active' : 'nav-link' }}">Tambah Tugas</a></li> --}}
                {{-- <li class="nav-item"><a href={{ url("materi") }} class="{{ (Request::is('materi/*') || Request::is('materi')) ? 'nav-link active' : 'nav-link' }}">Daftar Materi</a></li> --}}
                {{-- <li class="nav-item"><a href={{ url("latihan-koding") }} class="{{ Request::is('latihan-koding') ? 'nav-link active' : 'nav-link' }}">Latihan Koding</a></li> --}}
                {{-- @if(Auth::user())
                    <li class="nav-item"><a href={{ url("profil") }} class="nav-link {{ Request::is('profil') ? 'active' : '' }}"><span>Profil</span></a></li>
                    <li class="nav-item"><a href={{ url("logout") }} class="nav-link" 
                        onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                        <span>Logout</span></a>
                    </li>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                @else
                    <li class="nav-item cta"><a href={{ url("login") }} class="{{ Request::is('login') ? 'nav-link active' : 'nav-link' }}"><span>Login</span></a></li>
                @endif --}}
                <li class="nav-item"><a href={{ url("logout") }} class="nav-link" 
                    onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                    <span>Logout</span></a>
                </li>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </ul>
        </div>
    </div>
</nav>