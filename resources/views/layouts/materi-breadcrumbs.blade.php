<p class="breadcrumbs mb-0">
    <span class="mr-3">
        <a href={{ url("/") }}>Beranda <i class="ion-ios-arrow-forward"></i></a>
    </span> 
    <span class="mr-3">
        <a href={{ url("materi") }}>Daftar Materi <i class="ion-ios-arrow-forward"></i></a>
    </span> 
    <span>{{ $page }}</span>
</p>