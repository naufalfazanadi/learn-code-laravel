<p class="breadcrumbs mb-0">
    <span class="mr-3">
        <a href={{ url("/") }}>Beranda <i class="ion-ios-arrow-forward"></i></a>
    </span> 
    <span class="mr-3">
        <a href={{ url("materi") }}>Daftar Materi <i class="ion-ios-arrow-forward"></i></a>
    </span> 
    <span class="mr-3">
        <a href={{ url("materi/".request()->segment(2)) }}>Materi {{ request()->segment(2) }} <i class="ion-ios-arrow-forward"></i></a>
    </span> 
    <span>Modul {{ request()->segment(3) }}</span>
</p>