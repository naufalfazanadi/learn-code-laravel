@extends('layouts.layout')

@section('content')
<div class="hero-wrap js-fullheight">
    <div class="overlay"></div>
    <div class="container-fluid px-0">
        <div class="row d-md-flex no-gutters slider-text align-items-center js-fullheight justify-content-end">
            <img class="one-third js-fullheight align-self-end order-md-last img-fluid" src="images/undraw_pair_programming_njlp.svg" alt="">
            <div class="one-forth d-flex align-items-center ftco-animate js-fullheight">
                <div class="text mt-5">
                    <span class="subheading">Mulai Dari Sekarang</span>
                    <h1 class="mb-3">LearnCode</h1>
                    <p>Belajar pemrograman web HTML dengan materi yang lengkap dan media yang memudahkan pembelajaran.</p>
                    <p><a href="{{ url('materi') }}" class="btn btn-primary px-4 py-3">Mulai Belajar</a></p>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection