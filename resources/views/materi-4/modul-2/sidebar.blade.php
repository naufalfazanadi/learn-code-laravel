<div class="list-group">
    <h3 style="font-weight: 600">Tahapan Modul</h3>
    @php $count = 5 @endphp
    <a href={{ url("materi/4/2/1") }} class="list-group-item list-group-item-action {{ request()->segment(4) == '1' ? 'active' : '' }}">Menyajikan audio dalam halaman web</a>
    @php $count++ @endphp
    <a href={{ url("materi/4/2/1") }} class="list-group-item list-group-item-action {{ request()->segment(4) == '2' ? 'active' : '' }} {{ $status->materi4 < $count ? 'disabled bg-light' : '' }}">Menyajikan audio dalam halaman web (2)</a>
    @php $count++ @endphp
    <a href={{ url("materi/4/2/3") }} class="list-group-item list-group-item-action {{ request()->segment(4) == '3' ? 'active' : '' }} {{ $status->materi4 < $count ? 'disabled bg-light' : '' }}">Plugin audio</a>
    {{-- @php $count++ @endphp
    <a href={{ url("materi/4/2/latihan") }} class="list-group-item list-group-item-action {{ request()->segment(4) == 'latihan' ? 'active' : '' }} {{ $status->materi4 < $count ? 'disabled bg-light' : '' }}"><b>Latihan</b></a> --}}
    @php $count++ @endphp
    <a href={{ url("materi/4/2/kuis") }} class="list-group-item list-group-item-action {{ request()->segment(4) == 'kuis' ? 'active' : '' }} {{ $status->materi4 < $count ? 'disabled bg-light' : '' }}"><b>Kuis</b></a>
</div>
