@extends('layouts.layout')

@section('content')
<section class="ftco-section bg-white">
    <div class="container">
        <br>
        <div class="row  mb-3 pb-md-3">
            <div class="col-md-12 heading-section ftco-animate">
                    <img src={{ asset("images/open-book.png") }} height="100px" style="margin: 40px" />
                    <img src={{ asset("images/social-media_sq.png") }} height="140px" />
                    @include('layouts.modul-breadcrumbs')
                <!-- <span class="subheading">Materi 1</span> -->
                <h2 class="mb-4">Modul 2 - Format Tampilan File Audio</h2>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 ftco-animate">
                @include('materi-'.request()->segment(2).'.modul-'.request()->segment(3).'.sidebar')
            </div>
            <div class="col-md-9 ftco-animate">
                    <h2 style="font-weight: 700" class="mb-4 text-center">Plug-in Audio</h2>
                    <p class="text-justify">Plug-in merupakan sebuah program komputer kecil yang memperluas fungsi standar  dari sebuah browser. Plug-in dapat ditambahkan ke halaman HTML menggunakan tag &lt;object&gt; atau tag &lt;embed&gt;.</p>
                    <p>
                        <ul>
                            <li>Tag &lt;embed&gt;</li>
                            <p class="text-justify">Tag &lt;embed&gt; diartikan sebagai sebuah wadah untuk konten eksternal (non-HTML). Adapun  potongan kode untuk memainkan file *.mp3 yang embed/tertanam di halaman web.</p>
                            <div class="highlight">
                                <xmp><embed height="50" width="100" src="house.mp3"></xmp>
                            </div>
                            <br>
                            <li>Tag &lt;object&gt;</li>
                            <p class="text-justify">Tag &lt;object&gt; diartikan sebagai sebuah wadah untuk konten eksternal (non-HTML). Adapun  potongan kode untuk memainkan file *.mp3 yang object/tertanam di halaman web.</p>
                            <div class="highlight">
                                <xmp><object height="50" width="100" data="house.mp3"></xmp>
                            </div>
                        </ul>
                    </p>
                <!-- <div class="highlight">
                    <xmp><table>
<tr>
    <th>Month</th>
    <th>Savings</th>
</tr>
<tr>
    <td>January</td>
    <td>$100</td>
</tr>
</table></xmp>
                </div> -->
                
                <br>
                <br>
                <div class="row ftco-animate">
                    <div class="col-md-3"></div>
                    <div class="col-md-3">
                        <center><p><a href={{ url("materi/4/2/2") }} class="btn btn-primary py-3">< Sebelumnya</a></p></center>
                    </div>
                    <div class="col-md-3">
                        <center><p><a href={{ url("materi/status/4/2/".((int)request()->segment(4)+1)) }} class="btn btn-primary py-3">Selanjutnya ></a></p></center>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection