@extends('layouts.layout')

@section('content')
<section class="ftco-section bg-white">
    <div class="container">
        <br>
        <div class="row  mb-3 pb-md-3">
            <div class="col-md-12 heading-section ftco-animate">
                <img src={{ asset("images/open-book.png") }} height="100px" style="margin: 40px" />
                <img src={{ asset("images/social-media_sq.png") }} height="140px" />
                @include('layouts.modul-breadcrumbs')
                <!-- <span class="subheading">Materi 1</span> -->
                <h2 class="mb-4">Modul 2 - Format Tampilan File Audio</h2>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 ftco-animate">
                @include('materi-'.request()->segment(2).'.modul-'.request()->segment(3).'.sidebar')
            </div>
            <div class="col-md-9 ftco-animate">
                    <h2 style="font-weight: 700" class="mb-4 text-center">Menyajikan audio dalam halaman web</h2>
                    <p class="text-justify">Untuk memasukkan suara pada html bisa menggunakan tag &lt;Audio&gt;.  Format suara yang telah didukung hingga saat ini adalah MP3, Ogg dan WAP. File-file Audio yang didukung HTML5 adalah MP3, WAV, dan OGG. Tidak semua tipe file audio tersebut didukung oleh browser.</p>
                    <ul>
                        <p>a. OGG</p>
                        <div class="row">
                            <div class="col-md-1">
                                <center><img src={{ asset("images/ogg.svg") }} alt="" width="50px" class="img-responsive"></center>
                            </div>
                            <div class="col-md-11">
                                <p>Merupakan format audio yang didukung oleh Mozilla firefox, opera, dan google chrome.</p>
                            </div>
                        </div>
                        <br>
                        <p>b. MP3</p>
                        <div class="row">
                            <div class="col-md-1">
                                <center><img src={{ asset("images/mp3.svg") }} alt="" width="50px" class="img-responsive"></center>
                            </div>
                            <div class="col-md-11">
                                <p>Merupakan format audio yang didukung oleh Google Chrome dan safari.</p>
                            </div>
                        </div>
                        <br>
                        <p>c. WAV</p>
                        <div class="row">
                            <div class="col-md-1">
                                <center><img src={{ asset("images/wav.svg") }} alt="" width="50px" class="img-responsive"></center>
                            </div>
                            <div class="col-md-11">
                                <p>Merupakan format audio yang didukung oleh Mozilla Firefox dan Opera.</p>
                            </div>
                        </div>
                    </ul>               
                    <!-- <div class="highlight">
                    <xmp><table>
<tr>
    <th>Month</th>
    <th>Savings</th>
</tr>
<tr>
    <td>January</td>
    <td>$100</td>
</tr>
</table></xmp>
                </div> -->
                
                <br>
                <br>
                <div class="row ftco-animate">
                    <!-- <div class="col-md-3"></div> -->
                    <!-- <div class="col-md-3"> -->
                        <!-- <center><p><a href="materi.html" class="btn btn-primary py-3">< Daftar materi</a></p></center> -->
                        <!-- <center><a href='plasma-flame.html' class="btn btn-primary btn-lg"><< Sebelumnya</a></center> -->
                    <!-- </div> -->
                    <div class="col-md-12">
                        <center><p><a href={{ url("materi/status/4/2/".((int)request()->segment(4)+1)) }} class="btn btn-primary py-3">Selanjutnya ></a></p></center>
                        <!-- <center><a href='persiapan-alat.html' class="btn btn-primary btn-lg">Materi Selanjutnya ></a></center> -->
                    </div>
                    <div class="col-md-3"></div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection