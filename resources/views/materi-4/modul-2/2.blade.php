@extends('layouts.layout')

@section('content')
<section class="ftco-section bg-white">
    <div class="container">
        <br>
        <div class="row  mb-3 pb-md-3">
            <div class="col-md-12 heading-section ftco-animate">
                    <img src={{ asset("images/open-book.png") }} height="100px" style="margin: 40px" />
                    <img src={{ asset("images/social-media_sq.png") }} height="140px" />
                    @include('layouts.modul-breadcrumbs')
                <!-- <span class="subheading">Materi 1</span> -->
                <h2 class="mb-4">Modul 2 - Format Tampilan File Audio</h2>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 ftco-animate">
                @include('materi-'.request()->segment(2).'.modul-'.request()->segment(3).'.sidebar')
            </div>
            <div class="col-md-9 ftco-animate">
                    <h2 style="font-weight: 700" class="mb-4 text-center">Menyajikan audio dalam halaman web (2)</h2>
                    <p>Berikut adalah format lengkap untuk memainkan musik ke dalam aplikasi web</p>
                    
                    <div class="highlight">
                            <xmp><audio>
<source src="house.mp3" type="audio/mp3">
<source src="house.ogg" type="audio/ogg">
</audio></xmp>
                    </div>
                    <br>
                    <p class="text-justify">Atribut yang digunakan pada tag &lt;Audio&gt; &lt;/Audio&gt; dapat juga menggunakan atribut autoplay, loop dan controls. Detail atribut pada audio adalah sebagai berikut:</p>
                    <ul>
                        <li>
                            <p><b>Autoplay</b></p>
                            <p>Memulai audio secara otomatis pada web terbuka</p>
                        </li>
                        <div class="highlight">
                                <xmp><audio autoplay = "autoplay"></audio></xmp>
                        </div>
                        <br>
                        <li>
                            <p><b>Controls</b></p>
                            <p>Menampilkan control seperti volume, audio dan juga tombol untuk memulai/pause.</p>
                        </li>
                        <div class="highlight">
                                <xmp><audio control = "controls"></audio></xmp>
                        </div>
                        <br>
                        <li>
                            <p><b>Loop</b></p>
                            <p>Mengulang audio kembali setelah pemutaran awal selesai.</p>
                        </li>
                        <div class="highlight">
                                <xmp><audio loop = "loop"></audio></xmp>
                        </div>
                        <br>
                        <li>
                            <p><b>Muted</b></p>
                            <p>Mematikan suara.</p>
                        </li>
                        <div class="highlight">
                                <xmp><audio muted = "muted"></audio></xmp>
                        </div>
                        <br>
                        <li>
                            <p><b>Preload</b></p>
                            <p>Mematikan suara.Memuat ulang audio ketika halaman dimuat ulang.
                        <div class="highlight">
                                <xmp><audio preload = "preload"></audio></xmp>
                        </div>
                    </ul>                <!-- <div class="highlight">
                    <xmp><table>
<tr>
    <th>Month</th>
    <th>Savings</th>
</tr>
<tr>
    <td>January</td>
    <td>$100</td>
</tr>
</table></xmp>
                </div> -->
                
                <br>
                <br>
                <div class="row ftco-animate">
                    <div class="col-md-3"></div>
                    <div class="col-md-3">
                        <center><p><a href={{ url("materi/4/2/1") }} class="btn btn-primary py-3">< Sebelumnya</a></p></center>
                    </div>
                    <div class="col-md-3">
                        <center><p><a href={{ url("materi/status/4/2/".((int)request()->segment(4)+1)) }} class="btn btn-primary py-3">Selanjutnya ></a></p></center>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection