<div class="list-group">
    <h3 style="font-weight: 600">Tahapan Modul</h3>
    @php $count = 9 @endphp
    <a href={{ url("materi/4/3/1") }} class="list-group-item list-group-item-action {{ request()->segment(4) == '1' ? 'active' : '' }}">Format Tampilan Video</a>
    @php $count++ @endphp
    <a href={{ url("materi/4/3/2") }} class="list-group-item list-group-item-action {{ request()->segment(4) == '2' ? 'active' : '' }} {{ $status->materi4 < $count ? 'disabled bg-light' : '' }}">Menyajikan Video di Halaman Web</a>
    {{-- @php $count++ @endphp
    <a href={{ url("materi/4/3/latihan") }} class="list-group-item list-group-item-action {{ request()->segment(4) == 'latihan' ? 'active' : '' }} {{ $status->materi4 < $count ? 'disabled bg-light' : '' }}"><b>Latihan</b></a> --}}
    @php $count++ @endphp
    <a href={{ url("materi/4/3/kuis") }} class="list-group-item list-group-item-action {{ request()->segment(4) == 'kuis' ? 'active' : '' }} {{ $status->materi4 < $count ? 'disabled bg-light' : '' }}"><b>Kuis</b></a>
</div>
