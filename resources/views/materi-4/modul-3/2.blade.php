@extends('layouts.layout')

@section('content')
<section class="ftco-section bg-white">
    <div class="container">
        <br>
        <div class="row  mb-3 pb-md-3">
            <div class="col-md-12 heading-section ftco-animate">
                    <img src={{ asset("images/open-book.png") }} height="100px" style="margin: 40px" />
                    <img src={{ asset("images/social-media_sq.png") }} height="140px" />
                    @include('layouts.modul-breadcrumbs')
                <!-- <span class="subheading">Materi 1</span> -->
                <h2 class="mb-4">Modul 3 - Format Tampilan File Video</h2>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 ftco-animate">
                @include('materi-'.request()->segment(2).'.modul-'.request()->segment(3).'.sidebar')
            </div>
            <div class="col-md-9 ftco-animate">
                    <h2 style="font-weight: 700" class="mb-4 text-center">Menyajikan Video di Halaman Web</h2>

                    <p class="text-justify">Ketika mencoba menjalankan video pada halaman web , file video ditempatkan dalam satu folder yang sama dengan file *.html dan nama file video nya disesuaikan dengan file video yang anda gunakan. Berikut adalah format lengkap untuk memainkan video ke dalam aplikasi web.</p>
                    <div class="highlight">
                        <xmp><video width="500" height="350" controls>
    <source src="images/video.mp4" type="video/mp4">
</video></xmp>
                    </div>
                    <br>
                    <p class="text-justify">Tag &lt;Video&gt; memiliki beberapa atribut, tidak hanya width dan height saja. Kontrol atribut menambahkan kontrol video, seperti play, pause, dan volume. Berikut ini adalah atribut – atribut yang digunakan pada tag &lt;Video&gt; :</p>
                    <p>
                        <ul>
                            <li><b> Autoplay</b></li>
                            <p>Memulai secara otomatis video </p>
                            <div class="highlight">
                                <xmp><video autoplay="autoplay"></xmp>
                            </div>
                            <br>
                            <li><b> Controls</b></li>
                            <p>Menampilkan tombol kontrol video</p>
                            <div class="highlight">
                                <xmp><video controls="controls"></xmp>
                            </div>
                            <br>
                            <li><b> Height</b></li>
                            <p>Mengatur tinggi frame video</p>
                            <div class="highlight">
                                <xmp><video height="500"></xmp>
                            </div>
                            <br>
                            <li><b> Width</b></li>
                            <p>Mengatur lebar video</p>
                            <div class="highlight">
                                <xmp><video widht="500"></xmp>
                            </div>
                            <br>
                            <li><b> Loop</b></li>
                            <p>Memutar video secara berulang-ulang</p>
                            <div class="highlight">
                                <xmp><video loop="loop"></xmp>
                            </div>
                            <br>
                            <li><b> Src</b></li>
                            <p>Memberi link video</p>
                            <div class="highlight">
                                <xmp><source src="bola.mp4" type="video/mp4"></xmp>
                            </div>
                        </ul>
                    </p>                <!-- <div class="highlight">
                    <xmp><table>
<tr>
    <th>Month</th>
    <th>Savings</th>
</tr>
<tr>
    <td>January</td>
    <td>$100</td>
</tr>
</table></xmp>
                </div> -->
                
                <br>
                <br>
                <div class="row ftco-animate">
                    <div class="col-md-3"></div>
                    <div class="col-md-3">
                        <center><p><a href={{ url("materi/4/3/1") }} class="btn btn-primary py-3">< Sebelumnya</a></p></center>
                    </div>
                    <div class="col-md-3">
                        <center><p><a href={{ url("materi/status/4/3/".((int)request()->segment(4)+1)) }} class="btn btn-primary py-3">Selanjutnya ></a></p></center>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection