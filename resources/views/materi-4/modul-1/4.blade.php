@extends('layouts.layout')

@section('content')
<section class="ftco-section bg-white">
    <div class="container">
        <br>
        <div class="row  mb-3 pb-md-3">
            <div class="col-md-12 heading-section ftco-animate">
                    <img src={{ asset("images/open-book.png") }} height="100px" style="margin: 40px" />
                    <img src={{ asset("images/social-media_sq.png") }} height="140px" />
                    @include('layouts.modul-breadcrumbs')
                <!-- <span class="subheading">Materi 1</span> -->
                <h2 class="mb-4">Modul 1 - Format Tampilan Gambar</h2>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 ftco-animate">
                @include('materi-'.request()->segment(2).'.modul-'.request()->segment(3).'.sidebar')
            </div>
            <div class="col-md-9 ftco-animate">
                <h2 style="font-weight: 700" class="mb-4 text-center">Memberikan Keterangan Pada Gambar</h2>
                <p class="text-justify">Agar lebih terkesan informatif, anda dapat memberikan keterangan yang relevan dan seperlunya (tidak berlebihan) terhadap gambar yang anda tampilkan pada halaman web. Keterangan ini akan muncul dalam bentuk tooltip (sesaat, hanya beberapa detik) pada saat penunjung mengarahkan kursor (penjujuk mouse) ke atas gambar. Untuk membuat keterangan semacam ini, kita dapat menggunakan atribut TITLE maupun ALT pada tag &lt;img&gt;.</p>
                <div class="highlight">
                    <xmp><img  src=”title.png”  title="linux"></xmp>
                </div>
                <p>Hasil dari kodingan diatas:</p>
                <img src={{ asset("images/title.png") }} alt="" width="200px" class="img-responsive">
                <!-- <div class="highlight">
                    <xmp><table>
<tr>
    <th>Month</th>
    <th>Savings</th>
</tr>
<tr>
    <td>January</td>
    <td>$100</td>
</tr>
</table></xmp>
                </div> -->
                
                <br>
                <br>
                <div class="row ftco-animate">
                    <div class="col-md-3"></div>
                    <div class="col-md-3">
                        <center><p><a href={{ url("materi/4/1/3") }} class="btn btn-primary py-3">< Sebelumnya</a></p></center>
                    </div>
                    <div class="col-md-3">
                        <center><p><a href={{ url("materi/status/4/1/".((int)request()->segment(4)+1)) }} class="btn btn-primary py-3">Selanjutnya ></a></p></center>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection