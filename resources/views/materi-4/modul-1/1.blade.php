@extends('layouts.layout')

@section('content')
<section class="ftco-section bg-white">
    <div class="container">
        <br>
        <div class="row  mb-3 pb-md-3">
            <div class="col-md-12 heading-section ftco-animate">
                <img src={{ asset("images/open-book.png") }} height="100px" style="margin: 40px" />
                <img src={{ asset("images/social-media_sq.png") }} height="140px" />
                @include('layouts.modul-breadcrumbs')
                <!-- <span class="subheading">Materi 1</span> -->
                <h2 class="mb-4">Modul 1 - Format Tampilan Gambar</h2>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 ftco-animate">
                @include('materi-'.request()->segment(2).'.modul-'.request()->segment(3).'.sidebar')
            </div>
            <div class="col-md-9 ftco-animate">
                    <h2 style="font-weight: 700" class="mb-4 text-center">Memasukan Gambar</h2>
                    <p class="text-justify">
                        Cara yang diperlukan untuk memasukkan atau menambahakan gambar ke dalam suatu halaman web sangatlah mudah. HTML telah menyediakan tag khusus untuk keperluan ini, yaitu &lt;img&gt;. Tag ini merupakan tag tunggal atau tidak memiliki pasangan. Atribut terpenting dari tag <img> adalah SRC (source atau sumber), yang berisi file gambar yang akan ditampilkan ke dalam halaman web.
                    </p>
                    <p>Bentuk umum penggunaan tag &lt;img&gt; adalah:</p>
                    <div class="highlight">
                        <xmp><img src = ”namafile”></xmp>
                    </div>
                    <p class="text-justify">Jika file yang akan ditampilkan berada di direktori lain atau bahkan berada di situs web lain, makan perlu menyebutkan juga lokasi dari file tersebut.</p>
                    <div class="highlight">
                        <xmp><img src = “../image/komputer.jpg”></xmp>
                    </div>
                    <p>Atau</p>
                    <div class="highlight">
                        <xmp><img src = http://www.abcde.com/image/kamera.jpg/></xmp>
                    </div>               
                    <!-- <div class="highlight">
                    <xmp><table>
<tr>
    <th>Month</th>
    <th>Savings</th>
</tr>
<tr>
    <td>January</td>
    <td>$100</td>
</tr>
</table></xmp>
                </div> -->
                
                <br>
                <br>
                <div class="row ftco-animate">
                    <!-- <div class="col-md-3"></div> -->
                    <!-- <div class="col-md-3"> -->
                        <!-- <center><p><a href="materi.html" class="btn btn-primary py-3">< Daftar materi</a></p></center> -->
                        <!-- <center><a href='plasma-flame.html' class="btn btn-primary btn-lg"><< Sebelumnya</a></center> -->
                    <!-- </div> -->
                    <div class="col-md-12">
                        <center><p><a href={{ url("materi/status/4/1/".((int)request()->segment(4)+1)) }} class="btn btn-primary py-3">Selanjutnya ></a></p></center>
                        <!-- <center><a href='persiapan-alat.html' class="btn btn-primary btn-lg">Materi Selanjutnya ></a></center> -->
                    </div>
                    <div class="col-md-3"></div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection