@extends('layouts.layout')

@section('content')
<section class="ftco-section bg-white">
    <div class="container">
        <br>
        <div class="row  mb-3 pb-md-3">
            <div class="col-md-12 heading-section ftco-animate">
                    <img src={{ asset("images/open-book.png") }} height="100px" style="margin: 40px" />
                    <img src={{ asset("images/table_sq.png") }} height="140px" />
                    @include('layouts.modul-breadcrumbs')
                <!-- <span class="subheading">Materi 1</span> -->
                <h2 class="mb-4">Modul 1 - Format Tampilan Gambar</h2>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 ftco-animate">
                @include('materi-'.request()->segment(2).'.modul-'.request()->segment(3).'.sidebar')
            </div>
            <div class="col-md-9 ftco-animate">
                @if ($kuis == "")
                    <form action="/kuis" method="POST">
                        {{ csrf_field() }}
                        <h2 style="font-weight: 700" class="mb-4 text-center">Kuis</h2>
                        @if (session('status_repeat'))
                            <div class="alert alert-info">
                                {{ session('status_repeat') }}
                            </div>
                        @endif
                        <p>1. Tag yang digunakan untuk menampilkan sebuah gambar adalah . . .</p>
                        <div class="highlight">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no1" id="1a" value="a" required>
                                <label class="form-check-label" for="1a">
                                    a.	&lt;img&gt;
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no1" id="1b" value="b">
                                <label class="form-check-label" for="1b">
                                    b.	&lt;jpg&gt;
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no1" id="1c" value="c">
                                <label class="form-check-label" for="1c">
                                    c.	&lt;png&gt;
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no1" id="1d" value="d">
                                <label class="form-check-label" for="1d">
                                    d.	&lt;pict&gt;
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no1" id="1e" value="e">
                                <label class="form-check-label" for="1e">
                                    e.	&lt;gambar&gt;
                                </label>
                            </div>
                        </div>
                        <br>
                        <p>2. Perintah untuk menambahkan gambar dengan nama “kamera.jpg” yang file-nya berada di folder images, maka penulisannya adalah . . .</p>
                        <div class="highlight">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no2" id="2a" value="a" required>
                                <label class="form-check-label" for="2a">
                                    a.	&lt;img src = “kamera”&gt;
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no2" id="2b" value="b">
                                <label class="form-check-label" for="2b">
                                    b.	&lt;img src = “images/kamera.jpg”&gt;
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no2" id="2c" value="c">
                                <label class="form-check-label" for="2c">
                                    c.	&lt;img src=http://www.abcde.com/image/kamera.jpg/&gt;
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no2" id="2d" value="d">
                                <label class="form-check-label" for="2d">
                                    d.	&lt;img src=http://image/kamera.jpg/&gt;
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no2" id="2e" value="e">
                                <label class="form-check-label" for="2e">
                                    e.	&lt;img src=http://www.abcde.com/image/kamera.jpg/&gt;
                                </label>
                            </div>
                        </div>
                        <br>
                        <p>3. Tag &lt;img&gt; memiliki atribut . . .</p>
                        <div class="highlight">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no3" id="3a" value="a" required>
                                <label class="form-check-label" for="3a">
                                    a.	Size, color, face
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no3" id="3b" value="b">
                                <label class="form-check-label" for="3b">
                                    b.	Size, width
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no3" id="3c" value="c">
                                <label class="form-check-label" for="3c">
                                    c.	Height, width
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no3" id="3d" value="d">
                                <label class="form-check-label" for="3d">
                                    d.	Type
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no3" id="3e" value="e">
                                <label class="form-check-label" for="3e">
                                    e.	Button                                        
                                </label>
                            </div>
                        </div>
                        <br>
                        <p>4. Di bawah ini mana kode yang benar dalam menyisipkan gambar pada HTML . . .</p>
                        <div class="highlight">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no4" id="4a" value="a" required>
                                <label class="form-check-label" for="4a">
                                    a.	&lt;img src = "koala.jpg"/&gt;
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no4" id="4b" value="b">
                                <label class="form-check-label" for="4b">
                                    b.	&lt;img = "koala.jpg"/&gt;
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no4" id="4c" value="c">
                                <label class="form-check-label" for="4c">
                                    c.	&lt;img src = koala/&gt;
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no4" id="4d" value="d">
                                <label class="form-check-label" for="4d">
                                    d.	&lt;img = koala/&gt;
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no4" id="4e" value="e">
                                <label class="form-check-label" for="4e">
                                    e.	&lt;src = koala.jpg/&gt;
                                </label>
                            </div>
                        </div>
                        <br>
                        <p>5. Terdapat gambar berukuran 200x200, ketika akan dimasukan ke dalam sebuah web ukuran gambar tersebut ingin diubah menjadi 100x100. Kode yang tepat untuk menyisipkan gambar tersebut adalah . . .</p>
                        <div class="highlight">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no5" id="5a" value="a" required>
                                <label class="form-check-label" for="5a">
                                    a.	&lt;img src = "koala" height = 100 width = 100/&gt;
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no5" id="5b" value="b">
                                <label class="form-check-label" for="5b">
                                    b.	&lt;img src = "koala.jpg" height = “100” width = “100”/&gt;
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no5" id="5c" value="c">
                                <label class="form-check-label" for="5c">
                                    c.	&lt;img = "koala.jpg" height = “100” width = “100”/&gt;
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no5" id="5d" value="d">
                                <label class="form-check-label" for="5d">
                                    d.	&lt;img = koala.jpg height = “100” width = “100”/&gt;
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no5" id="5e" value="e">
                                <label class="form-check-label" for="5e">
                                    e.	&lt;src = koala.jpg height = 100 width = 100/&gt;
                                </label>
                            </div>
                        </div>
                        <br>
                        <br>
                        <div class="row ftco-animate">
                            <div class="col-md-3"></div>
                            <div class="col-md-3">
                                <center><p><a href={{ url("materi/4/1/latihan") }} class="btn btn-primary py-3">< Sebelumnya</a></p></center>
                                <!-- <center><a href='plasma-flame.html' class="btn btn-primary btn-lg"><< Sebelumnya</a></center> -->
                            </div>
                            <div class="col-md-3">
                                <input type="hidden" id="id" name="jumlahSoal" value="5">
                                <input type="hidden" id="id" name="materi" value={{ request()->segment(2) }}>
                                <input type="hidden" id="id" name="modul" value={{ request()->segment(3) }}>

                                <center><input type="submit" value="Kirim Jawaban" class="btn btn-success py-3 px-5"></center>
                                <!-- <center><a href='persiapan-alat.html' class="btn btn-primary btn-lg">Materi Selanjutnya ></a></center> -->
                            </div>
                            <div class="col-md-3"></div>
                        </div>
                    </form>
                @else
                    <div class="alert alert-success">
                        <h3 class="text-center">Kamu telah menyelesaikan kuis</h2>
                        <h2 style="font-weight: 700" class="mb-4 text-center">Nilai kamu : <b>{{ $kuis->nilai }}</b></h3>
                    </div>
                    <br>
                    <br>
                    <div class="row ftco-animate">
                        <div class="col-md-3"></div>
                        <div class="col-md-3">
                            <center><p><a href={{ url("materi/4/1/4") }} class="btn btn-primary py-3">< Sebelumnya</a></p></center>
                        </div>
                        <div class="col-md-3">
                            <center><p><a href={{ url("materi/4") }} class="btn btn-success py-3">Daftar modul</a></p></center>
                        </div>
                        <div class="col-md-3"></div>
                    </div>
                @endif
            </div>
        </div>
    </div>
</section>
@endsection