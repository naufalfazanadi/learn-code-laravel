@extends('layouts.layout')

@section('content')
<section class="ftco-section bg-white">
    <div class="container">
        <br>
        <div class="row  mb-3 pb-md-3">
            <div class="col-md-12 heading-section ftco-animate">
                    <img src={{ asset("images/open-book.png") }} height="100px" style="margin: 40px" />
                    <img src={{ asset("images/social-media_sq.png") }} height="140px" />
                    @include('layouts.modul-breadcrumbs')
                <!-- <span class="subheading">Materi 1</span> -->
                <h2 class="mb-4">Modul 1 - Format Tampilan Gambar</h2>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 ftco-animate">
                @include('materi-'.request()->segment(2).'.modul-'.request()->segment(3).'.sidebar')
            </div>
            <div class="col-md-9 ftco-animate">
                    <h2 style="font-weight: 700" class="mb-4 text-center">Menggabungkan Gambar dan Teks</h2>
                    <p class="text-justify">Jika kita ingin menggabungkan atau menyisipkan gambar di dalam suatu teks atau paragraf tertentu, terdapat pilihan posisi gambar yang harus diperhatikan: </p>
                    <p><b> Secara Vertikal</b></p>
                    <img src={{ asset("images/top.png") }} alt="" width="350px" class="img-responsive"> <br>
                    <img src={{ asset("images/middle.png") }} alt="" width="350px" class="img-responsive"> <br>
                    <img src={{ asset("images/bottom.png") }} alt="" width="350px" class="img-responsive">
                    <p class="text-justify">Untuk posisi gambar yang dilihat dari arah vertikal dapat ditentukan dengan mengisi nilai TOP, MIDDLE, atau BOTTOM ke dalam atribut ALIGN yang terdapat pada tag &lt;img&gt;.</p>
                    <p><b>Secara Horizontal</b></p>
                    <img src={{ asset("images/right.png") }} alt="" width="350px" class="img-responsive"> <br>
                    <img src={{ asset("images/left.png") }} alt="" width="350px" class="img-responsive">
                    <p class="text-justify">Adapun untuk posisi gambar yang dilihat dari arah horizontal ditentukan dengan mengisi nilai LEFT dan RIGHT kedalam ALIGN pada tag &lt;img&gt;.</p>
                <!-- <div class="highlight">
                    <xmp><table>
<tr>
    <th>Month</th>
    <th>Savings</th>
</tr>
<tr>
    <td>January</td>
    <td>$100</td>
</tr>
</table></xmp>
                </div> -->
                
                <br>
                <br>
                <div class="row ftco-animate">
                    <div class="col-md-3"></div>
                    <div class="col-md-3">
                        <center><p><a href={{ url("materi/4/1/1") }} class="btn btn-primary py-3">< Sebelumnya</a></p></center>
                    </div>
                    <div class="col-md-3">
                        <center><p><a href={{ url("materi/status/4/1/".((int)request()->segment(4)+1)) }} class="btn btn-primary py-3">Selanjutnya ></a></p></center>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection