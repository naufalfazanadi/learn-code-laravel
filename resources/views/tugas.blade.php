@extends('layouts.layout')

@section('content')
<section class="ftco-section bg-light">
    <div class="container">
        <div class="row mb-3 pb-md-3 justify-content-center">
            <div class="col-md-7 text-center heading-section ftco-animate">
                <img src={{ asset("images/undraw_taking_notes_tjaf.svg") }} height="140px" />
                @include('layouts.materi-breadcrumbs')
                <!-- <span class="subheading">Materi 1</span> -->
                <h2 class="mb-4">Tugas {{ $tugas->judul_tugas }}</h2>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <span><h3 style="font-weight: 700">Soal</h3><p>{{ $tugas->soal }}</p></span>
                
                <p>
                    @php
                        $date = new Date($tugas->deadline);
                        $resultDate = $date->format('l, d F Y H:i:s');   
                    @endphp
                    <b>Deadline :</b> {{ $resultDate }}
                </p>
            </div>
        </div>
        <hr>
        <form action="/tugas" method="POST">
            {{ csrf_field() }}
            <div id="box">
                <div class="row">
                    <div class="col-md-5 text-center">
                        <h3 class="heading ftco-animate" style="font-weight: 700">Kode HTML</h3>
                        <div class="form-group">
                            <textarea name="kode" id="" rows="19" class="textarea-live1 form-control innerbox html" placeholder="HTML" style="font-size:16px;font-family:Consolas;color:#003399;white-space:pre-wrap;tab-size: 2;" required></textarea>
                        </div>
                        <br>
                    </div>
                    <!-- <div class="col-md-3 text-center">
                        <h3 class="heading ftco-animate" style="font-weight: 700">Kode CSS</h3>
                        <textarea name="" id="" rows="16" class="textarea-live2 form-control innerbox css" placeholder="CSS" style="font-family:Consolas;color:#003399;white-space:pre-wrap;tab-size: 2;"></textarea>
                    </div> -->
                    <div class="col-md-7 text-center">
                        <h3 class="heading ftco-animate" style="font-weight: 700">Keluaran</h3>
                        <div class="innerbox preview">
                            <iframe id="live-update" src="" class="form-control" frameborder="0.3" style="width: 100%; min-height: 470px">
                                <!DOCTYPE html>
                                <html>
                                <head>
                                    <meta charset="utf-8">
                                </head>
                                <body>
                                </body>
                                </html>
                            </iframe>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row ftco-animate">
                    <div class="col-md-12">
                        <input type="hidden" id="id" name="tugas_guru_id" value="{{ $tugas->id }}">
                        <input type="hidden" id="id" name="materi" value="{{ $tugas->materi }}">
                        <center><input type="submit" value="Kirim Tugas" class="btn btn-primary py-3 px-5"></center>
                        {{-- <center><p><a href="materi-1.html" class="btn btn-primary py-3" type="submit">Simpan Kodingan</a></p></center> --}}
                        <!-- <center><a href='persiapan-alat.html' class="btn btn-primary btn-lg">Materi Selanjutnya ></a></center> -->
                    </div>
                    <!-- <div class="col-md-3"></div> -->
                </div>
            </div>
        </form>
    </div>
</section>
@endsection