@extends('layouts.layout')

@section('content')
<div class="hero-wrap hero-wrap-2" style="background-image: url('images/image_2.jpg');" data-stellar-background-ratio="0.5">
    <div class="overlay"></div>
    <div class="container">
        <div class="row no-gutters slider-text align-items-end justify-content-start">
            <div class="col-md-8 ftco-animate text-center text-md-left mb-5">
                <p class="breadcrumbs mb-0"><span class="mr-3"><a href="index.html">Beranda <i class="ion-ios-arrow-forward"></i></a></span> <span>Latihan Koding</span></p>
                <h1 class="mb-3 bread">Latihan Koding</h1>
            </div>
        </div>
    </div>
</div>

<section class="ftco-section bg-light">
    <div class="container">
        @if (session('status_gagal'))
            <div class="alert alert-danger">
                {{ session('status_gagal') }}
            </div>
        @endif
        @if (session('status_sukses'))
            <div class="alert alert-success">
                {{ session('status_sukses') }}
            </div>
        @endif
        <!-- <div class="row justify-content-center mb-5 pb-3">
            <div class="col-md-7 text-center heading-section ftco-animate">
                <h2 class="mb-4">Practice makes perfect.</h2>
            </div>
        </div> -->
        <form action="/latihan-koding" method="POST">
            {{ csrf_field() }}
            <div id="box">
                <div class="row">
                    <div class="col-md-5 text-center">
                        <h3 class="heading ftco-animate" style="font-weight: 700">Kode HTML</h3>
                        <div class="form-group">
                            <textarea name="kode" id="" rows="19" class="textarea-live1 form-control innerbox html" placeholder="HTML" style="font-size:16px;font-family:Consolas;color:#003399;white-space:pre-wrap;tab-size: 2;" required></textarea>
                        </div>
                        <br>
                    </div>
                    <!-- <div class="col-md-3 text-center">
                        <h3 class="heading ftco-animate" style="font-weight: 700">Kode CSS</h3>
                        <textarea name="" id="" rows="16" class="textarea-live2 form-control innerbox css" placeholder="CSS" style="font-family:Consolas;color:#003399;white-space:pre-wrap;tab-size: 2;"></textarea>
                    </div> -->
                    <div class="col-md-7 text-center">
                        <h3 class="heading ftco-animate" style="font-weight: 700">Keluaran</h3>
                        <div class="innerbox preview">
                            <iframe id="live-update" src="" class="form-control" frameborder="0.3" style="width: 100%; min-height: 470px">
                                <!DOCTYPE html>
                                <html>
                                <head>
                                    <meta charset="utf-8">
                                </head>
                                <body>
                                </body>
                                </html>
                            </iframe>
                        </div>
                    </div>
                </div>
                <div class="row ftco-animate">
                    <div class="col-md-2">
                        <h4 style="font-weight: 700">Judul Kode :</h4>
                    </div>
                    <div class="col-md-10">
                        <div class="form-group">
                            <input name="judul" type="text" class="form-control" placeholder="Judul" width="100%" required>
                        </div>
                        <!-- <div class="col-md-3"></div> -->
                        <!-- <div class="col-md-3"> -->
                            <!-- <center><p><a href="materi-1-modul-1-latihan.html" class="btn btn-primary py-3">< Sebelumnya</a></p></center> -->
                            <!-- <center><a href='plasma-flame.html' class="btn btn-primary btn-lg"><< Sebelumnya</a></center> -->
                            <!-- </div> -->
                    </div>
                </div>
                <br>
                <div class="row ftco-animate">
                    <div class="col-md-12">
                        <center><input type="submit" value="Simpan Kodingan" class="btn btn-primary py-3 px-5"></center>
                        {{-- <center><p><a href="materi-1.html" class="btn btn-primary py-3" type="submit">Simpan Kodingan</a></p></center> --}}
                        <!-- <center><a href='persiapan-alat.html' class="btn btn-primary btn-lg">Materi Selanjutnya ></a></center> -->
                    </div>
                    <!-- <div class="col-md-3"></div> -->
                </div>
            </div>
        </form>
    </div>
</section>
@endsection