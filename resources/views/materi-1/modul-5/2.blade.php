@extends('layouts.layout')

@section('content')
<section class="ftco-section bg-white">
    <div class="container">
        <br>
        <div class="row  mb-3 pb-md-3">
            <div class="col-md-12 heading-section ftco-animate">
                <img src={{ asset("images/open-book.png") }} height="100px" style="margin: 40px" />
                <img src={{ asset("images/server_sq.png") }} height="140px" />
                @include('layouts.modul-breadcrumbs')
                <!-- <span class="subheading">Materi 1</span> -->
                <h2 class="mb-4">Modul 5 - Perangkat pengembangan aplikasi web</h2>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 ftco-animate">
                @include('materi-'.request()->segment(2).'.modul-'.request()->segment(3).'.sidebar')
            </div>
            <div class="col-md-9 ftco-animate">
                <h2 style="font-weight: 700" class="mb-4 text-center">Perangkat Lunak</h2>
                <p class="text-justify">Untuk perangkat lunak pengembangan web dapat dikelompokkan menjadi beberapa, diantaranya:</p>
                <ol>
                    <li>
                        Web Page Authoring
                        <p class="text-justify">Ini adalah kelompok perangkat lunak seperti desktop publishing tetapi format yang dihasilkan adalah halaman web. Beberapa contoh aplikasi yang termasuk dalam kelompok ini:</p>
                        <ul>
                            <li class="text-justify">Adobe Dreamweaver, aplikasi ini berbayar dan dibawah lisensi perusahaan Adobe, aplikasi ini banyak digunakan oleh pengembang web. Memiliki banyak fasilitas dan fiture.</li>
                            <li class="text-justify">Microsoft Expression Web (lisensi dari Windows). Bagian  dari    Microsoft’s   suite yang merupakan aplikasi perancangan professional.</li>
                            <li class="text-justify">Nvu (Linux, Windows, dan Mac OS X). Tidak ingin membayar untuk Editor WYSIWYG? Nvu (diucapkan N-view, untuk "pandangan baru") adalah sebuah aplikasi open source (sumber terbuka) yang cocok dengan banyak fitur dalam Adobe Dreamweaver, dan dapat diunduh secara gratis di nvu.com.</li>
                        </ul>
                    </li>
                    <li>
                        Editor
                        <p class="text-justify">Ini adalah kelompok perangkat lunak yang kebalikan dari Adobe Dreamweaver Microsoft Expression Web. Beberapa contoh aplikasi yang termasuk dalam kelompok ini:</p>
                        <ul>
                            <li class="text-justify">TextPad (Windows saja). TextPad adalah plain-text sederhana dan murah untuk editor kode di Windows.</li>
                            <li class="text-justify">Teks Sublime (Window, Mac, Linux). Editor teks ini terlihat dipreteli namun memiliki banyak fungsi (seperti kode warna dan ikhtisar kode penuh) bagi pengembang web.</li>
                            <li class="text-justify">Textmate oleh MacroMates (hanya Macintosh). Editor teks canggih ini memiliki alat manajemen proyek dan antarmuka yang terintegrasi dengan sistem operasi Mac. Hal ini semakin populer karena disesuaikan, kaya fitur, dan murah.</li>
                        </ul>
                    </li>
                    <li>
                        Aplikasi editing gambar dan menggambar
                        <p class="text-justify">Untuk menambahkan gambar ke halaman web, akan perlu program editing gambar. Berikut ini beberapa aplikasi yang cukup populer:</p>
                        <ul>
                            <li class="text-justify">Adobe Photoshop. Photoshop dapat dikatakan sebagai standar industri untuk penciptaan gambar baik dalam cetak maupun  dunia web.</li>
                            <li class="text-justify">Adobe Illustrator. Karena desainer perlu membuat logo, ikon, dan ilustrasi di berbagai ukuran dan resolusi, untuk itu semua aplikasi ini sangat membantu.</li>
                            <li class="text-justify">Corel Paint Shop Photo Pro (Windows saja). Editor gambar yang penuh fitur ini populer  di pengguna Windows, terutama karena harga yang rendah.</li>
                        </ul>
                    </li>
                    <li>
                        Aplikasi Internet
                        <p class="text-justify">Karena hasil akhir dari pengembangan halaman web digunakan di Internet, maka diperlukan beberapa aplikasi untuk membuka halaman web, biasa disebut “browser”.    Berikut  ini  beberapa  aplikasi  browser  yang  cukup  populer:</p>
                        <ul>
                            <li>
                                Windows:
                                <ul>
                                    <li>Internet Explorer</li>
                                    <li>Chrome</li>
                                    <li>Firefox</li>
                                    <li>Safari</li>
                                    <li>Opera</li>
                                </ul>
                            </li>
                            <li>
                                Macintosh OS X: 
                                <ul>
                                    <li>Safari</li>
                                    <li>Chrome</li>
                                    <li>Firefox</li>
                                    <li>Opera</li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li>
                        Aplikasi Transer File
                        <p class="text-justify">Sebuah aplikasi FTP memungkinkan untuk mengunggah dan mengunduh file antara komputer kita dan komputer yang akan menjadi tempat halaman web/web server. Beberapa aplikasi authoring web sebelumnya juga memiliki aplikasi FTP yang terintegrasi langsung.</p>
                    </li>
                </ol>
                <!-- <div class="highlight">
                    <xmp><table>
<tr>
    <th>Month</th>
    <th>Savings</th>
</tr>
<tr>
    <td>January</td>
    <td>$100</td>
</tr>
</table></xmp>
                </div> -->
                
                <br>
                <br>
                <div class="row ftco-animate">
                    <div class="col-md-3"></div>
                    <div class="col-md-3">
                        <center><p><a href={{ url("materi/1/5/".((int)request()->segment(4)-1)) }} class="btn btn-primary py-3">< Sebelumnya</a></p></center>
                    </div>
                    <div class="col-md-3">
                        <center><p><a href={{ url("materi/status/1/5/".((int)request()->segment(4)+1)) }} class="btn btn-primary py-3">Selanjutnya ></a></p></center>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection