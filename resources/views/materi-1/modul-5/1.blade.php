@extends('layouts.layout')

@section('content')
<section class="ftco-section bg-white">
    <div class="container">
        <br>
        <div class="row  mb-3 pb-md-3">
            <div class="col-md-12 heading-section ftco-animate">
                <img src={{ asset("images/open-book.png") }} height="100px" style="margin: 40px" />
                <img src={{ asset("images/server_sq.png") }} height="140px" />
                @include('layouts.modul-breadcrumbs')
                <!-- <span class="subheading">Materi 1</span> -->
                <h2 class="mb-4">Modul 5 - Perangkat pengembangan aplikasi web</h2>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 ftco-animate">
                @include('materi-'.request()->segment(2).'.modul-'.request()->segment(3).'.sidebar')
            </div>
            <div class="col-md-9 ftco-animate">
                <h2 style="font-weight: 700" class="mb-4 text-center">Perangkat Keras</h2>
                <p class="text-justify">Untuk lingkungan pengembangan web yang nyaman, berikut ini peralatan yang sebaiknya disediakan:</p>
                <ol>
                    <li class="text-justify">Komputer Utama, dengan operating system Macintosh, Windows, ataupun Linux. Perusahaan pengembangan web profesional cenderung berbasis Macintosh.</li>
                    <li class="text-justify">Memori tambahan. sebaiknya computer memiliki memori yang besar, karena dalam pengembangan web cenderung kita akan membuka beberapa aplikasi sekaligus.</li>
                    <li class="text-justify">Sebuah monitor besar. Meskipun bukan keharusan, monitor besar memudahkan untuk desainer visual. Semakin besar monitor, semakin banyak jendela dan panel kontrol dapat terbuka pada waktu yang sama.</li>
                    <li class="text-justify">Perangkat mobile/smartphone. Saat ini banyak orang mengakses web melalui smartphone/tablet, sehingga kita perlu menguji penampilan dan kinerja situs web yang kita buat pada mobile browser pada smartphone atau perangkat tablet. Namun saat ini banyak web emulator yang dapat digunakan sebagai penguji web kita.</li>
                </ol>
                <!-- <div class="highlight">
                    <xmp><table>
<tr>
    <th>Month</th>
    <th>Savings</th>
</tr>
<tr>
    <td>January</td>
    <td>$100</td>
</tr>
</table></xmp>
                </div> -->
                
                <br>
                <br>
                <div class="row ftco-animate">
                    <!-- <div class="col-md-3"></div> -->
                    <!-- <div class="col-md-3"> -->
                        <!-- <center><p><a href="materi.html" class="btn btn-primary py-3">< Daftar materi</a></p></center> -->
                        <!-- <center><a href='plasma-flame.html' class="btn btn-primary btn-lg"><< Sebelumnya</a></center> -->
                    <!-- </div> -->
                    <div class="col-md-12">
                        <center><p><a href={{ url("materi/status/1/5/".((int)request()->segment(4)+1)) }} class="btn btn-primary py-3">Selanjutnya ></a></p></center>
                        <!-- <center><a href='persiapan-alat.html' class="btn btn-primary btn-lg">Materi Selanjutnya ></a></center> -->
                    </div>
                    <div class="col-md-3"></div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection