@extends('layouts.layout')

@section('content')
<section class="ftco-section bg-white">
    <div class="container">
        <br>
        <div class="row  mb-3 pb-md-3">
            <div class="col-md-12 heading-section ftco-animate">
                    <img src={{ asset("images/open-book.png") }} height="100px" style="margin: 40px" />
                    <img src={{ asset("images/server_sq.png") }} height="140px" />
                    @include('layouts.modul-breadcrumbs')
                <!-- <span class="subheading">Materi 1</span> -->
                <h2 class="mb-4">Modul 1 - Profesi dalam bidang Teknologi Informasi</h2>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 ftco-animate">
                @include('materi-'.request()->segment(2).'.modul-'.request()->segment(3).'.sidebar')
            </div>
            <div class="col-md-9 ftco-animate">
                <h2 style="font-weight: 700" class="mb-4 text-center">Hardware</h2>
                <p class="text-justify">Mereka yang bergelut di bidang perangkat keras (hardware). Pada lingkungan kelompok ini, terdapat pekerjaan-pekerjaan seperti: </p>
                <ol>
                    <div class="row">
                        <div class="col-md-3">
                            <center><img src={{ asset("images/undraw_bug_fixing_oc7a.svg") }} alt="" width="150px" class="img-responsive"></center>
                        </div>
                        <div class="col-md-9">
                            <li>
                                <p class="text-justify">Technical engineer, yaitu orang yang berkecimpung dalam bidang teknik, baik mengenai pemeliharaan maupun perbaikan perangkat system computer</p>
                            </li>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3">
                            <center><img src={{ asset("images/undraw_connected_world_wuay.svg") }} alt="" width="150px" class="img-responsive"></center>
                        </div>
                        <div class="col-md-9">
                            <li>
                                <p class="text-justify">Networking engineer, adalah orang yang berkecimpung dalam bidang teknis jaringan computer dari maintenance sampai pada troubleshooting-nya</p>
                            </li>
                        </div>
                    </div>
                </ol>
                <!-- <div class="highlight">
                    <xmp><table>
<tr>
    <th>Month</th>
    <th>Savings</th>
</tr>
<tr>
    <td>January</td>
    <td>$100</td>
</tr>
</table></xmp>
                </div> -->
                
                <br>
                <br>
                <div class="row ftco-animate">
                    <div class="col-md-3"></div>
                    <div class="col-md-3">
                        <center><p><a href={{ url("materi/1/1/2") }} class="btn btn-primary py-3">< Sebelumnya</a></p></center>
                    </div>
                    <div class="col-md-3">
                        <center><p><a href={{ url("materi/status/1/1/".((int)request()->segment(4)+1)) }} class="btn btn-primary py-3">Selanjutnya ></a></p></center>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection