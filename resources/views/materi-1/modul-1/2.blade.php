@extends('layouts.layout')

@section('content')
<section class="ftco-section bg-white">
    <div class="container">
        <br>
        <div class="row  mb-3 pb-md-3">
            <div class="col-md-12 heading-section ftco-animate">
                    <img src={{ asset("images/open-book.png") }} height="100px" style="margin: 40px" />
                    <img src={{ asset("images/server_sq.png") }} height="140px" />
                    @include('layouts.modul-breadcrumbs')
                <!-- <span class="subheading">Materi 1</span> -->
                <h2 class="mb-4">Modul 1 - Profesi dalam bidang Teknologi Informasi</h2>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 ftco-animate">
                @include('materi-'.request()->segment(2).'.modul-'.request()->segment(3).'.sidebar')
            </div>
            <div class="col-md-9 ftco-animate">
                <h2 style="font-weight: 700" class="mb-4 text-center">Web Programmer</h2>
                <p class="text-justify">Web Programmer bertugas dalam melakukan pengcodingan atau pemograman sebuah website agar dinamis. dimana agar sebuah web tersebut dapat telihat mudah bagi seorang web admin. Jika situs yang akan dibuat mempunya fasilitas interaksi antara pengunjung dan situs misalnya menyangkut dengan transaksi, input output data dan database maka seorang Web Programmer yang akan mengerjakannya dengan membuat aplikasi-aplikasi yang berkerja diatas situs (web). Penguasaan yang biasanya harus dikuasai pada umumnya oleh Web Programmer adalah CGI Perl, PHP, MySql, ASP, Java Script dan Applet.</p>
                <div class="row">
                    <div class="col-md-4 col-sm-6 text-center">
                        <img src={{ asset("images/php.svg") }} alt="" height="130px" style="padding: 5px">
                    </div>
                    <div class="col-md-4 col-sm-6 text-center">
                        <img src={{ asset("images/mysql.svg") }} alt="" height="130px" style="padding: 5px">
                    </div>
                    <div class="col-md-4 col-sm-6 text-center">
                        <img src={{ asset("images/javascript.svg") }} alt="" height="130px" style="padding: 5px">
                    </div>
                </div>
                <!-- <div class="highlight">
                    <xmp><table>
<tr>
    <th>Month</th>
    <th>Savings</th>
</tr>
<tr>
    <td>January</td>
    <td>$100</td>
</tr>
</table></xmp>
                </div> -->
                
                <br>
                <br>
                <div class="row ftco-animate">
                    <div class="col-md-3"></div>
                    <div class="col-md-3">
                        <center><p><a href={{ url("materi/1/1/1") }} class="btn btn-primary py-3">< Sebelumnya</a></p></center>
                    </div>
                    <div class="col-md-3">
                        <center><p><a href={{ url("materi/status/1/1/".((int)request()->segment(4)+1)) }} class="btn btn-primary py-3">Selanjutnya ></a></p></center>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection