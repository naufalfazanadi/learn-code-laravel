@extends('layouts.layout')

@section('content')
<section class="ftco-section bg-white">
    <div class="container">
        <br>
        <div class="row  mb-3 pb-md-3">
            <div class="col-md-12 heading-section ftco-animate">
                <img src={{ asset("images/open-book.png") }} height="100px" style="margin: 40px" />
                <img src={{ asset("images/server_sq.png") }} height="140px" />
                @include('layouts.modul-breadcrumbs')
                <!-- <span class="subheading">Materi 1</span> -->
                <h2 class="mb-4">Modul 3 - Sejarah dan cara kerja web</h2>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 ftco-animate">
                @include('materi-'.request()->segment(2).'.modul-'.request()->segment(3).'.sidebar')
            </div>
            <div class="col-md-9 ftco-animate">
                <h2 style="font-weight: 700" class="mb-4 text-center">Standard Web</h2>
                <p class="text-justify">HTML (Hypertext Markup Language) merupakan bahasa standar web yang didefinisikan dan dikelola penggunaannya oleh W3C (World Wide Web Consurtium). HTML dibuat dan dikembangkan oleh Tim Berners-Lee. Selanjutnya , bahasa tersebut dipopulerkan oleh browser Mosaic. Selama tahun 90-an telah muncul berbagai versi HTML dengan berbagai macam fiturnya. Versi HTML yang ada tersebut antara lain:</p>
                <ol>
                    <li>Versi 1.0<p class="text-justify">Merupakan versi pertama setelah lahirnya HTML dan mempunyai kemampuan seperti untuk heading, paragraph, hypertext, list, cetak tebal, dan cetak miring pada teks. Selain itu juga dukungan peletakan image pada dokumen dengan tidak memperbolehkan peletakan teks di sekelilingnya (wrapping).</p></li>
                    <li>Versi 2.0<p class="text-justify">Diliris pada tanggal 14 Januari 1996 dengan beberapa kemampuan tambahan seperti penambahan form. Hal ini menjadi pionir untuk membuat sebuah web yang interaktif.</p></li>
                    <li>Versi 3.0<p class="text-justify">Diliris pada tanggal 18 Desember 1997, yang dikenal dengan HTML+. Versi ini memiliki beberapa vitur tambahan seperti fitur table dalam paragraph.</p></li>
                    <li>Versi 3.2<p class="text-justify">Diliris pada buan Mei 1998, merupakan versi terbaru penyempurnaan dari versi 3.0.</p></li>
                    <li>Versi 4.0<p class="text-justify">Diliris pada tanggal 24 Desember 1999 dengan penambahan beberapa fitur seperti adanya link, imagemaps, image, dan lain-lain.</p></li>
                    <li>Versi 5.0<p class="text-justify">Diliris pada tahun 2009 yang menjadi standar baru untuk HTML, XHTML, dan DOM HTML. HTML5 ini merupakan proyek kerja sama antara W3C (World Wide Web Consurtium) dan WHATG (Web Hypertext Application Technology Working Group.</p></li>
                </ol>
                <!-- <div class="highlight">
                    <xmp><table>
<tr>
    <th>Month</th>
    <th>Savings</th>
</tr>
<tr>
    <td>January</td>
    <td>$100</td>
</tr>
</table></xmp>
                </div> -->
                
                <br>
                <br>
                <div class="row ftco-animate">
                    <div class="col-md-3"></div>
                    <div class="col-md-3">
                        <center><p><a href={{ url("materi/1/3/".((int)request()->segment(4)-1)) }} class="btn btn-primary py-3">< Sebelumnya</a></p></center>
                    </div>
                    <div class="col-md-3">
                        <center><p><a href={{ url("materi/status/1/3/".((int)request()->segment(4)+1)) }} class="btn btn-primary py-3">Selanjutnya ></a></p></center>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection