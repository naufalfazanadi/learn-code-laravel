<div class="list-group">
    <h3 style="font-weight: 600">Tahapan Modul</h3>
    @php $count = 11 @endphp
    <a href={{ url("materi/1/3/1") }} class="list-group-item list-group-item-action {{ request()->segment(4) == '1' ? 'active' : '' }}">Sejarah Web</a>
    @php $count++ @endphp
    <a href={{ url("materi/1/3/2") }} class="list-group-item list-group-item-action {{ request()->segment(4) == '2' ? 'active' : '' }} {{ $status->materi1 < $count ? 'disabled bg-light' : '' }}">Standard Web</a>
    @php $count++ @endphp
    <a href={{ url("materi/1/3/3") }} class="list-group-item list-group-item-action {{ request()->segment(4) == '3' ? 'active' : '' }} {{ $status->materi1 < $count ? 'disabled bg-light' : '' }}">Anatomi Browser</a>
    @php $count++ @endphp
    <!-- <a href="materi-1-modul-1-latihan.html" class="list-group-item list-group-item-action"><b>Latihan</b></a> -->
    <a href={{ url("materi/1/3/kuis") }} class="list-group-item list-group-item-action {{ request()->segment(4) == 'kuis' ? 'active' : '' }} {{ $status->materi1 < $count ? 'disabled bg-light' : '' }}"><b>Kuis</b></a>
</div>
