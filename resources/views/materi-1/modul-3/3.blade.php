@extends('layouts.layout')

@section('content')
<section class="ftco-section bg-white">
    <div class="container">
        <br>
        <div class="row  mb-3 pb-md-3">
            <div class="col-md-12 heading-section ftco-animate">
                <img src={{ asset("images/open-book.png") }} height="100px" style="margin: 40px" />
                <img src={{ asset("images/server_sq.png") }} height="140px" />
                @include('layouts.modul-breadcrumbs')
                <!-- <span class="subheading">Materi 1</span> -->
                <h2 class="mb-4">Modul 3 - Sejarah dan cara kerja web</h2>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 ftco-animate">
                @include('materi-'.request()->segment(2).'.modul-'.request()->segment(3).'.sidebar')
            </div>
            <div class="col-md-9 ftco-animate">
                <h2 style="font-weight: 700" class="mb-4 text-center">Anatomi Browser</h2>
                <p class="text-justify">World Wide Web sampai saat ini adalah bagian yang paling populer dari Internet. Setelah menghabiskan waktu di Web akan merasa bahwa tidak ada batas untuk apa yang dapat ditemukan. Web memungkinkan beragam komunikasi, juga memungkinkan untuk mengakses dan berinteraksi dengan teks, grafis, animasi, foto, audio dan video.</p>
                <p class="text-justify">Jadi apa saja yang membentuk web ini? Pada tingkat yang paling sederhana, Web secara fisik terdiri dari komputer pribadi atau perangkat mobile, perangkat lunak web browser, sambungan ke penyedia layanan Internet, komputer yang disebut server yang disebut hosting dan router dan switch yang mengarahkan aliran informasi.</p>
                <p class="text-justify">Sebuah halaman web adalah dokumen elektronik yang ditulis dalam bahasa komputer yang disebut HTML, singkatan dari Hypertext Markup Language. Setiap halaman web memiliki alamat yang unik, yang disebut URL atau Uniform Eerources Locator yang mengidentifikasi di mana web server yang memuat  dokumen web.</p>
                <p class="text-justify">Sebuah website memiliki satu atau lebih halaman web terkait, tergantung pada bagaimana itu dirancang. Halaman web pada situs yang dihubungkan bersama melalui sistem hyperlink, memungkinkan untuk melompat di antara halaman web dengan mengklik pada link.</p>
                <p class="text-justify">Halaman Web bervariasi dalam desain dan konten, tetapi banyak menggunakan format majalah tradisional. Di bagian atas halaman adalah “header” seperti kop surat atau gambar banner, kemudian daftar item dan artikel. Daftar item atau menu biasanya link ke halaman lain atau situs lain. Kadang-kadang link ini disusun dalam daftar, seperti indeks. Dapat juga kombinasi dari keduanya. Sebuah halaman web mungkin juga menggunakan gambar sebagai link ke konten lainnya.</p>
                <p class="text-justify">Web adalah media yang dinamis. Untuk mendorong pengunjung untuk kembali ke sebuah situs, banyak penerbit web sering memperbarui konten. Itulah yang membuat Web sangat menarik.</p>
                <p class="text-justify">World Wide Web adalah koleksi besar file elektronik yang tersimpan di jutaan komputer di seluruh dunia. Uniform Resource Locators atau URL adalah alamat yang digunakan untuk menemukan file. Informasi yang terkandung dalam URL memberi kemampuan untuk berpindah dari satu halaman web yang lain hanya dengan klik. Ketika mengetik URL ke browser atau klik link hypertext, browser mengirim permintaan ke komputer remote, yang disebut server web, untuk mendownload satu atau lebih file. Setiap URL unik, mengidentifikasi satu file tertentu.</p>
                <p class="text-justify">Sebuah URL lengkap umumnya terdiri dari tiga komponen : protokol, nama situs, dan path absolut ke dokumen atau sumber daya.</p>
                <ol>
                    <li>http://<p class="text-justify">Hal pertama yang dilakukan adalah URL menentukan protokol yang akan digunakan untuk transaksi tertentu. Aturan HTTP membiarkan server tahu untuk menggunakan Hypertext Transfer Protocol, atau masuk ke dalam "modus web"</p></li>
                    <li>www.example.com<p class="text-justify">Bagian berikutnya dari URL mengidentifikasi situs dengan nama domainnya. Dalam contoh ini, nama domain adalah example.com. www di bagian  awal adalah nama host tertentu di domain tersebut. Nama host " www " telah menjadi sebuah kesepakatan. Tetapi, kadang-kadang nama host dapat dihilangkan. Bisa lebih dari satu website di domain (kadang-kadang disebut subdomain). Misalnya, ada : development.example.com, clients.example.com, dan sebagainya.</p></li>
                    <li>/2012/samples/first.html<p class="text-justify">Ini adalah path absolut melalui direktori pada server ke dokumen HTML yang diminta, first.html. Kata-kata dipisahkan oleh garis miring adalah nama direktori, dimulai dengan direktori root dari host ( seperti yang ditunjukkan oleh awal /), URL pada Gambar 1.3. mengatakan ingin menggunakan protokol HTTP untuk terhubung ke server web di Internet disebut www.example.com dan meminta first.html dokumen ( terletak di direktori sampel yang ada di direktori 2012).</p></li>
                </ol>
                <!-- <div class="highlight">
                    <xmp><table>
<tr>
    <th>Month</th>
    <th>Savings</th>
</tr>
<tr>
    <td>January</td>
    <td>$100</td>
</tr>
</table></xmp>
                </div> -->
                
                <br>
                <br>
                <div class="row ftco-animate">
                    <div class="col-md-3"></div>
                    <div class="col-md-3">
                        <center><p><a href={{ url("materi/1/3/".((int)request()->segment(4)-1)) }} class="btn btn-primary py-3">< Sebelumnya</a></p></center>
                    </div>
                    <div class="col-md-3">
                        <center><p><a href={{ url("materi/status/1/3/".((int)request()->segment(4)+1)) }} class="btn btn-primary py-3">Selanjutnya ></a></p></center>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection