@extends('layouts.layout')

@section('content')
<section class="ftco-section bg-white">
    <div class="container">
        <br>
        <div class="row  mb-3 pb-md-3">
            <div class="col-md-12 heading-section ftco-animate">
                <img src={{ asset("images/open-book.png") }} height="100px" style="margin: 40px" />
                <img src={{ asset("images/server_sq.png") }} height="140px" />
                @include('layouts.modul-breadcrumbs')
                <!-- <span class="subheading">Materi 1</span> -->
                <h2 class="mb-4">Modul 2 - Profesi dalam pengembangan aplikasi web</h2>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 ftco-animate">
                @include('materi-'.request()->segment(2).'.modul-'.request()->segment(3).'.sidebar')
            </div>
            <div class="col-md-9 ftco-animate">
                <h2 style="font-weight: 700" class="mb-4 text-center">Web Designer</h2>
                <p class="text-justify">Seorang desainer Web adalah orang yang bertanggung jawab untuk menentukan tampilan sebuah website. Tugasnya adalah pendisainan tampilan situs (web) mulai dari pengolahan gambar, tata letak, warna, dan semua aspek visual situs. Fokus utama mereka adalah tampilan / layout dari web. Mereka lebih konsen dengan bagaimana halaman terlihat dan apakah berfungsi sempurna ketika sudah diberikan bahasa pemrograman. Didalam pendandanan suatu situs seorang Web Designer harus menguasai HTML, Pengolahan Gambar, Animasi dan Movie.</p>
                <div class="row">
                    <div class="col-md-2 col-sm-6 text-center">
                        <img src={{ asset("images/html-5.svg") }} alt="" height="130px" style="padding: 5px">
                    </div>
                    <div class="col-md-2 col-sm-6 text-center">
                        <img src={{ asset("images/css-3.svg") }} alt="" height="130px" style="padding: 5px">
                    </div>
                    <div class="col-md-2 col-sm-6 text-center">
                        <img src={{ asset("images/illustrator.svg") }} alt="" height="130px" style="padding: 5px">
                    </div>
                    <div class="col-md-2 col-sm-6 text-center">
                        <img src={{ asset("images/flash.svg") }} alt="" height="130px" style="padding: 5px">
                    </div>
                    <div class="col-md-2 col-sm-6 text-center">
                        <img src={{ asset("images/after-effects.png") }} alt="" height="130px" style="padding: 5px">
                    </div>
                    <div class="col-md-2 col-sm-6 text-center">
                        <img src={{ asset("images/photoshop.svg") }} alt="" height="130px" style="padding: 5px">
                    </div>
                </div>
                <!-- <div class="highlight">
                    <xmp><table>
<tr>
    <th>Month</th>
    <th>Savings</th>
</tr>
<tr>
    <td>January</td>
    <td>$100</td>
</tr>
</table></xmp>
                </div> -->
                
                <br>
                <br>
                <div class="row ftco-animate">
                    <!-- <div class="col-md-3"></div> -->
                    <!-- <div class="col-md-3"> -->
                        <!-- <center><p><a href="materi.html" class="btn btn-primary py-3">< Daftar materi</a></p></center> -->
                        <!-- <center><a href='plasma-flame.html' class="btn btn-primary btn-lg"><< Sebelumnya</a></center> -->
                    <!-- </div> -->
                    <div class="col-md-12">
                        <center><p><a href={{ url("materi/status/1/2/".((int)request()->segment(4)+1)) }} class="btn btn-primary py-3">Selanjutnya ></a></p></center>
                        <!-- <center><a href='persiapan-alat.html' class="btn btn-primary btn-lg">Materi Selanjutnya ></a></center> -->
                    </div>
                    <div class="col-md-3"></div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection