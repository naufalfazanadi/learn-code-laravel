@extends('layouts.layout')

@section('content')
<section class="ftco-section bg-white">
    <div class="container">
        <br>
        <div class="row  mb-3 pb-md-3">
            <div class="col-md-12 heading-section ftco-animate">
                <img src={{ asset("images/open-book.png") }} height="100px" style="margin: 40px" />
                <img src={{ asset("images/server_sq.png") }} height="140px" />
                @include('layouts.modul-breadcrumbs')
                <!-- <span class="subheading">Materi 1</span> -->
                <h2 class="mb-4">Modul 2 - Profesi dalam pengembangan aplikasi web</h2>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 ftco-animate">
                @include('materi-'.request()->segment(2).'.modul-'.request()->segment(3).'.sidebar')
            </div>
            <div class="col-md-9 ftco-animate">
                <h2 style="font-weight: 700" class="mb-4 text-center">Web Master</h2>
                <p class="text-justify">Web Master adalah seorang yang mengerti akan kesemua hal mulai dari disain, program dan keamanan server namun tidak terlalu turut mencampuri ke masing-masing divisi, cukup dengan mempertanggun jawabkan atas jalannya suatu situs (web). Penguasaan yang harus dimiliki HTML, PHP, MySql, ASP, Penguasaan OS, Keamanan Server dan Jaringan.</p>
                <div class="row">
                    <div class="col-md-4 col-sm-6 text-center">
                        <img src={{ asset("images/html-5.svg") }} alt="" height="130px" style="padding: 5px">
                    </div>
                    <div class="col-md-4 col-sm-6 text-center">
                        <img src={{ asset("images/linux.svg") }} alt="" height="130px" style="padding: 5px">
                    </div>
                    <div class="col-md-4 col-sm-6 text-center">
                        <img src={{ asset("images/laptop.svg") }} alt="" height="130px" style="padding: 5px">
                    </div>
                </div>
                <!-- <div class="highlight">
                    <xmp><table>
<tr>
    <th>Month</th>
    <th>Savings</th>
</tr>
<tr>
    <td>January</td>
    <td>$100</td>
</tr>
</table></xmp>
                </div> -->
                
                <br>
                <br>
                <div class="row ftco-animate">
                    <div class="col-md-3"></div>
                    <div class="col-md-3">
                        <center><p><a href={{ url("materi/1/2/".((int)request()->segment(4)-1)) }} class="btn btn-primary py-3">< Sebelumnya</a></p></center>
                    </div>
                    <div class="col-md-3">
                        <center><p><a href={{ url("materi/status/1/2/".((int)request()->segment(4)+1)) }} class="btn btn-primary py-3">Selanjutnya ></a></p></center>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection