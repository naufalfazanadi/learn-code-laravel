<div class="list-group">
    <h3 style="font-weight: 600">Tahapan Modul</h3>
    @php $count = 5 @endphp
    <a href={{ url("materi/1/2/1") }} class="list-group-item list-group-item-action {{ request()->segment(4) == '1' ? 'active' : '' }}">Web Designer</a>
    @php $count++ @endphp
    <a href={{ url("materi/1/2/2") }} class="list-group-item list-group-item-action {{ request()->segment(4) == '2' ? 'active' : '' }} {{ $status->materi1 < $count ? 'disabled bg-light' : '' }}">Web Programmer</a>
    @php $count++ @endphp
    <a href={{ url("materi/1/2/3") }} class="list-group-item list-group-item-action {{ request()->segment(4) == '3' ? 'active' : '' }} {{ $status->materi1 < $count ? 'disabled bg-light' : '' }}">Web Administrator</a>
    @php $count++ @endphp
    <a href={{ url("materi/1/2/4") }} class="list-group-item list-group-item-action {{ request()->segment(4) == '4' ? 'active' : '' }} {{ $status->materi1 < $count ? 'disabled bg-light' : '' }}">Web Master</a>
    @php $count++ @endphp
    <a href={{ url("materi/1/2/5") }} class="list-group-item list-group-item-action {{ request()->segment(4) == '5' ? 'active' : '' }} {{ $status->materi1 < $count ? 'disabled bg-light' : '' }}">Web Developer</a>
    @php $count++ @endphp
    <!-- <a href="materi-1-modul-1-latihan.html" class="list-group-item list-group-item-action"><b>Latihan</b></a> -->
    <a href={{ url("materi/1/2/kuis") }} class="list-group-item list-group-item-action {{ request()->segment(4) == 'kuis' ? 'active' : '' }} {{ $status->materi1 < $count ? 'disabled bg-light' : '' }}"><b>Kuis</b></a>
</div>
