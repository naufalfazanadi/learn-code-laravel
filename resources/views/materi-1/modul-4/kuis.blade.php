@extends('layouts.layout')

@section('content')
<section class="ftco-section bg-white">
    <div class="container">
        <br>
        <div class="row  mb-3 pb-md-3">
            <div class="col-md-12 heading-section ftco-animate">
                    <img src={{ asset("images/open-book.png") }} height="100px" style="margin: 40px" />
                    <img src={{ asset("images/server_sq.png") }} height="140px" />
                    @include('layouts.modul-breadcrumbs')
                <!-- <span class="subheading">Materi 1</span> -->
                <h2 class="mb-4">Modul 4 - Alur pengembangan aplikasi web</h2>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 ftco-animate">
                @include('materi-'.request()->segment(2).'.modul-'.request()->segment(3).'.sidebar')
            </div>
            <div class="col-md-9 ftco-animate">
                @if ($kuis == "")
                    <form action="/kuis" method="POST">
                        {{ csrf_field() }}
                        <h2 style="font-weight: 700" class="mb-4 text-center">Kuis</h2>
                        @if (session('status_repeat'))
                            <div class="alert alert-info">
                                {{ session('status_repeat') }}
                            </div>
                        @endif
                        <p>1. Tahap yang benar dalam pengembangan aplikasi web adalah . . .</p>
                        <div class="highlight">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no1" id="1a" value="a" required>
                                <label class="form-check-label" for="1a">
                                    a. Perencanaan, Analisis, Rekayasa, Implementasi dan Pengujian
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no1" id="1b" value="b">
                                <label class="form-check-label" for="1b">
                                    b. Analisis, Rekayasa, Perencanaan, Implementasi dan Pengujian
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no1" id="1c" value="c">
                                <label class="form-check-label" for="1c">
                                    c. Analisis, Rekayasa, Implementasi, Perencanaan dan Pengujian
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no1" id="1d" value="d">
                                <label class="form-check-label" for="1d">
                                    d. Perencanaan, Pengujian, Analisis, Rekayasa dan Implementasi
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no1" id="1e" value="e">
                                <label class="form-check-label" for="1e">
                                    e. Pengujian, Perencanaan, Analisis, Rekayasa dan Implementasi
                                </label>
                            </div>
                        </div>
                        <br>
                        <p>2. Ada empat jenis analisis pada rekayasa web yang salah satunya analisis interaksi, yang dimaksud dengan analisis interaksi adalah . . .</p>
                        <div class="highlight">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no2" id="2a" value="a" required>
                                <label class="form-check-label" for="2a">
                                    a. Mengidentifikasi isi yang akan ditampilkan pada aplikasi
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no2" id="2b" value="b">
                                <label class="form-check-label" for="2b">
                                    b. Menganalisis hubungan antara aplikasi dan pengguna
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no2" id="2c" value="c">
                                <label class="form-check-label" for="2c">
                                    c. Menganalisis proses aplikasi
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no2" id="2d" value="d">
                                <label class="form-check-label" for="2d">
                                    d. Mengatur keperluan yang akan digunakan pada aplikasi
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no2" id="2e" value="e">
                                <label class="form-check-label" for="2e">
                                    e. Membuat relasi database yang diperlukan aplikasi
                                </label>
                            </div>
                        </div>
                        <br>
                        <p>3. Yang perlu dilakukan pada tahap desain, kecuali . . .</p>
                        <div class="highlight">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no3" id="3a" value="a" required>
                                <label class="form-check-label" for="3a">
                                    a. Pembuatan Wireframe
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no3" id="3b" value="b">
                                <label class="form-check-label" for="3b">
                                    b. Pembuatan Diagram situs
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no3" id="3c" value="c">
                                <label class="form-check-label" for="3c">
                                    c. Pembuatan Storyboard
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no3" id="3d" value="d">
                                <label class="form-check-label" for="3d">
                                    d. Pembuatan Diagram alir
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no3" id="3e" value="e">
                                <label class="form-check-label" for="3e">
                                    e. Pembuatan Database
                                </label>
                            </div>
                        </div>
                        <br>
                        <br>
                        <div class="row ftco-animate">
                            <div class="col-md-3"></div>
                            <div class="col-md-3">
                                <center><p><a href={{ url("materi/1/4/6") }} class="btn btn-primary py-3">< Sebelumnya</a></p></center>
                                <!-- <center><a href='plasma-flame.html' class="btn btn-primary btn-lg"><< Sebelumnya</a></center> -->
                            </div>
                            <div class="col-md-3">
                                <input type="hidden" id="id" name="jumlahSoal" value="3">
                                <input type="hidden" id="id" name="materi" value={{ request()->segment(2) }}>
                                <input type="hidden" id="id" name="modul" value={{ request()->segment(3) }}>

                                <center><input type="submit" value="Kirim Jawaban" class="btn btn-success py-3 px-5"></center>
                                <!-- <center><a href='persiapan-alat.html' class="btn btn-primary btn-lg">Materi Selanjutnya ></a></center> -->
                            </div>
                            <div class="col-md-3"></div>
                        </div>
                    </form>
                @else
                    <div class="alert alert-success">
                        <h3 class="text-center">Kamu telah menyelesaikan kuis</h2>
                        <h2 style="font-weight: 700" class="mb-4 text-center">Nilai kamu : <b>{{ $kuis->nilai }}</b></h3>
                    </div>
                    <br>
                    <br>
                    <div class="row ftco-animate">
                        <div class="col-md-3"></div>
                        <div class="col-md-3">
                            <center><p><a href={{ url("materi/1/4/6") }} class="btn btn-primary py-3">< Sebelumnya</a></p></center>
                        </div>
                        <div class="col-md-3">
                            <center><p><a href={{ url("materi/1") }} class="btn btn-success py-3">Daftar modul</a></p></center>
                        </div>
                        <div class="col-md-3"></div>
                    </div>
                @endif
            </div>
        </div>
    </div>
</section>
@endsection