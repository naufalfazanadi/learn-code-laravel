<div class="list-group">
    <h3 style="font-weight: 600">Tahapan Modul</h3>
    @php $count = 15 @endphp
    <a href={{ url("materi/1/4/1") }} class="list-group-item list-group-item-action {{ request()->segment(4) == '1' ? 'active' : '' }}">Formulasi</a>
    @php $count++ @endphp
    <a href={{ url("materi/1/4/2") }} class="list-group-item list-group-item-action {{ request()->segment(4) == '2' ? 'active' : '' }} {{ $status->materi1 < $count ? 'disabled bg-light' : '' }}">Perencanaan</a>
    @php $count++ @endphp
    <a href={{ url("materi/1/4/3") }} class="list-group-item list-group-item-action {{ request()->segment(4) == '3' ? 'active' : '' }} {{ $status->materi1 < $count ? 'disabled bg-light' : '' }}">Analisis</a>
    @php $count++ @endphp
    <a href={{ url("materi/1/4/4") }} class="list-group-item list-group-item-action {{ request()->segment(4) == '4' ? 'active' : '' }} {{ $status->materi1 < $count ? 'disabled bg-light' : '' }}">Rekayasa</a>
    @php $count++ @endphp
    <a href={{ url("materi/1/4/5") }} class="list-group-item list-group-item-action {{ request()->segment(4) == '5' ? 'active' : '' }} {{ $status->materi1 < $count ? 'disabled bg-light' : '' }}">Implementasi dan Pengujian</a>
    @php $count++ @endphp
    <a href={{ url("materi/1/4/6") }} class="list-group-item list-group-item-action {{ request()->segment(4) == '6' ? 'active' : '' }} {{ $status->materi1 < $count ? 'disabled bg-light' : '' }}">Evaluasi oleh konsumen</a>
    @php $count++ @endphp
    <!-- <a href="materi-1-modul-1-latihan.html" class="list-group-item list-group-item-action"><b>Latihan</b></a> -->
    <a href={{ url("materi/1/4/kuis") }} class="list-group-item list-group-item-action {{ request()->segment(4) == 'kuis' ? 'active' : '' }} {{ $status->materi1 < $count ? 'disabled bg-light' : '' }}"><b>Kuis</b></a>
</div>
