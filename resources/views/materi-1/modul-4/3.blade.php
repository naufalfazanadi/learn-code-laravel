@extends('layouts.layout')

@section('content')
<section class="ftco-section bg-white">
    <div class="container">
        <br>
        <div class="row  mb-3 pb-md-3">
            <div class="col-md-12 heading-section ftco-animate">
                <img src={{ asset("images/open-book.png") }} height="100px" style="margin: 40px" />
                <img src={{ asset("images/server_sq.png") }} height="140px" />
                @include('layouts.modul-breadcrumbs')
                <!-- <span class="subheading">Materi 1</span> -->
                <h2 class="mb-4">Modul 4 - Alur pengembangan aplikasi web</h2>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 ftco-animate">
                @include('materi-'.request()->segment(2).'.modul-'.request()->segment(3).'.sidebar')
            </div>
            <div class="col-md-9 ftco-animate">
                <h2 style="font-weight: 700" class="mb-4 text-center">Analisis</h2>
                <p class="text-justify">Kegiatan untuk menentukan persyaratan-persyaratan teknik dan mengidentifikasi informasi yang akan ditampilkan pada aplikasi berbasis web. Analisis yang digunakan pada rekayasa web dilakukan dari empat sisi, yaitu:</p>
                <ol>
                    <li>Analisis isi informasi<p class="text-justify">Mengidentifikasi isi yang akan ditampilkan pada aplikasi berbasis web ini. Isi informasi dapat berupa teks, grafik, audio, maupun video.</p></li>
                    <li>Analisis interaksi<p class="text-justify">Analisis yang menunjukkan hubungan antara web dengan pengguna.</p></li>
                    <li>Analisis fungsional<p class="text-justify">Analisis tentang proses bagaimana aplikasi berbasis web ini akan menampilkan informasi kepada pengguna.</p></li>
                    <li>Analisis konfigurasi<p class="text-justify">Konfigurasi yang digunakan pada aplikasi berbasis web, internet, intranet, atau extranet. Selain itu, analisis ini juga meliputi relasi database dengan web jika diperlukan.</p></li>
                </ol>
                <!-- <div class="highlight">
                    <xmp><table>
<tr>
    <th>Month</th>
    <th>Savings</th>
</tr>
<tr>
    <td>January</td>
    <td>$100</td>
</tr>
</table></xmp>
                </div> -->
                
                <br>
                <br>
                <div class="row ftco-animate">
                    <div class="col-md-3"></div>
                    <div class="col-md-3">
                        <center><p><a href={{ url("materi/1/4/".((int)request()->segment(4)-1)) }} class="btn btn-primary py-3">< Sebelumnya</a></p></center>
                    </div>
                    <div class="col-md-3">
                        <center><p><a href={{ url("materi/status/1/4/".((int)request()->segment(4)+1)) }} class="btn btn-primary py-3">Selanjutnya ></a></p></center>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection