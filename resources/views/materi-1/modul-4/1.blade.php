@extends('layouts.layout')

@section('content')
<section class="ftco-section bg-white">
    <div class="container">
        <br>
        <div class="row  mb-3 pb-md-3">
            <div class="col-md-12 heading-section ftco-animate">
                <img src={{ asset("images/open-book.png") }} height="100px" style="margin: 40px" />
                <img src={{ asset("images/server_sq.png") }} height="140px" />
                @include('layouts.modul-breadcrumbs')
                <!-- <span class="subheading">Materi 1</span> -->
                <h2 class="mb-4">Modul 4 - Alur pengembangan aplikasi web</h2>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 ftco-animate">
                @include('materi-'.request()->segment(2).'.modul-'.request()->segment(3).'.sidebar')
            </div>
            <div class="col-md-9 ftco-animate">
                <h2 style="font-weight: 700" class="mb-4 text-center">Formulasi</h2>
                <p class="text-justify">Kegiatan yang berfungsi untuk merumuskan tujuan dan ukuran dari aplikasi berbasis web serta menentukan batasannya sistem. Tujuan yang ingin dicapai bisa dibedakan menjadi dua kategori, yaitu:</p>
                <ol>
                    <li>Tujuan yang bersifat informatif<p class="text-justify">Menyediakan suatu informasi tertentu kepada pengguna, berupa teks, grafik, audio, dan video.</p></li>
                    <li>Tujuan yang bersifat fungsional<p class="text-justify">Kemampuan untuk melakukan suatu fungsi yang dibutuhkan pengguna, misal dengan menggunakan aplikasi tersebut seorang guru dapat memperoleh nilai akhir dan statistik nilai guru dari data-data ujian, tugas, kuis yang ia input ke dalam aplikasi.</p></li>
                </ol>
                <!-- <div class="highlight">
                    <xmp><table>
<tr>
    <th>Month</th>
    <th>Savings</th>
</tr>
<tr>
    <td>January</td>
    <td>$100</td>
</tr>
</table></xmp>
                </div> -->
                
                <br>
                <br>
                <div class="row ftco-animate">
                    <!-- <div class="col-md-3"></div> -->
                    <!-- <div class="col-md-3"> -->
                        <!-- <center><p><a href="materi.html" class="btn btn-primary py-3">< Daftar materi</a></p></center> -->
                        <!-- <center><a href='plasma-flame.html' class="btn btn-primary btn-lg"><< Sebelumnya</a></center> -->
                    <!-- </div> -->
                    <div class="col-md-12">
                        <center><p><a href={{ url("materi/status/1/4/".((int)request()->segment(4)+1)) }} class="btn btn-primary py-3">Selanjutnya ></a></p></center>
                        <!-- <center><a href='persiapan-alat.html' class="btn btn-primary btn-lg">Materi Selanjutnya ></a></center> -->
                    </div>
                    <div class="col-md-3"></div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection