@extends('layouts.layout')

@section('content')
<section class="ftco-section bg-white">
    <div class="container">
        <br>
        <div class="row  mb-3 pb-md-3">
            <div class="col-md-12 heading-section ftco-animate">
                <img src={{ asset("images/open-book.png") }} height="100px" style="margin: 40px" />
                <img src={{ asset("images/server_sq.png") }} height="140px" />
                @include('layouts.modul-breadcrumbs')
                <!-- <span class="subheading">Materi 1</span> -->
                <h2 class="mb-4">Modul 4 - Alur pengembangan aplikasi web</h2>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 ftco-animate">
                @include('materi-'.request()->segment(2).'.modul-'.request()->segment(3).'.sidebar')
            </div>
            <div class="col-md-9 ftco-animate">
                <h2 style="font-weight: 700" class="mb-4 text-center">Implementasi dan Pengujian</h2>
                <p class="text-justify">Suatu kegiatan pengembangan untuk mewujudkan desain menjadi suatu web site. Teknologi yang digunakan tergantung dengan kebutuhan yang telah dirumuskan pada tahap analisis. Dalam tahap pengembangan ini, meliputi kegiatan:</p>
                <ol>
                    <li class="text-justify">Authoring, adalah istilah yang digunakan untuk proses mempersiapkan konten untuk nantinya diunggah di Web, atau lebih khusus, menandai konten dengan tag HTML yang menggambarkan isi dan fungsinya.</li>
                    <li class="text-justify">Styling, dalam development web, penampilan halaman dalam browser dikendalikan oleh aturan style yang ditulis dalam CSS (Cascading Style Sheets).</li>
                    <li class="text-justify">Scripting dan pemrograman, ada bahasa pemrograman web terkait diantaranya PHP, Ruby, Python, dan ASP.NET, yang berjalan pada server dan mengolah data dan informasi sebelum dikirim ke browser pengguna.</li>
                </ol>
                <p class="text-justify">Pengujian dilakukan setelah implementasi selesai dilaksanakan. Pengujian meliputi beberapa parameter yang akan menentukan standar aplikasi berbasis web yang telah dibuat. Tahap pengujian adalah suatu proses untuk menguji aplikasi berbasis web yang telah selesai dibuat. Hal ini bertujuan untuk menemukan kesalahan dan kemudian memperbaikinya. Pengembang suatu aplikasi berbasis web mendapat tantangan besar untuk melakukan pengujian karena karakter aplikasi ini yang beroperasi pada jaringan dengan berbagai macam pengguna, berbagai macam sistem operasi, perangkat keras, browser, protokol komunikasi, dll.</p>
                <!-- <div class="highlight">
                    <xmp><table>
<tr>
    <th>Month</th>
    <th>Savings</th>
</tr>
<tr>
    <td>January</td>
    <td>$100</td>
</tr>
</table></xmp>
                </div> -->
                
                <br>
                <br>
                <div class="row ftco-animate">
                    <div class="col-md-3"></div>
                    <div class="col-md-3">
                        <center><p><a href={{ url("materi/1/4/".((int)request()->segment(4)-1)) }} class="btn btn-primary py-3">< Sebelumnya</a></p></center>
                    </div>
                    <div class="col-md-3">
                        <center><p><a href={{ url("materi/status/1/4/".((int)request()->segment(4)+1)) }} class="btn btn-primary py-3">Selanjutnya ></a></p></center>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection