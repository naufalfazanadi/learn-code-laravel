@extends('layouts.layout')

@section('content')
<section class="ftco-section bg-white">
    <div class="container">
        <br>
        <div class="row  mb-3 pb-md-3">
            <div class="col-md-12 heading-section ftco-animate">
                <img src={{ asset("images/open-book.png") }} height="100px" style="margin: 40px" />
                <img src={{ asset("images/server_sq.png") }} height="140px" />
                @include('layouts.modul-breadcrumbs')
                <!-- <span class="subheading">Materi 1</span> -->
                <h2 class="mb-4">Modul 4 - Alur pengembangan aplikasi web</h2>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 ftco-animate">
                @include('materi-'.request()->segment(2).'.modul-'.request()->segment(3).'.sidebar')
            </div>
            <div class="col-md-9 ftco-animate">
                <h2 style="font-weight: 700" class="mb-4 text-center">Rekayasa</h2>
                <p class="text-justify">Terdapat dua pekerjaan yang dilakukan secara paralel, yaitu desain isi informasi dan desain arsitektur web. Pada saat tahap desain, ada beberapa hal yang perlu dilakukan:</p>
                <ol>
                    <li class="text-justify">Pembuatan Diagram Wireframe (Gambar Rangka), tujuan dari diagram gambar rangka adalah untuk menunjukkan bagaimana lay out halaman web dan menunjukkan di mana fungsi dan konten seperti navigasi, kotak pencarian, elemen bentuk dan sebagainya, tanpa desain grafis.</li>
                    <li class="text-justify">Diagram Situs, Sebuah diagram situs menunjukkan struktur situs secara keseluruhan dan bagaimana halaman individual berhubungan satu sama lain.</li>
                    <li class="text-justify">Storyboard dan diagram alir pengguna, Storyboard ini bertujuan untuk menunjukkan langkah-langkah yang diperlukan untuk menyelesaikan tugas-tugas, opsi yang mungkin, dan juga memperkenalkan beberapa standar jenis halaman.</li>
                </ol>
                <!-- <div class="highlight">
                    <xmp><table>
<tr>
    <th>Month</th>
    <th>Savings</th>
</tr>
<tr>
    <td>January</td>
    <td>$100</td>
</tr>
</table></xmp>
                </div> -->
                
                <br>
                <br>
                <div class="row ftco-animate">
                    <div class="col-md-3"></div>
                    <div class="col-md-3">
                        <center><p><a href={{ url("materi/1/4/".((int)request()->segment(4)-1)) }} class="btn btn-primary py-3">< Sebelumnya</a></p></center>
                    </div>
                    <div class="col-md-3">
                        <center><p><a href={{ url("materi/status/1/4/".((int)request()->segment(4)+1)) }} class="btn btn-primary py-3">Selanjutnya ></a></p></center>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection