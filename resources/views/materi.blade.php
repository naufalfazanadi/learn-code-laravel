@extends('layouts.layout')

@section('content')
<div class="hero-wrap hero-wrap-2" style="background-image: url('images/image_4.jpg');" data-stellar-background-ratio="0.5">
    <div class="overlay"></div>
    <div class="container">
        <div class="row no-gutters slider-text align-items-end justify-content-start">
            <div class="col-md-8 ftco-animate text-center text-md-left mb-5">
                <p class="breadcrumbs mb-0"><span class="mr-3"><a href={{ url('/') }}> Beranda <i class="ion-ios-arrow-forward"></i></a></span> <span>Daftar Materi</span></p>
                <h1 class="mb-3 bread">Daftar Materi</h1>
            </div>
        </div>
    </div>
</div>

<section class="ftco-section bg-light">
    <div class="container">
        <div class="row justify-content-center mb-5 pb-3">
            <div class="col-md-7 text-center heading-section ftco-animate">
                <h2 class="mb-4">List Daftar Materi</h2>
            </div>
        </div>
        <div class="row d-flex">
            <div class="col-lg-6 col-md-6 ftco-animate">
                <div class="block-7">
                    <div class="text-center">
                        <!-- <h2 class="heading">Free</h2> -->
                        <img src={{ asset("images/server_sq.png") }} height="140px" />
                        <br>
                        <br>
                        <span class="price"><span class="number" style="font-size: 36px">1. Teknologi Aplikasi Web</span></span>
                        <!-- <span class="excerpt d-block">100% free. Forever</span> -->
                        <br>
                        <h3 class="heading-2 mb-3">Memahami konsep dan beragam teknologi aplikasi web.</h3>
                        
                        <span class="excerpt d-block">Materi mencakup:</span>
                        <ul class="pricing-text mb-4">
                            <li><strong>Profesi dalam teknologi informasi</strong></li>
                            <li><strong>Profesi dalam pengembangan aplikasi web</strong></li>
                            <li><strong>Sejarah dan cara kerja web</strong></li>
                            <!-- <li><strong>dan lain lain</strong></li> -->
                            <!-- <li>All features</li> -->
                            <li class="excerpt d-block">dan lain lain.</li>
                        </ul>
                        <a href={{ url("materi/1") }} class="btn btn-primary d-block px-3 py-3 mb-4">Mulai Belajar</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 ftco-animate">
                <div class="block-7">
                    <div class="text-center">
                        <img src={{ asset("images/article_sq.png") }} height="140px" />
                        <br>
                        <br>
                        <span class="price"><span class="number" style="font-size: 36px">2. Format Teks</span></span>
                        <br>
                        <h3 class="heading-2 mb-3">Memahami dan menyajikan format teks dalam halaman web.</h3>
                        
                        <span class="excerpt d-block">Materi mencakup:</span>
                        <ul class="pricing-text mb-4">
                            <li><strong>Anatomi dokumen web</strong></li>
                            <li><strong>Pemformatan teks dan paragraph</strong></li>
                            <li><strong>Pembuatan list minimal</strong></li>
                            <li class="excerpt d-block">dan lain lain.</li>
                        </ul>
                        <a href={{ url("materi/2") }} class="btn btn-primary d-block px-3 py-3 mb-4">Mulai Belajar</a>
                    </div>
                </div>
            </div>
        </div>
        <br/>
        <div class="row d-flex">
            <div class="col-lg-6 col-md-6 ftco-animate">
                <div class="block-7">
                    <div class="text-center">
                        <img src={{ asset("images/table_sq.png") }} height="140px" />
                        <br>
                        <br>
                        <span class="price"><span class="number" style="font-size: 36px">3. Format Tabel</span></span>
                        <br>
                        <h3 class="heading-2 mb-3">Memahami dan menyajikan tabel dalam halaman web.</h3>
                        
                        <span class="excerpt d-block">Materi mencakup:</span>
                        <ul class="pricing-text mb-4">
                            <li><strong>Anatomi table minimal</strong></li>
                            <li><strong>Table dengan spaning</strong></li>
                            <li><strong>Table di dalam table</strong></li>
                            <li class="excerpt d-block">dan lain lain.</li>
                        </ul>
                        <a href={{ url("materi/3") }} class="btn btn-primary d-block px-3 py-3 mb-4">Mulai Belajar</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 ftco-animate">
                <div class="block-7">
                    <div class="text-center">
                        <img src={{ asset("images/social-media_sq.png") }} height="140px" />
                        <br>
                        <br>
                        <span class="price"><span class="number" style="font-size: 36px">4. Multimedia Web</span></span>
                        <br>
                        <h3 class="heading-2 mb-3">Memahami dan menyajikan tampilan format multimedia.</h3>
                        
                        <span class="excerpt d-block">Materi mencakup:</span>
                        <ul class="pricing-text mb-4">
                            <li><strong>Format tampilan gambar</strong></li>
                            <li><strong>Format tampilan file audio</strong></li>
                            <li><strong>Format tampilan video dan animasi</strong></li>
                            <!-- <li><strong>Tabel</strong></li> -->
                            <li class="excerpt d-block">dan lain lain.</li>
                        </ul>
                        <a href={{ url("materi/4") }} class="btn btn-primary d-block px-3 py-3 mb-4">Mulai Belajar</a>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="row d-flex">
            <div class="col-lg-12 col-md-12 ftco-animate">
                <div class="block-7">
                    <div class="text-center">
                        <img src={{ asset("images/evaluation.png") }} height="140px" />
                        <br>
                        <br>
                        <span class="price"><span class="number" style="font-size: 36px">5. Evaluasi</span></span>
                        <h3 class="heading-2 mb-3">Evaluasi seluruh materi untuk siswa.</h3>
                        <br>
                        <a href={{ url("materi/evaluasi") }} class="btn btn-primary d-block px-3 py-3 mb-4">Mulai Kerjakan</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection