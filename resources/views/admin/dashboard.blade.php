@extends('layouts.admin')

@section('content')
<style>
    @media (min-width: 768px ) {
        .bottom-align-text {
            position: absolute;
            bottom: 0;
            right: 0;
        }
    }
</style>
<section class="ftco-section bg-light">
    <div class="container">
        <br>
        <div class="row" style="position: relative">
            <div class="col-md-3 heading-section ftco-animate text-center">
                <img src={{ asset("images/undraw_all_the_data_h4ki.svg") }} height="100px" style="margin: 25px" class="img-responsive" />
                <!-- <span class="subheading">Materi 1</span> -->
            </div>
            <div class="col-md-9 heading-section ftco-animate bottom-align-text">
                <h2 class="">Dashboard - {{ Auth::user()->nama }}</h2>
                <hr>
                <p class="mb-4">{{ Auth::user()->email }}</p>
            </div>
        </div>
        <br>
        <br>
        <div class="row">
            <div class="col-md-12 nav-link-wrap mb-5 pb-md-5 pb-sm-1 ftco-animate">
                <div class="nav ftco-animate nav-pills justify-content-center text-center" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                    <a class="nav-link active" id="v-pills-materi-tab" href="/admin">Progress Materi</a>
        
                    <a class="nav-link" id="v-pills-praktik-tab"  href="/admin/praktik" >Nilai Praktik</a>
                    
                    <a class="nav-link" id="v-pills-kuis-tab" href="/admin/kuis">Nilai Kuis</a>
                        
                    {{-- <a class="nav-link" id="v-pills-tugas-tab" href="/admin/tugas">Nilai Tugas</a> --}}
                    
                    <a class="nav-link" id="v-pills-latihan-koding-tab" href="/admin/latihan-koding">Latihan Koding</a>
                </div>
            </div>
            <div class="col-md-12 align-items-center ftco-animate">
                <div class="tab-content ftco-animate" id="v-pills-tabContent">
                    <div class="tab-pane fade show active" id="v-pills-materi" role="tabpanel" aria-labelledby="v-pills-materi-tab">
                        <div class="d-md-flex">
                            <div class="col-md-3 align-self-start">
                                <img src={{ asset("images/undraw_percentages_0rur.svg") }} class="img-fluid" alt="">
                            </div>
                            <div class="col-md-9 ftco-animate">
                                {{-- <h1 class="text-center" style="font-weight: 700">Tugas Siswa</h1> --}}
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead class="thead-primary">
                                            <tr>
                                                <th>No</th>
                                                <th>Nama</th>
                                                <th>Materi 1</th>
                                                <th>Materi 2</th>
                                                <th>Materi 3</th>
                                                <th>Materi 4</th>
                                                <th>Progress Keseluruhan</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                                @php $i = 1 + ($users->currentPage() * $users->perPage()) - $users->perPage() @endphp
                                            @foreach($users as $user)
                                                @if(count($user->statusMateri) != 0)
                                                <tr>
                                                    <td>{{ $i }}</td>
                                                    <td class="text-left">{{ $user->nama }}</td>
                                                    <td>
                                                        {{ $user->statusMateri[0]->materi1 }} / 25
                                                    </td>
                                                    <td>
                                                        {{ $user->statusMateri[0]->materi2 }} / 15
                                                    </td>
                                                    <td>
                                                        {{ $user->statusMateri[0]->materi3 }} / 13
                                                    </td>
                                                    <td>
                                                        {{ $user->statusMateri[0]->materi4 }} / 15
                                                    </td>
                                                    <td class="color">
                                                        @php $progress =  $user->statusMateri[0]->materi1 + $user->statusMateri[0]->materi2 + $user->statusMateri[0]->materi3 + $user->statusMateri[0]->materi4 / 68 * 100 @endphp
                                                        <div class="progress" style="height:30px">
                                                            <div class="progress-bar progress-bar-striped progress-bar-animated {{ $progress == 100 ? 'bg-info' : 'bg-success' }} " style="width:{{ round($progress) }}%; height: 30px; {{ $progress == 0 ? 'color: #020202;' : '' }}">{{ round($progress) }}%</div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                @endif
                                                @php $i++ @endphp
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <nav>
                                        <ul class="pagination justify-content-end">
                                            {{$users->links('vendor.pagination.bootstrap-4')}}
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection