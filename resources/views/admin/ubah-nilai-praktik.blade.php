@extends('layouts.admin')

@section('content')
<style>
    @media (min-width: 768px ) {
        .bottom-align-text {
            position: absolute;
            bottom: 0;
            right: 0;
        }
    }
</style>
<section class="ftco-section bg-light">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h4 class="heading ftco-animate" style="font-weight: 700">Nama : {{ $praktik->user->nama }} </h4>
                <h4 class="heading ftco-animate" style="font-weight: 700">Studi Kasus</h4>
                <div class="form-group">
                    <textarea name="analisis" disabled id="" rows="8" class="textarea-live1 form-control innerbox" placeholder="" style="" required>{{ $praktik->studi_kasus }}</textarea>
                </div>
                <h4 class="heading ftco-animate" style="font-weight: 700">Studi Kasus Ulang</h4>
                <div class="form-group">
                    <textarea name="analisis" disabled id="" rows="8" class="textarea-live1 form-control innerbox" placeholder="" style="" required>{{ $praktik->studi_kasus_akhir }}</textarea>
                </div>
                <h4 class="heading ftco-animate" style="font-weight: 700">Kode HTML</h4>
                <div class="form-group">
                    <textarea name="code" id="" rows="19" class="textarea-live1 form-control innerbox html" placeholder="" style="font-size:16px;font-family:Consolas;color:#003399;white-space:pre-wrap;tab-size: 2;" disabled>{{ $praktik->code }}</textarea>
                </div>
            </div>
            <div class="col-md-12">
                <h4 style="font-weight: 700">Nilai :</h4>
            </div>
            <div class="col-md-8">
                <form action="/admin/praktik/ubah-nilai" method="POST">
                    {{ csrf_field() }}
                    <div class="form-group">
                        @if($praktik->nilai == null)
                            <input name="nilai" type="number" class="form-control" placeholder="Nilai" width="100%" required>
                        @else
                            <input name="nilai" type="number" class="form-control" placeholder="Nilai" width="100%" required value="{{ $praktik->nilai }}">
                        @endif
                    </div>
                    <input type="hidden" id="id" name="id" value="{{ $praktik->id }}">
                    <a href="/admin/praktik" class="btn btn-success py-3 px-5">Kembali</a>
                    <input type="submit" value="Ubah Nilai" class="btn btn-primary py-3 px-5">
                </form>
            </div>
        </div>
    </div>
</section>
@endsection     