@extends('layouts.admin')

@section('content')
<style>
    @media (min-width: 768px ) {
        .bottom-align-text {
            position: absolute;
            bottom: 0;
            right: 0;
        }
    }
</style>
<section class="ftco-section bg-light">
    <div class="container">
        <br>
        <div class="row" style="position: relative">
            <div class="col-md-3 heading-section ftco-animate text-center">
                <img src={{ asset("images/undraw_all_the_data_h4ki.svg") }} height="100px" style="margin: 25px" class="img-responsive" />
                <!-- <span class="subheading">Materi 1</span> -->
            </div>
            <div class="col-md-9 heading-section ftco-animate bottom-align-text">
                <h2 class="">Tugas Guru - {{ Auth::user()->nama }}</h2>
                <hr>
                <p class="mb-4">{{ Auth::user()->email }}</p>
            </div>
        </div>
        <br>
        <br>
        <div class="row">
            <div class="col-md-6">
                <div class="table-responsive">
                    <table class="table">
                        <thead class="thead-primary">
                            <tr>
                                <th>No</th>
                                <th>Materi</th>
                                <th>Judul Tugas</th>
                                <th>Soal</th>
                                <th>Deadline</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $i = 1 + ($tugasGuru->currentPage() * $tugasGuru->perPage()) - $tugasGuru->perPage() @endphp
                            @foreach ($tugasGuru as $val)
                                @php
                                    $date = new Date($val->deadline);
                                    $resultDate = $date->format('l, d F Y H:i:s');   
                                @endphp
                                <tr>
                                    <td>{{ $i }}</td>
                                    <td>{{ $val->materi }}</td>
                                    <td>{{ $val->judul_tugas }}</td>
                                    <td>{{ $val->soal }}</td>
                                    <td>{{ $resultDate }}</td>
                                </tr>
                            @endforeach
                            {{-- @php $i = 1 + ($users->currentPage() * $users->perPage()) - $users->perPage() @endphp
                            @foreach($users as $user)
                                @php $count = 1 @endphp
                                @if($count == 1)
                                <tr>
                                    <td rowspan="{{ count($user->tugasSiswa) == 0 ? '1' : count($user->tugasSiswa) }}">{{ $i }}</td>
                                    <td class="text-left" rowspan="{{ count($user->tugasSiswa) == 0 ? '1' : count($user->tugasSiswa) }}">{{ $user->nama }}</td>
                                @endif
                                @if(count($user->tugasSiswa) != 0)
                                    @foreach ($user->tugasSiswa as $tugasSiswa)
                                        @if($count == 1)
                                            @php $count = 0 @endphp
                                        @endif
                                            <td>
                                                {{ $tugasSiswa->tugasGuru->id }}
                                            </td>
                                            <td>
                                                {{ $tugasSiswa->tugasGuru->judul_tugas }}
                                            </td>
                                            <td class="text-left">
                                                {{ $tugasSiswa->code }}
                                            </td>
                                            <td>
                                                {{ $tugasSiswa->nilai == null ? '-' : $tugasSiswa->nilai }}
                                            </td>
                                            <td>
                                                <a href="/admin/tugas/ubah-nilai/{{ $tugasSiswa->id }}" class="btn btn-primary">Ubah Nilai</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <td>
                                        -
                                    </td>
                                    <td>
                                        -
                                    </td>
                                    <td>
                                        -
                                    </td>
                                    <td>
                                        -
                                    </td>
                                    <td>
                                        <a href="" class="btn btn-primary disabled">Ubah Nilai</a>
                                    </td>
                                </tr>
                                @endif
                                @php $i++ @endphp
                            @endforeach --}}
                        </tbody>
                    </table>
                    {{-- <nav>
                        <ul class="pagination justify-content-end">
                            {{$tugasGuru->links('vendor.pagination.bootstrap-4')}}
                        </ul>
                    </nav> --}}
                </div>
            </div>
            <div class="col-md-6">
                <h3 style="font-weight: 700" class="mb-4">Tambah Tugas</h3>
                <form action="/admin/tugas-guru" method="POST">
                    {{ csrf_field() }}
                    <div class="form-group">
                        Materi:<br>
                        <input name="materi" type="number" class="form-control" placeholder="Materi" width="100%" required>
                    </div>
                    <div class="form-group">
                        Judul Tugas:<br>
                        <input name="judul_tugas" type="text" class="form-control" placeholder="Judul Tugas" required>
                    </div>
                    <div class="form-group">
                        Soal:<br>
                        <textarea name="soal" id="" cols="30" rows="7" class="form-control" placeholder="Soal" required></textarea>
                    </div>
                    <div class="form-group">
                        Deadline:<br>
                        <input type="date" name="deadline" required>
                    </div>
                    <input type="submit" value="Tambah Tugas" class="btn btn-primary py-3 px-5">
                </form>
            </div>
        </div>
    </div>
</section>
@endsection