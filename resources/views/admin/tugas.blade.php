@extends('layouts.admin')

@section('content')
<style>
    @media (min-width: 768px ) {
        .bottom-align-text {
            position: absolute;
            bottom: 0;
            right: 0;
        }
    }
</style>
<section class="ftco-section bg-light">
    <div class="container">
        <br>
        <div class="row" style="position: relative">
            <div class="col-md-3 heading-section ftco-animate text-center">
                <img src={{ asset("images/undraw_all_the_data_h4ki.svg") }} height="100px" style="margin: 25px" class="img-responsive" />
                <!-- <span class="subheading">Materi 1</span> -->
            </div>
            <div class="col-md-9 heading-section ftco-animate bottom-align-text">
                <h2 class="">Dashboard - {{ Auth::user()->nama }}</h2>
                <hr>
                <p class="mb-4">{{ Auth::user()->email }}</p>
            </div>
        </div>
        <br>
        <br>
        <div class="row">
            <div class="col-md-12 nav-link-wrap mb-5 pb-md-5 pb-sm-1 ftco-animate">
                <div class="nav ftco-animate nav-pills justify-content-center text-center" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                    <a class="nav-link" id="v-pills-materi-tab" href="/admin">Progress Materi</a>
        
                    <a class="nav-link" id="v-pills-latihan-tab"  href="/admin/latihan" >Nilai Latihan</a>
                    
                    <a class="nav-link" id="v-pills-kuis-tab" href="/admin/kuis">Nilai Kuis</a>
                        
                    <a class="nav-link active" id="v-pills-tugas-tab" href="/admin/tugas">Nilai Tugas</a>
                    
                    <a class="nav-link" id="v-pills-latihan-koding-tab" href="/admin/latihan-koding">Latihan Koding</a>
                </div>
            </div>
            <div class="col-md-12 align-items-center ftco-animate">
                <div class="tab-content ftco-animate" id="v-pills-tabContent">
                    <div class="tab-pane fade show active" id="v-pills-tugas" role="tabpanel" aria-labelledby="v-pills-tugas-tab">
                        <div class="d-md-flex">
                            <div class="col-md-3 align-self-start">
                                <img src={{ asset("images/undraw_taking_notes_tjaf.svg") }} class="img-fluid" alt="">
                            </div>
                            <div class="col-md-9 ftco-animate">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead class="thead-primary">
                                            <tr>
                                                <th>No</th>
                                                <th>Nama</th>
                                                <th>ID Tugas</th>
                                                <th>Judul Tugas</th>
                                                <th>Kode</th>
                                                <th>Nilai</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php $i = 1 + ($users->currentPage() * $users->perPage()) - $users->perPage() @endphp
                                            @foreach($users as $user)
                                                @php $count = 1 @endphp
                                                @if($count == 1)
                                                <tr>
                                                    <td rowspan="{{ count($user->tugasSiswa) == 0 ? '1' : count($user->tugasSiswa) }}">{{ $i }}</td>
                                                    <td class="text-left" rowspan="{{ count($user->tugasSiswa) == 0 ? '1' : count($user->tugasSiswa) }}">{{ $user->nama }}</td>
                                                @endif
                                                @if(count($user->tugasSiswa) != 0)
                                                    @foreach ($user->tugasSiswa as $tugasSiswa)
                                                        @if($count == 1)
                                                            @php $count = 0 @endphp
                                                        @endif
                                                            <td>
                                                                {{ $tugasSiswa->tugasGuru->id }}
                                                            </td>
                                                            <td>
                                                                {{ $tugasSiswa->tugasGuru->judul_tugas }}
                                                            </td>
                                                            <td class="text-left">
                                                                {{ $tugasSiswa->code }}
                                                            </td>
                                                            <td>
                                                                {{ $tugasSiswa->nilai == null ? '-' : $tugasSiswa->nilai }}
                                                            </td>
                                                            <td>
                                                                <a href="/admin/tugas/ubah-nilai/{{ $tugasSiswa->id }}" class="btn btn-primary">Ubah Nilai</a>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @else
                                                    <td>
                                                        -
                                                    </td>
                                                    <td>
                                                        -
                                                    </td>
                                                    <td>
                                                        -
                                                    </td>
                                                    <td>
                                                        -
                                                    </td>
                                                    <td>
                                                        <a href="" class="btn btn-primary disabled">Ubah Nilai</a>
                                                    </td>
                                                </tr>
                                                @endif
                                                @php $i++ @endphp
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <nav>
                                        <ul class="pagination justify-content-end">
                                            {{$users->links('vendor.pagination.bootstrap-4')}}
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection