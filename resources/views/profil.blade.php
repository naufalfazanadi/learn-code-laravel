@extends('layouts.layout')

@section('content')
<style>
    @media (min-width: 768px ) {
        .bottom-align-text {
            position: absolute;
            bottom: 0;
            right: 0;
        }
    }
</style>
<section class="ftco-section bg-light">
    <div class="container">
        <br>
        <div class="row" style="position: relative">
            <div class="col-md-4 heading-section ftco-animate text-center">
                <img src={{ asset("images/undraw_resume_folder_2_arse.svg") }} height="200px" style="margin: 40px" class="img-responsive" />
                <!-- <span class="subheading">Materi 1</span> -->
            </div>
            <div class="col-md-8 heading-section ftco-animate bottom-align-text">
                <h2 class="">Profil - {{ Auth::user()->nama }}</h2>
                <hr>
                <p class="mb-4">{{ Auth::user()->email }}</p>
            </div>
        </div>
        <br>
        <br>
        <div class="row">
            <div class="col-md-12 nav-link-wrap mb-5 pb-md-5 pb-sm-1 ftco-animate">
                <div class="nav ftco-animate nav-pills justify-content-center text-center" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                    <a class="nav-link active" id="v-pills-materi-tab" data-toggle="pill" href="#v-pills-materi" role="tab" aria-controls="v-pills-materi" aria-selected="true">Progress Materi</a>
        
                    <a class="nav-link" id="v-pills-kuis-tab" data-toggle="pill" href="#v-pills-kuis" role="tab" aria-controls="v-pills-kuis" aria-selected="false">Nilai Kuis</a>
                    
                    <a class="nav-link" id="v-pills-evaluasi-tab" data-toggle="pill" href="#v-pills-evaluasi" role="tab" aria-controls="v-pills-evaluasi" aria-selected="false">Nilai Evaluasi</a>
                        
                    <a class="nav-link" id="v-pills-tugas-tab" data-toggle="pill" href="#v-pills-tugas" role="tab" aria-controls="v-pills-tugas" aria-selected="false">Nilai Praktik</a>
                    
                    <a class="nav-link" id="v-pills-latihan-koding-tab" data-toggle="pill" href="#v-pills-latihan-koding" role="tab" aria-controls="v-pills-latihan-koding" aria-selected="false">Latihan Koding</a>
                </div>
            </div>
            <div class="col-md-12 align-items-center ftco-animate">
                <div class="tab-content ftco-animate" id="v-pills-tabContent">
                    <div class="tab-pane fade show active" id="v-pills-materi" role="tabpanel" aria-labelledby="v-pills-materi-tab">
                        <div class="d-md-flex">
                            <div class="col-md-3 align-self-start">
                                <img src={{ asset("images/undraw_percentages_0rur.svg") }} class="img-fluid" alt="">
                            </div>
                            <div class="col-md-9 ftco-animate">
                                {{-- <h1 class="text-center" style="font-weight: 700">Tugas Siswa</h1> --}}
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead class="thead-primary">
                                            <tr>
                                                <th>No</th>
                                                <th>Materi</th>
                                                <th>Modul</th>
                                                <th>Jumlah Konten</th>
                                                <th>Progress Modul</th>
                                                <th>Progress Materi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td rowspan="5">1</td>
                                                <td rowspan="5">Materi 1</td>
                                                @php
                                                    $progress_materi1_all = $status->materi1;

                                                    if ($status->materi1 > 5) {
                                                        $modul = 5;
                                                    } else if ($status->materi1 < 0) {
                                                        $modul = 0;
                                                    } else {
                                                        $modul = $status->materi1 % 6;
                                                    }
                                                    $status->materi1 -= 5;
                                                    $progress = $modul / 5 * 100;    
                                                @endphp
                                                <td>1</td>
                                                <td>{{ $modul }} / 5</td>
                                                <td>
                                                    <div class="progress" style="height:30px">
                                                        <div class="progress-bar progress-bar-striped progress-bar-animated {{ $progress == 100 ? 'bg-info' : 'bg-success' }} " style="width:{{ round($progress) }}%; height: 30px; {{ $progress == 0 ? 'color: #020202;' : '' }}">{{ round($progress) }}%</div>
                                                    </div>
                                                </td>
                                                <td rowspan="5" class="color">
                                                    <div class="progress" style="height:30px">
                                                        @php
                                                            $progress = $progress_materi1_all / 25 * 100;    
                                                        @endphp
                                                        <div class="progress-bar progress-bar-striped progress-bar-animated {{ $progress == 100 ? 'bg-info' : 'bg-success' }} " style="width:{{ round($progress) }}%; height: 30px; {{ $progress == 0 ? 'color: #020202;' : '' }}">{{ round($progress) }}%</div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                @php
                                                    if ($status->materi1 > 6) {
                                                        $modul = 6;
                                                    } else if ($status->materi1 < 0) {
                                                        $modul = 0;
                                                    } else {
                                                        $modul = $status->materi1 % 7;
                                                    }
                                                    $status->materi1 -= 6;
                                                    $progress = $modul / 6 * 100;    
                                                @endphp
                                                <td>2</td>
                                                <td>{{ $modul }} / 6</td>
                                                <td>
                                                    <div class="progress" style="height:30px">
                                                        <div class="progress-bar progress-bar-striped progress-bar-animated {{ $progress == 100 ? 'bg-info' : 'bg-success' }} " style="width:{{ round($progress) }}%; height: 30px; {{ $progress == 0 ? 'color: #020202;' : '' }}">{{ round($progress) }}%</div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                @php
                                                    if ($status->materi1 > 4) {
                                                        $modul = 4;
                                                    } else if ($status->materi1 < 0) {
                                                        $modul = 0;
                                                    } else {
                                                        $modul = $status->materi1 % 5;
                                                    }
                                                    $status->materi1 -= 4;

                                                    $progress = $modul / 4 * 100;    
                                                @endphp
                                                <td>3</td>
                                                <td>{{ $modul }} / 4</td>
                                                <td>
                                                    <div class="progress" style="height:30px">
                                                        <div class="progress-bar progress-bar-striped progress-bar-animated {{ $progress == 100 ? 'bg-info' : 'bg-success' }} " style="width:{{ round($progress) }}%; height: 30px; {{ $progress == 0 ? 'color: #020202;' : '' }}">{{ round($progress) }}%</div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                @php
                                                    if ($status->materi1 > 7) {
                                                        $modul = 7;
                                                    } else if ($status->materi1 < 0) {
                                                        $modul = 0;
                                                    } else {
                                                        $modul = $status->materi1 % 8;
                                                    }
                                                    $status->materi1 -= 7;

                                                    $progress = $modul / 7 * 100;    
                                                @endphp
                                                <td>4</td>
                                                <td>{{ $modul }} / 7</td>
                                                <td>
                                                    <div class="progress" style="height:30px">
                                                        <div class="progress-bar progress-bar-striped progress-bar-animated {{ $progress == 100 ? 'bg-info' : 'bg-success' }} " style="width:{{ round($progress) }}%; height: 30px; {{ $progress == 0 ? 'color: #020202;' : '' }}">{{ round($progress) }}%</div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                @php
                                                    if ($status->materi1 > 3) {
                                                        $modul = 3;
                                                    } else if ($status->materi1 < 0) {
                                                        $modul = 0;
                                                    } else {
                                                        $modul = $status->materi1 % 4;
                                                    }
                                                    $status->materi1 -= 3;

                                                    $progress = $modul / 3 * 100;    
                                                @endphp
                                                <td>5</td>
                                                <td>{{ $modul }} / 3</td>
                                                <td>
                                                    <div class="progress" style="height:30px">
                                                        <div class="progress-bar progress-bar-striped progress-bar-animated {{ $progress == 100 ? 'bg-info' : 'bg-success' }} " style="width:{{ round($progress) }}%; height: 30px; {{ $progress == 0 ? 'color: #020202;' : '' }}">{{ round($progress) }}%</div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                @php
                                                    $progress_materi2_all = $status->materi2;

                                                    if ($status->materi2 > 4) {
                                                        $modul = 4;
                                                    } else if ($status->materi2 < 0) {
                                                        $modul = 0;
                                                    } else {
                                                        $modul = $status->materi2 % 5;
                                                    }
                                                    $status->materi2 -= 4;
                                                    $progress = $modul / 4 * 100;    
                                                @endphp
                                                <td rowspan="3">2</td>
                                                <td rowspan="3">Materi 2</td>
                                                <td>1</td>
                                                <td>{{ $modul }} / 4</td>
                                                <td>
                                                    <div class="progress" style="height:30px">
                                                        <div class="progress-bar progress-bar-striped progress-bar-animated {{ $progress == 100 ? 'bg-info' : 'bg-success' }} " style="width:{{ round($progress) }}%; height: 30px; {{ $progress == 0 ? 'color: #020202;' : '' }}">{{ round($progress) }}%</div>
                                                    </div>
                                                </td>
                                                <td rowspan="3" class="color">
                                                    <div class="progress" style="height:30px">
                                                        @php
                                                            $progress = $progress_materi2_all / 12 * 100;    
                                                        @endphp
                                                        <div class="progress-bar progress-bar-striped progress-bar-animated {{ $progress == 100 ? 'bg-info' : 'bg-success' }} " style="width:{{ round($progress) }}%; height: 30px; {{ $progress == 0 ? 'color: #020202;' : '' }}">{{ round($progress) }}%</div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                @php
                                                    if ($status->materi2 > 3) {
                                                        $modul = 3;
                                                    } else if ($status->materi2 < 0) {
                                                        $modul = 0;
                                                    } else {
                                                        $modul = $status->materi2 % 4;
                                                    }
                                                    $status->materi2 -= 3;

                                                    $progress = $modul / 3 * 100;    
                                                @endphp
                                                <td>2</td>
                                                <td>{{ $modul }} / 3</td>
                                                <td>
                                                    <div class="progress" style="height:30px">
                                                        <div class="progress-bar progress-bar-striped progress-bar-animated {{ $progress == 100 ? 'bg-info' : 'bg-success' }} " style="width:{{ round($progress) }}%; height: 30px; {{ $progress == 0 ? 'color: #020202;' : '' }}">{{ round($progress) }}%</div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                @php
                                                    if ($status->materi2 > 5) {
                                                        $modul = 5;
                                                    } else if ($status->materi2 < 0) {
                                                        $modul = 0;
                                                    } else {
                                                        $modul = $status->materi2 % 6;
                                                    }
                                                    $status->materi2 -= 5;

                                                    $progress = $modul / 5 * 100;    
                                                @endphp
                                                <td>3</td>
                                                <td>{{ $modul }} / 5</td>
                                                <td>
                                                    <div class="progress" style="height:30px">
                                                        <div class="progress-bar progress-bar-striped progress-bar-animated {{ $progress == 100 ? 'bg-info' : 'bg-success' }} " style="width:{{ round($progress) }}%; height: 30px; {{ $progress == 0 ? 'color: #020202;' : '' }}">{{ round($progress) }}%</div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                @php
                                                    $progress_materi3_all = $status->materi3;

                                                    if ($status->materi3 > 4) {
                                                        $modul = 4;
                                                    } else if ($status->materi3 < 0) {
                                                        $modul = 0;
                                                    } else {
                                                        $modul = $status->materi3 % 5;
                                                    }
                                                    $status->materi3 -= 4;
                                                    $progress = $modul / 4 * 100;    
                                                @endphp
                                                <td rowspan="3">3</td>
                                                <td rowspan="3">Materi 3</td>
                                                <td>1</td>
                                                <td>{{ $modul }} / 4</td>
                                                <td>
                                                    <div class="progress" style="height:30px">
                                                        <div class="progress-bar progress-bar-striped progress-bar-animated {{ $progress == 100 ? 'bg-info' : 'bg-success' }} " style="width:{{ round($progress) }}%; height: 30px; {{ $progress == 0 ? 'color: #020202;' : '' }}">{{ round($progress) }}%</div>
                                                    </div>
                                                </td>
                                                <td rowspan="3" class="color">
                                                    <div class="progress" style="height:30px">
                                                        @php
                                                            $progress = $progress_materi3_all / 10 * 100;    
                                                        @endphp
                                                        <div class="progress-bar progress-bar-striped progress-bar-animated {{ $progress == 100 ? 'bg-info' : 'bg-success' }} " style="width:{{ round($progress) }}%; height: 30px; {{ $progress == 0 ? 'color: #020202;' : '' }}">{{ round($progress) }}%</div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                @php
                                                    if ($status->materi3 > 2) {
                                                        $modul = 2;
                                                    } else if ($status->materi3 < 0) {
                                                        $modul = 0;
                                                    } else {
                                                        $modul = $status->materi3 % 3;
                                                    }
                                                    $status->materi3 -= 2;

                                                    $progress = $modul / 2 * 100;    
                                                @endphp
                                                <td>2</td>
                                                <td>{{ $modul }} / 2</td>
                                                <td>
                                                    <div class="progress" style="height:30px">
                                                        <div class="progress-bar progress-bar-striped progress-bar-animated {{ $progress == 100 ? 'bg-info' : 'bg-success' }} " style="width:{{ round($progress) }}%; height: 30px; {{ $progress == 0 ? 'color: #020202;' : '' }}">{{ round($progress) }}%</div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                @php
                                                    if ($status->materi3 > 4) {
                                                        $modul = 4;
                                                    } else if ($status->materi3 < 0) {
                                                        $modul = 0;
                                                    } else {
                                                        $modul = $status->materi3 % 5;
                                                    }
                                                    $status->materi3 -= 4;

                                                    $progress = $modul / 4 * 100;    
                                                @endphp
                                                <td>3</td>
                                                <td>{{ $modul }} / 4</td>
                                                <td>
                                                    <div class="progress" style="height:30px">
                                                        <div class="progress-bar progress-bar-striped progress-bar-animated {{ $progress == 100 ? 'bg-info' : 'bg-success' }} " style="width:{{ round($progress) }}%; height: 30px; {{ $progress == 0 ? 'color: #020202;' : '' }}">{{ round($progress) }}%</div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                @php
                                                    $progress_materi4_all = $status->materi4;

                                                    if ($status->materi4 > 5) {
                                                        $modul = 5;
                                                    } else if ($status->materi4 < 0) {
                                                        $modul = 0;
                                                    } else {
                                                        $modul = $status->materi4 % 6;
                                                    }
                                                    $status->materi4 -= 5;
                                                    $progress = $modul / 5 * 100;    
                                                @endphp
                                                <td rowspan="3">4</td>
                                                <td rowspan="3">Materi 4</td>
                                                <td>1</td>
                                                <td>{{ $modul }} / 5</td>
                                                <td>
                                                    <div class="progress" style="height:30px">
                                                        <div class="progress-bar progress-bar-striped progress-bar-animated {{ $progress == 100 ? 'bg-info' : 'bg-success' }} " style="width:{{ round($progress) }}%; height: 30px; {{ $progress == 0 ? 'color: #020202;' : '' }}">{{ round($progress) }}%</div>
                                                    </div>
                                                </td>
                                                <td rowspan="3" class="color">
                                                    <div class="progress" style="height:30px">
                                                        @php
                                                            $progress = $progress_materi4_all / 12 * 100;    
                                                        @endphp
                                                        <div class="progress-bar progress-bar-striped progress-bar-animated {{ $progress == 100 ? 'bg-info' : 'bg-success' }} " style="width:{{ round($progress) }}%; height: 30px; {{ $progress == 0 ? 'color: #020202;' : '' }}">{{ round($progress) }}%</div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                @php
                                                    if ($status->materi4 > 4) {
                                                        $modul = 4;
                                                    } else if ($status->materi4 < 0) {
                                                        $modul = 0;
                                                    } else {
                                                        $modul = $status->materi4 % 5;
                                                    }
                                                    $status->materi4 -= 4;

                                                    $progress = $modul / 4 * 100;    
                                                @endphp
                                                <td>2</td>
                                                <td>{{ $modul }} / 4</td>
                                                <td>
                                                    <div class="progress" style="height:30px">
                                                        <div class="progress-bar progress-bar-striped progress-bar-animated {{ $progress == 100 ? 'bg-info' : 'bg-success' }} " style="width:{{ round($progress) }}%; height: 30px; {{ $progress == 0 ? 'color: #020202;' : '' }}">{{ round($progress) }}%</div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                @php
                                                    if ($status->materi4 > 3) {
                                                        $modul = 3;
                                                    } else if ($status->materi4 < 0) {
                                                        $modul = 0;
                                                    } else {
                                                        $modul = $status->materi4 % 4;
                                                    }
                                                    $status->materi4 -= 3;

                                                    $progress = $modul / 3 * 100;    
                                                @endphp
                                                <td>3</td>
                                                <td>{{ $modul }} / 3</td>
                                                <td>
                                                    <div class="progress" style="height:30px">
                                                        <div class="progress-bar progress-bar-striped progress-bar-animated {{ $progress == 100 ? 'bg-info' : 'bg-success' }} " style="width:{{ round($progress) }}%; height: 30px; {{ $progress == 0 ? 'color: #020202;' : '' }}">{{ round($progress) }}%</div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="v-pills-kuis" role="tabpanel" aria-labelledby="v-pills-kuis-tab">
                        <div class="d-md-flex">
                            <div class="col-md-3 align-self-start">
                                <img src={{ asset("images/undraw_exams_g4ow.svg") }} class="img-fluid" alt="">
                            </div>
                            <div class="col-md-9 ftco-animate">
                                {{-- <h1 class="text-center" style="font-weight: 700">Tugas Siswa</h1> --}}
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead class="thead-primary">
                                            <tr>
                                                <th>No</th>
                                                <th>Materi</th>
                                                <th>Modul</th>
                                                <th>Nilai</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if (count($kuis) != 0)
                                                @php $i = 1 @endphp
                                                @foreach ($kuis as $val)
                                                    <tr>
                                                        <td>{{ $i }}</td>
                                                        <td>Materi {{ $val->materi }}</td>
                                                        <td>{{ $val->modul }}</td>
                                                        <td class="color">{{ $val->nilai }}</td>
                                                    </tr>
                                                    @php $i++ @endphp
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="4">Belum ada kuis yang dikerjakan</td>
                                                </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="v-pills-evaluasi" role="tabpanel" aria-labelledby="v-pills-evaluasi-tab">
                        <div class="d-md-flex">
                            <div class="col-md-3 align-self-start">
                                <img src={{ asset("images/undraw_web_development_w2vv.svg") }} class="img-fluid" alt="">
                            </div>
                            <div class="col-md-9 ftco-animate">
                                {{-- <h1 class="text-center" style="font-weight: 700">Tugas Siswa</h1> --}}
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead class="thead-primary">
                                            <tr>
                                                {{-- <th>No</th> --}}
                                                <th>Jawaban</th>
                                                {{-- <th>Modul</th> --}}
                                                {{-- <th>Kode</th> --}}
                                                <th>Nilai</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if ($evaluasi)
                                                <tr>
                                                    <td>{{ $evaluasi->jawaban }}</td>
                                                    <td class="color">{{ $evaluasi->nilai == null ? 'Belum ada nilai' : $evaluasi->nilai }}</td>
                                                </tr>
                                            @else
                                                <tr>
                                                    <td colspan="5">Belum ada latihan yang dikerjakan</td>
                                                </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="v-pills-tugas" role="tabpanel" aria-labelledby="v-pills-tugas-tab">
                        <div class="d-md-flex">
                            <div class="col-md-3 align-self-start">
                                <img src={{ asset("images/undraw_taking_notes_tjaf.svg") }} class="img-fluid" alt="">
                            </div>
                            <div class="col-md-9 ftco-animate">
                                {{-- <h1 class="text-center" style="font-weight: 700">Tugas Siswa</h1> --}}
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead class="thead-primary">
                                            <tr>
                                                <th>No</th>
                                                <th>Materi</th>
                                                {{-- <th>Modul</th> --}}
                                                <th>Nilai</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php $i = 1 @endphp
                                            @if (count($praktik) != 0)
                                                @foreach ($praktik as $val)
                                                    <tr>
                                                        <td>{{ $i }}</td>
                                                        <td>Materi {{ $val->materi }}</td>
                                                        <td class="color">{{ $val->nilai == null ? 'Belum ada nilai' : $val->nilai }}</td>
                                                    </tr>
                                                    @php $i++ @endphp
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="3">Belum ada praktik yang dikerjakan</td>
                                                </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="v-pills-latihan-koding" role="tabpanel" aria-labelledby="v-pills-latihan-koding-tab">
                        <div class="d-md-flex">
                            <div class="col-md-3 align-self-start">
                                <img src={{ asset("images/undraw_web_developer_p3e5.svg") }} class="img-fluid" alt="">
                            </div>
                            <div class="col-md-9 ftco-animate">
                                {{-- <h1 class="text-center" style="font-weight: 700">Tugas Siswa</h1> --}}
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead class="thead-primary">
                                            <tr>
                                                <th>No</th>
                                                <th>Judul</th>
                                                <th>Kode</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php $i = 1 @endphp
                                            @if (count($latihanKoding) != 0)
                                                @foreach ($latihanKoding as $val)
                                                    <tr>
                                                        <td>{{ $i }}</td>
                                                        <td>{{ $val->judul }}</td>
                                                        <td>{{ $val->html }}</td>
                                                    </tr>
                                                    @php $i++ @endphp
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="3">Belum ada latihan koding yang disimpan</td>
                                                </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection