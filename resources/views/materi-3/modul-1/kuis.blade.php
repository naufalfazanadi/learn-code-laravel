@extends('layouts.layout')

@section('content')
<section class="ftco-section bg-white">
    <div class="container">
        <br>
        <div class="row  mb-3 pb-md-3">
            <div class="col-md-12 heading-section ftco-animate">
                    <img src={{ asset("images/open-book.png") }} height="100px" style="margin: 40px" />
                    <img src={{ asset("images/table_sq.png") }} height="140px" />
                    @include('layouts.modul-breadcrumbs')
                <!-- <span class="subheading">Materi 1</span> -->
                <h2 class="mb-4">Modul 1 - Anatomi Tabel Minimal</h2>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 ftco-animate">
                @include('materi-'.request()->segment(2).'.modul-'.request()->segment(3).'.sidebar')
            </div>
            <div class="col-md-9 ftco-animate">
                @if ($kuis == "")
                    <form action="/kuis" method="POST">
                        {{ csrf_field() }}
                        <h2 style="font-weight: 700" class="mb-4 text-center">Kuis</h2>
                        @if (session('status_repeat'))
                            <div class="alert alert-info">
                                {{ session('status_repeat') }}
                            </div>
                        @endif
                        <p>1. Tag untuk membuat tabel pada sebuha halaman web adalah . . .</p>
                        <div class="highlight">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no1" id="1a" value="a" required>
                                <label class="form-check-label" for="1a">
                                    a. &lt;TABLE&gt;
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no1" id="1b" value="b">
                                <label class="form-check-label" for="1b">
                                    b. &lt;HTML&gt;
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no1" id="1c" value="c">
                                <label class="form-check-label" for="1c">
                                    c. &lt;HEAD&gt;
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no1" id="1d" value="d">
                                <label class="form-check-label" for="1d">
                                    d. &lt;IL&gt;
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no1" id="1e" value="e">
                                <label class="form-check-label" for="1e">
                                    e. &lt;BR&gt;
                                </label>
                            </div>
                        </div>
                        <br>
                        <p>2. Struktur dasar pembuatan tabel pada HTML adalah . . .</p>
                        <div class="highlight">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no2" id="2a" value="a" required>
                                <label class="form-check-label" for="2a">
                                    a.	&lt;table&gt;<br>&emsp;&lt;tr&gt;<br>&emsp;&emsp;&lt;td&gt;&lt;/td&gt;<br>&emsp;&lt;tr&gt;<br>&emsp;&lt;/table&gt;
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no2" id="2b" value="b">
                                <label class="form-check-label" for="2b">
                                    b.	&lt;table&gt;<br>&emsp;&lt;td&gt;<br>&emsp;&emsp;&lt;tr&gt;&lt;/tr&gt;<br>&emsp;&lt;td&gt;<br>&emsp;&lt;/table&gt;
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no2" id="2c" value="c">
                                <label class="form-check-label" for="2c">
                                    c.	&lt;table&gt;<br>&emsp;&lt;tr&gt;<br>&emsp;&emsp;&lt;td&gt;&lt;/td&gt;<br>&emsp;&lt;/tr&gt;<br>&emsp;&lt;/table&gt;
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no2" id="2d" value="d">
                                <label class="form-check-label" for="2d">
                                    d.	&lt;table&gt;<br>&emsp;&lt;tr&gt;<br>&emsp;&emsp;&lt;td&gt;&lt;/td&gt;<br>&emsp;&lt;/tr&gt;<br>&emsp;&lt;table&gt;
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no2" id="2e" value="e">
                                <label class="form-check-label" for="2e">
                                    e.	&lt;table&gt;<br>&emsp;&lt;tr&gt;<br>&emsp;&emsp;&lt;td&gt;<br>&emsp;&lt;/tr&gt;<br>&emsp;&emsp;&lt;/td&gt;<br>&emsp;&lt;/table&gt;
                                </label>
                            </div>
                        </div>
                        <br>
                        <p>3. Perhatikan tabel berikut!</p>
                        <table border="1">
                            <tr>
                                <td width="100px">A</td>
                                <td width="100px">B</td>
                            </tr>
                        </table>
                        <p>Perintah yang benar untuk membuat tabel di atas adalah . . .</p>
                        <div class="highlight">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no3" id="3a" value="a" required>
                                <label class="form-check-label" for="3a">
                                    a.	&lt;table&gt;<br>
                                    &emsp;&emsp;&lt;td&gt;a&lt;/td&gt;<br>
                                    &emsp;&emsp;&lt;td&gt;b&lt;/td&gt;<br>
                                    &emsp;&lt;/table&gt;
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no3" id="3b" value="b">
                                <label class="form-check-label" for="3b">
                                    b.	&lt;table&gt;<br>
                                    &emsp;&lt;tr&gt;<br>
                                    &emsp;&emsp;&lt;td&gt;a&lt;/td&gt;<br>
                                    &emsp;&emsp;&lt;td&gt;b&lt;/td&gt;<br>
                                    &emsp;&lt;/tr&gt;<br>
                                    &emsp;&lt;/table&gt;
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no3" id="3c" value="c">
                                <label class="form-check-label" for="3c">
                                    c.	&lt;table&gt;<br>
                                    &emsp;&lt;tr&gt;<br>
                                    &emsp;&emsp;&lt;td&gt;a&lt;td&gt;<br>
                                    &emsp;&emsp;&lt;td&gt;b&lt;td&gt;<br>
                                    &emsp;&lt;tr&gt;<br>
                                    &emsp;&lt;/table&gt;
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no3" id="3d" value="d">
                                <label class="form-check-label" for="3d">
                                    d.	&lt;table&gt;<br>
                                    &emsp;&emsp;&lt;tr&gt;a&lt;/tr&gt;<br>
                                    &emsp;&emsp;&lt;tr&gt;b&lt;/tr&gt;<br>
                                    &emsp;&lt;/table&gt;
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no3" id="3e" value="e">
                                <label class="form-check-label" for="3e">
                                    e.	&lt;table&gt;<br>
                                    &emsp;&lt;tr&gt;<br>
                                    &emsp;&emsp;&lt;td&gt;a&lt;/td&gt;<br>
                                    &emsp;&lt;tr&gt;<br>
                                    &emsp;&emsp;&lt;td&gt;b&lt;/td&gt;<br>
                                    &emsp;&lt;/table&gt;
                                </label>
                            </div>
                        </div>
                        <br>
                        <p>4. Tag membuat tabel yang benar adalah . . .</p>
                        <div class="highlight">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no4" id="4a" value="a" required>
                                <label class="form-check-label" for="4a">
                                    a.	&lt;TABLE&gt;…&lt;/TABLE&gt;
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no4" id="4b" value="b">
                                <label class="form-check-label" for="4b">
                                    b.	&lt;TABLE&gt;…&lt;TABLE&gt;
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no4" id="4c" value="c">
                                <label class="form-check-label" for="4c">
                                    c.	&lt;/TABLE&gt;…&lt;TABLE&gt;
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no4" id="4d" value="d">
                                <label class="form-check-label" for="4d">
                                    d.	&lt;/TABLE&gt;…&lt;/TABLE&gt;
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no4" id="4e" value="e">
                                <label class="form-check-label" for="4e">
                                    e.	&lt;TABLE&gt;…&lt;TABLE/&gt;
                                </label>
                            </div>
                        </div>
                        <br>
                        <p>5. Pengertian dari tabel adalah . . .</p>
                        <div class="highlight">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no5" id="5a" value="a" required>
                                <label class="form-check-label" for="5a">
                                    a.	Informasi dalam bentuk baris dan kolom
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no5" id="5b" value="b">
                                <label class="form-check-label" for="5b">
                                    b.	Tulisan berjalan
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no5" id="5c" value="c">
                                <label class="form-check-label" for="5c">
                                    c.	Efek suara
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no5" id="5d" value="d">
                                <label class="form-check-label" for="5d">
                                    d.	Informasi berbentuk gambar
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no5" id="5e" value="e">
                                <label class="form-check-label" for="5e">
                                    e.	Gambar bergerak
                                </label>
                            </div>
                        </div>
                        <br>
                        <br>
                        <div class="row ftco-animate">
                            <div class="col-md-3"></div>
                            <div class="col-md-3">
                                <center><p><a href={{ url("materi/3/1/3") }} class="btn btn-primary py-3">< Sebelumnya</a></p></center>
                                <!-- <center><a href='plasma-flame.html' class="btn btn-primary btn-lg"><< Sebelumnya</a></center> -->
                            </div>
                            <div class="col-md-3">
                                <input type="hidden" id="id" name="jumlahSoal" value="5">
                                <input type="hidden" id="id" name="materi" value={{ request()->segment(2) }}>
                                <input type="hidden" id="id" name="modul" value={{ request()->segment(3) }}>

                                <center><input type="submit" value="Kirim Jawaban" class="btn btn-success py-3 px-5"></center>
                                <!-- <center><a href='persiapan-alat.html' class="btn btn-primary btn-lg">Materi Selanjutnya ></a></center> -->
                            </div>
                            <div class="col-md-3"></div>
                        </div>
                    </form>
                @else
                    <div class="alert alert-success">
                        <h3 class="text-center">Kamu telah menyelesaikan kuis</h2>
                        <h2 style="font-weight: 700" class="mb-4 text-center">Nilai kamu : <b>{{ $kuis->nilai }}</b></h3>
                    </div>
                    <br>
                    <br>
                    <div class="row ftco-animate">
                        <div class="col-md-3"></div>
                        <div class="col-md-3">
                            <center><p><a href={{ url("materi/3/1/latihan") }} class="btn btn-primary py-3">< Sebelumnya</a></p></center>
                        </div>
                        <div class="col-md-3">
                            <center><p><a href={{ url("materi/3") }} class="btn btn-success py-3">Daftar modul</a></p></center>
                        </div>
                        <div class="col-md-3"></div>
                    </div>
                @endif
            </div>
        </div>
    </div>
</section>
@endsection