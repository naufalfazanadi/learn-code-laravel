@extends('layouts.layout')

@section('content')
<section class="ftco-section bg-white">
    <div class="container">
        <br>
        <div class="row  mb-3 pb-md-3">
            <div class="col-md-12 heading-section ftco-animate">
                    <img src={{ asset("images/open-book.png") }} height="100px" style="margin: 40px" />
                    <img src={{ asset("images/table_sq.png") }} height="140px" />
                    @include('layouts.modul-breadcrumbs')
                <!-- <span class="subheading">Materi 1</span> -->
                <h2 class="mb-4">Modul 1 - Anatomi Tabel Minimal</h2>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 ftco-animate">
                @include('materi-'.request()->segment(2).'.modul-'.request()->segment(3).'.sidebar')
            </div>
            <div class="col-md-9 ftco-animate">
                    <h2 style="font-weight: 700" class="mb-4 text-center">Pembuatan Tabel (3)</h2>
                    <p class="text-justify">
                        Ada kalanya kita membuat table dengan menggabungkan baris, ataupun kolom. Berikut ini contoh tabel yang menggabungkan baris:
                    </p>
                    <table border="1" width="500px" align="center">
                        <tr>
                            <td>Benua</td>
                            <td>Negara</td>
                        </tr>
                        <tr>
                            <td rowspan="3">Asia</td>
                            <td>Arab Saudi</td>
                        </tr>
                        <tr>
                            <td>India</td>
                        </tr>
                        <tr>
                            <td>Indonesia</td>
                        </tr>
                    </table>
                    <br>
                    <p>
                        Untuk membuat table seperti di atas, maka Tabel Atribut Data yang digunakan adalah sebagai berikut:
                    </p>
                    <ul>
                        <li>Rowspan = angka (baris yang di span oleh sel) </li>
                        <li>Colspan = angka (kolom yang di span oleh sel)</li>
                    </ul>
                <!-- <div class="highlight">
                    <xmp><table>
<tr>
    <th>Month</th>
    <th>Savings</th>
</tr>
<tr>
    <td>January</td>
    <td>$100</td>
</tr>
</table></xmp>
                </div> -->
                
                <br>
                <br>
                <div class="row ftco-animate">
                    <div class="col-md-3"></div>
                    <div class="col-md-3">
                        <center><p><a href={{ url("materi/3/1/2") }} class="btn btn-primary py-3">< Sebelumnya</a></p></center>
                    </div>
                    <div class="col-md-3">
                        <center><p><a href={{ url("materi/status/3/1/".((int)request()->segment(4)+1)) }} class="btn btn-primary py-3">Selanjutnya ></a></p></center>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection