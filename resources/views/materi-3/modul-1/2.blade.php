@extends('layouts.layout')

@section('content')
<section class="ftco-section bg-white">
    <div class="container">
        <br>
        <div class="row  mb-3 pb-md-3">
            <div class="col-md-12 heading-section ftco-animate">
                    <img src={{ asset("images/open-book.png") }} height="100px" style="margin: 40px" />
                    <img src={{ asset("images/table_sq.png") }} height="140px" />
                    @include('layouts.modul-breadcrumbs')
                <!-- <span class="subheading">Materi 1</span> -->
                <h2 class="mb-4">Modul 1 - Anatomi Tabel Minimal</h2>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 ftco-animate">
                @include('materi-'.request()->segment(2).'.modul-'.request()->segment(3).'.sidebar')
            </div>
            <div class="col-md-9 ftco-animate">
                    <h2 style="font-weight: 700" class="mb-4 text-center">Pembuatan Tabel (2)</h2>
                    <b>Atribut Elemen Tabel</b>
                    <ul>
                        <li>Width   = Panjang (lebar table, pixel atau persen)</li>
                        <li>Height  = Panjang (tinggi table, pixel atau persen)</li>
                        <li>Border		= Pixel (tebal garis tepi) </li>
                        <li>Cellspacing	= Pixel (spasi antar sel) </li>
                        <li>Cellpadding	= Pixel (spasi di dalam sel) </li>
                        <li>Align		= [left|center|right] (perataan table) </li>
                        <li>Bgcolor	= warna (warna latar belakang table)</li>
                    </ul>
                    <b>Atribut Tabel Row</b>
                    <ul>
                        <li>Align		= [left|center|right] (perataan sebaris sel secara horizontal) </li>
                        <li>Valign		= [top|middle|bottom] (perataaan sebaris sel secara vertical) </li>
                        <li>Bgcolor		= Warna (warna latar belakang baris)</li>
                    </ul>
                    <b>Atribut Tabel Data</b>
                    <ul>
                        <li>Align 		= [left|center|right] (perataan horizontal) </li>
                        <li>Width 		= [top|middle|bottom] (perataan vertical)</li>
                        <li>Height 		= Pixel (tinggi sel, pixel atau persen)</li>
                        <li>Bgcolor 	= Warna (warna latar belakang sel)</li>
                    </ul>
                <!-- <div class="highlight">
                    <xmp><table>
<tr>
    <th>Month</th>
    <th>Savings</th>
</tr>
<tr>
    <td>January</td>
    <td>$100</td>
</tr>
</table></xmp>
                </div> -->
                
                <br>
                <br>
                <div class="row ftco-animate">
                    <div class="col-md-3"></div>
                    <div class="col-md-3">
                        <center><p><a href={{ url("materi/3/1/1") }} class="btn btn-primary py-3">< Sebelumnya</a></p></center>
                    </div>
                    <div class="col-md-3">
                        <center><p><a href={{ url("materi/status/3/1/".((int)request()->segment(4)+1)) }} class="btn btn-primary py-3">Selanjutnya ></a></p></center>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection