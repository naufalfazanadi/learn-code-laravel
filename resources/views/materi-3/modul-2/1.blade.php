@extends('layouts.layout')

@section('content')
<section class="ftco-section bg-white">
    <div class="container">
        <br>
        <div class="row  mb-3 pb-md-3">
            <div class="col-md-12 heading-section ftco-animate">
                <img src={{ asset("images/open-book.png") }} height="100px" style="margin: 40px" />
                <img src={{ asset("images/table_sq.png") }} height="140px" />
                @include('layouts.modul-breadcrumbs')
                <!-- <span class="subheading">Materi 1</span> -->
                <h2 class="mb-4">Modul 2 - Tabel di dalam Tabel</h2>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 ftco-animate">
                @include('materi-'.request()->segment(2).'.modul-'.request()->segment(3).'.sidebar')
            </div>
            <div class="col-md-9 ftco-animate">
                    <h2 style="font-weight: 700" class="mb-4 text-center">Tabel dalam Tabel (Tabel Bersarang)</h2>
                    <p class="text-justify">
                        Dalam penulisan HTML “tabel bersarang” ini adalah dengan menuliskan HTML Tabel di dalam sebuah sel dari tabel, letaknya adalah pada &lt;td&gt;…&lt;/td&gt;, seperti berikut ini :
                    </p>
                    <div class="highlight">
                        <xmp><table border=1>   
<tr>     
    <td>kolom 1 baris 1</td>
    <td>kolom 2 baris 1</td>   
</tr>   
<tr>
    <td>kolom 1 baris 2</td>
    <td> 
        <table border=1>   
            <tr>     
            <td>kolom 1 baris 1</td>     
            <td>kolom 2 baris 1</td>   
            </tr>   
            <tr>     
            <td>kolom 1 baris 2</td>     
            <td>kolom 2 baris 2</td>   
            </tr> 
        </table> 
    </td>   
</tr> 
</table></xmp>
                    </div>
                    <p>Hasilnya adalah sebagai berikut:</p>
                    <table border=1>   
                            <tr>     
                                <td>kolom 1 baris 1</td>
                                <td>kolom 2 baris 1</td>   
                            </tr>   
                            <tr>
                                <td>kolom 1 baris 2</td>
                                <td> 
                                    <table border=1>   
                                        <tr>     
                                        <td>kolom 1 baris 1</td>     
                                        <td>kolom 2 baris 1</td>   
                                        </tr>   
                                        <tr>     
                                        <td>kolom 1 baris 2</td>     
                                        <td>kolom 2 baris 2</td>   
                                        </tr> 
                                    </table> 
                                </td>   
                            </tr> 
                    </table>                <!-- <div class="highlight">
                    <xmp><table>
<tr>
    <th>Month</th>
    <th>Savings</th>
</tr>
<tr>
    <td>January</td>
    <td>$100</td>
</tr>
</table></xmp>
                </div> -->
                
                <br>
                <br>
                <div class="row ftco-animate">
                    <!-- <div class="col-md-3"></div> -->
                    <!-- <div class="col-md-3"> -->
                        <!-- <center><p><a href="materi.html" class="btn btn-primary py-3">< Daftar materi</a></p></center> -->
                        <!-- <center><a href='plasma-flame.html' class="btn btn-primary btn-lg"><< Sebelumnya</a></center> -->
                    <!-- </div> -->
                    <div class="col-md-12">
                        <center><p><a href={{ url("materi/status/3/2/".((int)request()->segment(4)+1)) }} class="btn btn-primary py-3">Selanjutnya ></a></p></center>
                        <!-- <center><a href='persiapan-alat.html' class="btn btn-primary btn-lg">Materi Selanjutnya ></a></center> -->
                    </div>
                    <div class="col-md-3"></div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection