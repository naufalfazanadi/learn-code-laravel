@extends('layouts.layout')

@section('content')
<section class="ftco-section bg-light">
    <div class="container">
        <br>
        <div class="row justify-content-center mb-3 pb-md-3">
            <div class="col-md-7 text-center heading-section ftco-animate">
                <img src={{ asset("images/table_sq.png") }} height="140px" />
                @include('layouts.materi-breadcrumbs')
                <!-- <span class="subheading">Materi 1</span> -->
                <h2 class="mb-4">Format Tabel Halaman Web</h2>

                @if (session('status_sukses'))
                    <div class="alert alert-info">
                        {{ session('status_sukses') }}
                    </div>
                @endif

                @if (session('studi_kasus'))
                    <div class="alert alert-info">
                        {{ session('studi_kasus') }}
                    </div>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 nav-link-wrap mb-5 pb-md-5 pb-sm-1 ftco-animate">
                <div class="nav ftco-animate nav-pills justify-content-center text-center" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                    <a class="nav-link {{ $status->materi3 == 0 ? 'active' : '' }}" id="v-pills-informasi-tab" data-toggle="pill" href="#v-pills-informasi" role="tab" aria-controls="v-pills-informasi" aria-selected="true">Informasi Kelas</a>

                    <a class="nav-link" id="v-pills-studi-tab" data-toggle="pill" href="#v-pills-studi" role="tab" aria-controls="v-pills-studi" aria-selected="false">Studi Kasus</a>
            
                    <a class="nav-link {{ $status->materi3 == 0 ? '' : 'active' }}" id="v-pills-materi-tab" data-toggle="pill" href="#v-pills-materi" role="tab" aria-controls="v-pills-materi" aria-selected="false">Materi</a>
                        
                    <a class="nav-link" id="v-pills-diskusi-tab" data-toggle="pill" href="#v-pills-diskusi" role="tab" aria-controls="v-pills-diskusi" aria-selected="false">Diskusi</a>
                    
                    <a class="nav-link" id="v-pills-praktik-tab" data-toggle="pill" href="#v-pills-praktik" role="tab" aria-controls="v-pills-praktik" aria-selected="false">Praktik</a>
                </div>
            </div>
            <div class="col-md-12 align-items-center ftco-animate">
                
                <div class="tab-content ftco-animate" id="v-pills-tabContent">
                    
                    <div class="tab-pane fade {{ $status->materi3 == 0 ? 'show active' : '' }}" id="v-pills-informasi" role="tabpanel" aria-labelledby="v-pills-informasi-tab">
                        <div class="d-md-flex">
                            <div class="col-md-5">
                                <img src={{ asset("images/undraw_code_typing_7jnv.svg") }} class="img-fluid" alt="">
                            </div>
                            <div class="col-md-7 ml-md-5 align-self-center">
                                <h1 style="font-weight: 700" class="mb-4">Informasi Kelas</h1>
                                <p>Kamu akan mempelajari materi pemrograman aplikasi web yang terdiri dari</p>
                                <ul>
                                    <li>Anatomi table minimal</li>
                                    {{-- <li>Table dengan spaning</li> --}}
                                    <li>Table di dalam table </li>
                                    <li>Desain halaman web dengan tabel</li>
                                </ul>
                                <p>Dalam mengatur teks ataupun gambar dalam baris dan kolom, digunakanlah pemformatan Tabel. Untuk menggabungkan baris ataupun kolom pada tabel, digunakan atribut yaitu Rowspan dan Colspan.</p>
                                <p>Teknik tabel bersarang adalah salah satu cara untuk menghasilkan layout tabel yang kompleks tanpa membuat tabel yang sangat kompleks.</p>
                                <p>Selain itu terdapat beberapa latihan dan kuis dalam materi yang harus kamu kerjakan.</p>
                                <!-- <p><a href="#" class="btn btn-primary py-3">Get in touch</a></p> -->
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="v-pills-studi" role="tabpanel" aria-labelledby="v-pills-studi-tab">
                        <div class="d-md-flex">
                            <div class="row">
                                <div class="col-md-12 ml-md-12 align-self-center">
                                    <h1 style="font-weight: 700" class="mb-4">Deskripsi Masalah</h1>
                                    <p>Masalah yang harus diselesaikan oleh siswa.</p>
                                    <p>Akhirnya mereka mendapatkan informasi yang dibutuhkan. Namun informasi yang didapatkan hanya berupa teks bertumpuk seperti contoh dibawah.</p>
                                    <center><img class="img-fluid" width="800px" src="{{ asset('images/materi-2.jpeg') }}"></center>
                                    <br>
                                    <p>Bagaimana caranya informasi yang mereka temukan agar mudah untuk dibaca dengan jelas dan dipahami?</p>
                                    <br>
                                    {{-- <h1 style="font-weight: 700" class="mb-4">Analisis & Merumuskan Masalah</h1>
                                    <p>Siswa menganalisis masalah dan menuliskan rumusan untuk memecahkan masalah yang diberikan</p>
                                    <form action="/studi-kasus" method="POST">
                                        {{ csrf_field() }}
                                        <input type="hidden" id="id" name="materi" value={{ request()->segment(2) }}>
                                        
                                        @if ($praktik == null)
                                        <textarea name="analisis" id="" rows="8" class="textarea-live1 form-control innerbox" placeholder="Masukkan analisis disini" style="" required></textarea>
                                        <br>
                                        <div align="right"><input type="submit" value="Selesai" class="btn btn-success py-3 px-5"></div>
                                        @else
                                        <textarea name="analisis" id="" rows="8" class="textarea-live1 form-control innerbox" placeholder="Masukkan analisis disini" style="" disabled required>{{ $praktik->studi_kasus }}</textarea>
                                        @endif
                                    </form> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="tab-pane fade {{ $status->materi3 == 0 ? '' : 'show active' }}" id="v-pills-materi" role="tabpanel" aria-labelledby="v-pills-materi-tab">
                        <div class="d-md-flex">
                            <div class="col-md-3 order-first">
                                <!-- <div class="col-md"> -->
                                    <br>
                                    <br>
                                    <br>
                                    <div class="ftco-footer ftco-bg-dark ftco-section ftco-footer-widget mb-4 bg-primary p-4">
                                        @if ($status->materi3 < 10)
                                        <h2 class="ftco-heading-2 text-center" style="color: white">Progress pengerjaan Materi 3</h2>
                                        <div class="progress" style="height:30px">
                                                @php
                                                $progress = $status->{'materi'.request()->segment(2)} / 10 * 100;    
                                                @endphp
                                            <div class="progress-bar progress-bar-striped progress-bar-animated bg-success " style="width:{{ round($progress) }}%; height: 30px; {{ $progress == 0 ? 'color: #020202;' : '' }}">{{ round($progress) }}%</div>
                                        </div>
                                        <br>
                                        <h3 class="ftco-heading-2 text-center" style="color: white">Download semua modul</h3>
                                        <hr style="border-color: #FFFFFF">
                                        <h3 class="ftco-heading-2" style="color: white">Materi tambahan bisa kamu akses pada link dibawah berikut:</h3>
                                        <br>
                                        <a href="https://www.codepolitan.com/interactive-coding/html" target="blank" style="color:#FFFFFF">1. Belajar HTML Codepolitan</a><br>
                                        <a href="https://www.dicoding.com/academies/123" target="blank" style="color:#FFFFFF">2. Belajar HTML Dicoding</a><br>
                                        <br>
                                        <h3 class="ftco-heading-2" style="color: white">Kamu pun bisa mencari materi tambahan lainnya sesuai dengan yang kamu pelajari</h3>
                                        <center><a href={{ url("/materi/download/3") }} class="btn btn-info" style="color: #FFFFFF">Download modul</a></center>
                                        @else
                                        <h2 class="ftco-heading-2 text-center" style="color: white">Selamat kamu telah menyelesaikan materi 3!</h2>
                                        <div class="progress" style="height:30px">
                                            @php
                                                $progress = $status->{'materi'.request()->segment(2)} / 10 * 100;    
                                            @endphp
                                            <div class="progress-bar progress-bar-striped progress-bar-animated bg-info " style="width:{{ round($progress) }}%; height: 30px;">{{ round($progress) }}%</div>
                                        </div>
                                        <br>
                                        <h3 class="ftco-heading-2 text-center" style="color: white">Download semua modul</h3>
                                        <hr style="border-color: #FFFFFF">
                                        <h3 class="ftco-heading-2" style="color: white">Materi tambahan bisa kamu akses pada link dibawah berikut:</h3>
                                        <br>
                                        <a href="https://www.codepolitan.com/interactive-coding/html" target="blank" style="color:#FFFFFF">1. Belajar HTML Codepolitan</a><br>
                                        <a href="https://www.dicoding.com/academies/123" target="blank" style="color:#FFFFFF">2. Belajar HTML Dicoding</a><br>
                                        <br>
                                        <h3 class="ftco-heading-2" style="color: white">Kamu pun bisa mencari materi tambahan lainnya sesuai dengan yang kamu pelajari</h3>
                                        <center><a href={{ url("/materi/download/3") }} class="btn btn-info" style="color: #FFFFFF">Download modul</a></center>
                                        @endif
                                        <br>
                                        <!-- <p>Guru yang bersangkutan:<br>Pa Eddy MT</p> -->
                                    </div>
                                <!-- </div> -->
                            </div>
                            <div class="col-md-9 order-last">
                                <h1 class="mb-4 text-center" style="font-weight: 700">Daftar Modul</h1>
                                <div class="table-responsive">
                                    <table class="table" width=100%>
                                        <thead class="thead-primary">
                                            <tr>
                                                <th width="100px">No</th>
                                                <th width="800px">Modul</th>
                                                <th width="200px">Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                @php $count = 1 @endphp
                                                <td>1.</td>
                                                <td class="text-left"><b>Anatomi table minimal </b></td>
                                                <td>
                                                    @if ($status->materi2 < 15)
                                                        <a href="#" class="btn btn-danger disabled">Belum Tersedia</a>
                                                    @else
                                                        <a href={{ url("/materi/3/1/1") }} class="btn btn-primary">Buka modul</a>
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td class="text-left">Pembuatan table</td>
                                                <td class="{{ ($status->materi3 < $count) ? '' : 'color' }}">
                                                    {{ ($status->materi3 < $count) ? 'Belum selesai' : 'Selesai' }}
                                                    @php $count++ @endphp
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td class="text-left">Pembuatan table (2)</td>
                                                <td class="{{ ($status->materi3 < $count) ? '' : 'color' }}">
                                                    {{ ($status->materi3 < $count) ? 'Belum selesai' : 'Selesai' }}
                                                    @php $count++ @endphp
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td class="text-left">Pembuatan table (3)</td>
                                                <td class="{{ ($status->materi3 < $count) ? '' : 'color' }}">
                                                    {{ ($status->materi3 < $count) ? 'Belum selesai' : 'Selesai' }}
                                                    @php $count++ @endphp
                                                </td>
                                            </tr>
                                            {{-- <tr>
                                                <td></td>
                                                <td class="text-left"><b>Latihan</b></td>
                                                <td class="{{ ($status->materi3 < $count) ? '' : 'color' }}">
                                                    {{ ($status->materi3 < $count) ? 'Belum selesai' : 'Selesai' }}
                                                    @php $count++ @endphp
                                                </td>
                                            </tr> --}}
                                            <tr>
                                                <td></td>
                                                <td class="text-left"><b>Kuis</b></td>
                                                <td class="{{ ($status->materi3 < $count) ? '' : 'color' }}">
                                                    {{ ($status->materi3 < $count) ? 'Belum selesai' : 'Selesai' }}
                                                    @php $count++ @endphp
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>2.</td>
                                                <td class="text-left"><b>Table di dalam table </b></td>
                                                <td>
                                                    @if ($status->materi3 < $count - 1)
                                                        <a href="#" class="btn btn-danger disabled">Belum Tersedia</a>
                                                    @else
                                                        <a href={{ url("/materi/3/2/1") }} class="btn btn-primary">Buka modul</a>
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td class="text-left">Tabel dalam table (table bersarang)</td>
                                                <td class="{{ ($status->materi3 < $count) ? '' : 'color' }}">
                                                    {{ ($status->materi3 < $count) ? 'Belum selesai' : 'Selesai' }}
                                                    @php $count++ @endphp
                                                </td>
                                            </tr>
                                            {{-- <tr>
                                                <td></td>
                                                <td class="text-left"><b>Latihan</b></td>
                                                <td class="{{ ($status->materi3 < $count) ? '' : 'color' }}">
                                                    {{ ($status->materi3 < $count) ? 'Belum selesai' : 'Selesai' }}
                                                    @php $count++ @endphp
                                                </td>
                                            </tr> --}}
                                            <tr>
                                                <td></td>
                                                <td class="text-left"><b>Kuis</b></td>
                                                <td class="{{ ($status->materi3 < $count) ? '' : 'color' }}">
                                                    {{ ($status->materi3 < $count) ? 'Belum selesai' : 'Selesai' }}
                                                    @php $count++ @endphp
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>3.</td>
                                                <td class="text-left"><b>Desain halaman web dengan tabel </b></td>
                                                <td>
                                                    @if ($status->materi3 < $count - 1)
                                                        <a href="#" class="btn btn-danger disabled">Belum Tersedia</a>
                                                    @else
                                                        <a href={{ url("/materi/3/3/1") }} class="btn btn-primary">Buka modul</a>
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td class="text-left">Anatomi halaman web</td>
                                                <td class="{{ ($status->materi3 < $count) ? '' : 'color' }}">
                                                    {{ ($status->materi3 < $count) ? 'Belum selesai' : 'Selesai' }}
                                                    @php $count++ @endphp
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td class="text-left">Layout halaman web</td>
                                                <td class="{{ ($status->materi3 < $count) ? '' : 'color' }}">
                                                    {{ ($status->materi3 < $count) ? 'Belum selesai' : 'Selesai' }}
                                                    @php $count++ @endphp
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td class="text-left">Layout halaman web (2)</td>
                                                <td class="{{ ($status->materi3 < $count) ? '' : 'color' }}">
                                                    {{ ($status->materi3 < $count) ? 'Belum selesai' : 'Selesai' }}
                                                    @php $count++ @endphp
                                                </td>
                                            </tr>
                                            {{-- <tr>
                                                <td></td>
                                                <td class="text-left"><b>Latihan</b></td>
                                                <td class="{{ ($status->materi3 < $count) ? '' : 'color' }}">
                                                    {{ ($status->materi3 < $count) ? 'Belum selesai' : 'Selesai' }}
                                                    @php $count++ @endphp
                                                </td>
                                            </tr> --}}
                                            <tr>
                                                <td></td>
                                                <td class="text-left"><b>Kuis</b></td>
                                                <td class="{{ ($status->materi3 < $count) ? '' : 'color' }}">
                                                    {{ ($status->materi3 < $count) ? 'Belum selesai' : 'Selesai' }}
                                                    @php $count++ @endphp
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- <div class="col-md-12 ftco-animate"> -->
                                    <!-- </div> -->
                                    
                                    <!-- <img src="images/undraw_visual_data_b1wx.svg" class="img-fluid" alt=""> -->
                                    <!-- </div> -->
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="v-pills-praktik" role="tabpanel" aria-labelledby="v-pills-praktik-tab">
                            <div class="d-md-flex">
                                {{-- <div class="one-forth align-self-center">
                                    <img src={{ asset("images/undraw_business_plan_5i9d.svg") }} class="img-fluid" alt="">
                                </div> --}}
                                <div class="col-md-12 ftco-animate">
                                    <h1 style="font-weight: 700" class="mb-4">Analisis & Merumuskan Masalah</h1>
                                    <p>Analisis dan rumusan masalah yang sebelumnya kamu jawab</p>
                                    @if(!$praktik)
                                        <h5>Belum di jawab</h5>
                                    @else
                                        <h5>{{ $praktik->studi_kasus }}</h5>
                                        <hr>
                                        <p>Analisis dan rumusan masalah ulang sesuai dengan informasi yang kamu dapat:</p>
                                        <form action="/studi-kasus-ulang" method="POST">
                                            {{ csrf_field() }}
                                            <input type="hidden" id="id" name="materi" value={{ request()->segment(2) }}>
                                            
                                            @if ($praktik->studi_kasus_akhir == null)
                                            <textarea name="analisis" id="" rows="8" class="textarea-live1 form-control innerbox" placeholder="Jawaban kamu" style="" required></textarea>
                                            <br>
                                            <div align="right"><input type="submit" value="Selesai" class="btn btn-success py-3 px-5"></div>
                                            @else
                                            <h5>{{ $praktik->studi_kasus_akhir }}</h5>
                                            @endif
                                        </form>
                                    @endif
                                    {{-- --------------------- --}}
                                    <br>
                                    <form action="/praktik" method="POST">
                                        {{ csrf_field() }}
                                        <h1 style="font-weight: 700" class="mb-4">Soal Praktik</h1>
                                        <p>Disini kita akan membuat web sederhana tentang daftar produk yang terdiri dari nama, deskripsi dan harga produk. Tuliskan kode program kalian dibawah ini :</p>
                                        <div id="box">
                                            <input type="hidden" id="id" name="materi" value={{ request()->segment(2) }}>
                                            <div class="row bg-light" style="padding: 10px">
                                                <div class="col-md-6 text-center">
                                                    <h4 class="heading ftco-animate" style="font-weight: 600">Kode HTML</h4>
                                                    @if ($praktik == null || $praktik->studi_kasus_akhir == null)
                                                        <textarea name="code" id="" rows="19" class="textarea-live1 form-control innerbox html" placeholder="HTML" style="font-size:16px;font-family:Consolas;color:#003399;white-space:pre-wrap;tab-size: 2;" required disabled></textarea>
                                                    @elseif ($praktik->code == null)
                                                        <textarea name="code" id="" rows="19" class="textarea-live1 form-control innerbox html" placeholder="HTML" style="font-size:16px;font-family:Consolas;color:#003399;white-space:pre-wrap;tab-size: 2;" required></textarea>
                                                    @else
                                                        <textarea name="code" id="" rows="19" class="textarea-live1 form-control innerbox html" placeholder="HTML" style="font-size:16px;font-family:Consolas;color:#003399;white-space:pre-wrap;tab-size: 2;" required disabled>{{ $praktik->code }}</textarea>
                                                    @endif
                                                    <br>
                                                </div>
                                                <!-- <div class="col-md-3 text-center">
                                                    <h4 class="heading ftco-animate" style="font-weight: 600">Kode CSS</h4>
                                                    <textarea name="" id="" rows="16" class="textarea-live2 form-control innerbox css" placeholder="CSS" style="font-family:Consolas;color:#003399;white-space:pre-wrap;tab-size: 2;"></textarea>
                                                </div> -->
                                                <div class="col-md-6 text-center">
                                                    <h4 class="heading ftco-animate" style="font-weight: 600">Keluaran</h4>
                                                    <div class="innerbox preview">
                                                        <iframe id="live-update" src="" class="form-control" frameborder="0.3" style="width: 100%; min-height: 470px">
                                                            <!DOCTYPE html>
                                                            <html>
                                                                <head>
                                                                    <meta charset="utf-8">
                                                                </head>
                                                                <body>
                                                                </body>
                                                            </html>
                                                        </iframe>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @if ($praktik == null || $praktik->studi_kasus_akhir == null)
                                            <div align="center"><input type="submit" value="Kirim" class="btn btn-success py-3 px-5" disabled></div>
                                        @elseif ($praktik->code == null)
                                            <div align="center"><input type="submit" value="Kirim" class="btn btn-success py-3 px-5"></div>
                                        @else
                                            <center><h2>Nilai anda : {{ $praktik->nilai ? $praktik->nilai : "Belum dinilai" }}</h2></center>
                                        @endif
                                    </form>
                                </div>
                            </div>
                        </div>
                        
                        <div class="tab-pane fade" id="v-pills-diskusi" role="tabpanel" aria-labelledby="v-pills-diskusi-tab">
                            <div class="d-md-flex">
                                <div class="col-md-4 align-self-center">
                                    <img src={{ asset("images/undraw_podcast_q6p7.svg") }} class="img-fluid" alt="">
                                </div>
                                <div class="col-md-8 ml-md-5 align-self-center">
                                    <h1 style="font-weight: 700" class="mb-4 text-center">Diskusi</h1>
                                    <!-- <a href="http://foo.com/bar.html#disqus_thread">Link</a> -->
                                    <div id="disqus_thread"></div>
                                    <script>
                                        
                                        /**
                                        *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
                                        *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
                                        
                                        var disqus_config = function () {
                                            this.page.url = "{{ Request::url() }}";  // Replace PAGE_URL with your page's canonical URL variable
                                            this.page.identifier = 3; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                                        };
                                        
                                        (function() { // DON'T EDIT BELOW THIS LINE
                                        var d = document, s = d.createElement('script');
                                        s.src = 'https://learncode-diskusi-3.disqus.com/embed.js';
                                        s.setAttribute('data-timestamp', +new Date());
                                        (d.head || d.body).appendChild(s);
                                    })();
                                </script>
                                <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <br>
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-3">
                <center><p><a href={{ url("materi/2") }} class="btn btn-primary py-3">< Materi Sebelumnya</a></p></center>
                <!-- <center><a href='plasma-flame.html' class="btn btn-primary btn-lg"><< Sebelumnya</a></center> -->
            </div>
            <div class="col-md-3">
                <center><p><a href={{ url("materi/4") }} class="btn btn-primary py-3">Materi Selanjutnya ></a></p></center>
                <!-- <center><a href='persiapan-alat.html' class="btn btn-primary btn-lg">Materi Selanjutnya ></a></center> -->
            </div>
            <div class="col-md-3"></div>
        </div>
    </div>
</section>
@endsection