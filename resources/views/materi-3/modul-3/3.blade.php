@extends('layouts.layout')

@section('content')
<section class="ftco-section bg-white">
    <div class="container">
        <br>
        <div class="row  mb-3 pb-md-3">
            <div class="col-md-12 heading-section ftco-animate">
                    <img src={{ asset("images/open-book.png") }} height="100px" style="margin: 40px" />
                    <img src={{ asset("images/table_sq.png") }} height="140px" />
                    @include('layouts.modul-breadcrumbs')
                <!-- <span class="subheading">Materi 1</span> -->
                <h2 class="mb-4">Modul 3 - Desain Halaman Web</h2>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 ftco-animate">
                @include('materi-'.request()->segment(2).'.modul-'.request()->segment(3).'.sidebar')
            </div>
            <div class="col-md-9 ftco-animate">
                    <h2 style="font-weight: 700" class="mb-4 text-center">Layout Halaman Web (2)</h2>
                    <p>Beberapa model layout yang biasa digunakan dalam mendesain suatu halaman web, diantaranya adalah</p>
                    <p><b>a. Top Index</b> <br>   
                    Digunakan untuk menampilkan link yang banyak ke situs lain, seperti search engine.</p>
                    <center><img src={{ asset("images/google.png") }} alt="" width="800px" class="img-responsive"></center> <br>
                    <p><b>b. Bottom Index</b> <br>   
                    Biasanya digunakan apabila isi dari halaman banyak berhubungan dengan topik tunggal.</p>
                    <center><img src={{ asset("images/code.png") }} alt="" width="800px" class="img-responsive"></center> <br>
                    <p><b>c. Left Index</b> <br>   
                    Digunakan untuk layar dengan resolusi yang lebar sehingga mudah dalam penyediaan navigasi, tanpa menimbulkan kekacauan penyajian pada halaman utama.</p>
                    <p><b>d. Layout Split</b> <br>   
                    Merupakan model yang banyak dipakai, karena terjaga keseimbangan dalam halaman web.</p>
                    <p><b>e. Alternating Index</b> <br>   
                    Digunakan untuk halaman yang banyak menampilkan grafik, foto dan produk yang disertai dengan teks berupa keterangan, harga, jumlah, dan lain – lain.</p>
                <!-- <div class="highlight">
                    <xmp><table>
<tr>
    <th>Month</th>
    <th>Savings</th>
</tr>
<tr>
    <td>January</td>
    <td>$100</td>
</tr>
</table></xmp>
                </div> -->
                
                <br>
                <br>
                <div class="row ftco-animate">
                    <div class="col-md-3"></div>
                    <div class="col-md-3">
                        <center><p><a href={{ url("materi/3/3/2") }} class="btn btn-primary py-3">< Sebelumnya</a></p></center>
                    </div>
                    <div class="col-md-3">
                        <center><p><a href={{ url("materi/status/3/3/".((int)request()->segment(4)+1)) }} class="btn btn-primary py-3">Selanjutnya ></a></p></center>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection