<div class="list-group">
    <h3 style="font-weight: 600">Tahapan Modul</h3>
    @php $count = 6 @endphp
    <a href={{ url("materi/3/3/1") }} class="list-group-item list-group-item-action {{ request()->segment(4) == '1' ? 'active' : '' }}">Anatomi Halaman Web</a>    
    @php $count++ @endphp
    <a href={{ url("materi/3/3/2") }} class="list-group-item list-group-item-action {{ request()->segment(4) == '2' ? 'active' : '' }} {{ $status->materi3 < $count ? 'disabled bg-light' : '' }}">Layout Halaman Web</a>
    @php $count++ @endphp
    <a href={{ url("materi/3/3/3") }} class="list-group-item list-group-item-action {{ request()->segment(4) == '3' ? 'active' : '' }} {{ $status->materi3 < $count ? 'disabled bg-light' : '' }}">Layout Halaman Web (2)</a>
    {{-- @php $count++ @endphp
    <a href={{ url("materi/3/3/latihan") }} class="list-group-item list-group-item-action {{ request()->segment(4) == 'latihan' ? 'active' : '' }} {{ $status->materi3 < $count ? 'disabled bg-light' : '' }}"><b>Latihan</b></a> --}}
    @php $count++ @endphp
    <a href={{ url("materi/3/3/kuis") }} class="list-group-item list-group-item-action {{ request()->segment(4) == 'kuis' ? 'active' : '' }} {{ $status->materi3 < $count ? 'disabled bg-light' : '' }}"><b>Kuis</b></a>
</div>
