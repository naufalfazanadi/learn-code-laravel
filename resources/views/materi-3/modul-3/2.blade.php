@extends('layouts.layout')

@section('content')
<section class="ftco-section bg-white">
    <div class="container">
        <br>
        <div class="row  mb-3 pb-md-3">
            <div class="col-md-12 heading-section ftco-animate">
                    <img src={{ asset("images/open-book.png") }} height="100px" style="margin: 40px" />
                    <img src={{ asset("images/table_sq.png") }} height="140px" />
                    @include('layouts.modul-breadcrumbs')
                <!-- <span class="subheading">Materi 1</span> -->
                <h2 class="mb-4">Modul 3 - Desain Halaman Web</h2>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 ftco-animate">
                @include('materi-'.request()->segment(2).'.modul-'.request()->segment(3).'.sidebar')
            </div>
            <div class="col-md-9 ftco-animate">
                    <h2 style="font-weight: 700" class="mb-4 text-center">Layout Halaman Web</h2>
                    <p class="text-justify">Layout situs merupakan tata letak elemen halaman situs web. Layout situs yang baik akan menjadikan halaman web web baik juga. Dengan desain halaman web yang baik dan tepat akan membuat pengunjung merasa nyaman dengan tampilan (layout) halaman web tersebut, dan tetap dapat fokus pada isi (content). Desain halaman web tidak mengganggu kejelasan bagian isi.</p>
                    <p class="text-justify">Adanya layout tata letak yang baik dapat membuat halaman lebih cantik dan seimbang, terutama enak dilihat dan mudah dibaca. Desain layout suatu halaman web meliputi penyusunan:</p>
                    <ul>
                        <li>Pembagian tempat pada halaman </li>
                        <li>Pengaturan jarak spasi</li>
                        <li>Pengelompokan teks dan grafik</li>
                        <li>Serta penekanan pada suatu bagian tertentu</li>
                    </ul>
                <!-- <div class="highlight">
                    <xmp><table>
<tr>
    <th>Month</th>
    <th>Savings</th>
</tr>
<tr>
    <td>January</td>
    <td>$100</td>
</tr>
</table></xmp>
                </div> -->
                
                <br>
                <br>
                <div class="row ftco-animate">
                    <div class="col-md-3"></div>
                    <div class="col-md-3">
                        <center><p><a href={{ url("materi/3/3/1") }} class="btn btn-primary py-3">< Sebelumnya</a></p></center>
                    </div>
                    <div class="col-md-3">
                        <center><p><a href={{ url("materi/status/3/3/".((int)request()->segment(4)+1)) }} class="btn btn-primary py-3">Selanjutnya ></a></p></center>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection