@extends('layouts.layout')

@section('content')
<section class="ftco-section bg-white">
    <div class="container">
        <br>
        <div class="row  mb-3 pb-md-3">
            <div class="col-md-12 heading-section ftco-animate">
                <img src={{ asset("images/open-book.png") }} height="100px" style="margin: 40px" />
                <img src={{ asset("images/table_sq.png") }} height="140px" />
                @include('layouts.modul-breadcrumbs')
                <!-- <span class="subheading">Materi 1</span> -->
                <h2 class="mb-4">Modul 3 - Desain Halaman Web</h2>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 ftco-animate">
                @include('materi-'.request()->segment(2).'.modul-'.request()->segment(3).'.sidebar')
            </div>
            <div class="col-md-9 ftco-animate">
                    <h2 style="font-weight: 700" class="mb-4 text-center">Anatomi Halaman Web</h2>
                    <p class="text-justify">
                        Secara garis besar anatomi atau susunan dari suatu halaman web terdiri dari containing block, Logo, navigation, content, white space dapat digambarkan sebagai berikut
                    </p>
                    <center><img src={{ asset("images/anatomi.png") }} alt="" width="350px" class="img-responsive"></center>
                    <br>
                    <ul>
                        <li>Containing block merupakan bagian dari halaman web yang membungkus semua komponen halaman web. Umumnya dibuat dengan tag &lt;body&gt;, &lt;div&gt; atau &lt;table&gt;.</li>  
                        <li>Logo adalah Identitas perusahaan, organisasi, pemilik situs.</li>
                        <li>Navigasi atau menu merupakan bagian penting dari situs yang memudahkan pengunjung berpindah-pindah halaman.</li>
                        <li>Content atau isi adalah segalanya, karena disinilah sebagian besar informasi disajikan.</li>
                        <li>Footer merupakan bagian bawah dan berisi informasi singkat pemilik situs (copyright) dan beberapa link.</li>
                        <li>Whitespace merupakan area kosong yang membatasi antar-komponen. Whitespace menjadi penting agar pengunjung mempunyai ruang berhenti di antara bagian-bagian situs yang lain. Whitespace dapat juga diisi dengan banner atau animasi.</li>
                    </ul>                
                    <!-- <div class="highlight">
                    <xmp><table>
<tr>
    <th>Month</th>
    <th>Savings</th>
</tr>
<tr>
    <td>January</td>
    <td>$100</td>
</tr>
</table></xmp>
                </div> -->
                
                <br>
                <br>
                <div class="row ftco-animate">
                    <!-- <div class="col-md-3"></div> -->
                    <!-- <div class="col-md-3"> -->
                        <!-- <center><p><a href="materi.html" class="btn btn-primary py-3">< Daftar materi</a></p></center> -->
                        <!-- <center><a href='plasma-flame.html' class="btn btn-primary btn-lg"><< Sebelumnya</a></center> -->
                    <!-- </div> -->
                    <div class="col-md-12">
                        <center><p><a href={{ url("materi/status/3/3/".((int)request()->segment(4)+1)) }} class="btn btn-primary py-3">Selanjutnya ></a></p></center>
                        <!-- <center><a href='persiapan-alat.html' class="btn btn-primary btn-lg">Materi Selanjutnya ></a></center> -->
                    </div>
                    <div class="col-md-3"></div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection