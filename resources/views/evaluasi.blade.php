@extends('layouts.layout')

@section('content')
{{-- abcedeacddbabca --}}
<section class="ftco-section bg-white">
    <div class="container">
        <br>
        <div class="row  mb-3 pb-md-3">
            <div class="col-md-12 heading-section ftco-animate">
                    <img src={{ asset("images/evaluation.png") }} height="100px" style="margin: 40px" />
                <!-- <span class="subheading">Materi 1</span> -->
                <h2 class="mb-4">Evaluasi Pembelajaran</h2>
                <hr>
            </div>
        </div>
        <div class="row">
            {{-- <div class="col-md-3 ftco-animate">
                <div class="list-group">
                    <h3 style="font-weight: 600">Tahapan Modul</h3>
                    @php $count = 0 @endphp
                    <a href={{ url("materi/1/1/1") }} class="list-group-item list-group-item-action {{ request()->segment(4) == '1' ? 'active' : '' }}">Pendahuluan</a>
                    @php $count++ @endphp
                    <a href={{ url("materi/1/1/2") }} class="list-group-item list-group-item-action {{ request()->segment(4) == '2' ? 'active' : '' }} {{ $status->materi1 < $count ? 'disabled bg-light' : '' }}">Software</a>
                    @php $count++ @endphp
                    <a href={{ url("materi/1/1/3") }} class="list-group-item list-group-item-action {{ request()->segment(4) == '3' ? 'active' : '' }} {{ $status->materi1 < $count ? 'disabled bg-light' : '' }}">Hardware</a>
                    @php $count++ @endphp
                    <a href={{ url("materi/1/1/4") }} class="list-group-item list-group-item-action {{ request()->segment(4) == '4' ? 'active' : '' }} {{ $status->materi1 < $count ? 'disabled bg-light' : '' }}">Sistem Informasi</a>
                    @php $count++ @endphp
                    <!-- <a href="materi-1-modul-1-latihan.html" class="list-group-item list-group-item-action"><b>Latihan</b></a> -->
                    <a href={{ url("materi/1/1/kuis") }} class="list-group-item list-group-item-action {{ request()->segment(4) == 'kuis' ? 'active' : '' }} {{ $status->materi1 < $count ? 'disabled bg-light' : '' }}"><b>Kuis</b></a>
                </div>
            </div> --}}
            <div class="col-md-12 ftco-animate">
                @if ($evaluasi == null)
                    <form action="/evaluasi" method="POST">
                        {{ csrf_field() }}
                        <p>1. Pada tag HTML ditandai dengan simbol . . .</p>
                        <div class="highlight">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no1" id="1a" value="a" required>
                                <label class="form-check-label" for="1a">
                                    a. &lt; dan &gt;
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no1" id="1b" value="b">
                                <label class="form-check-label" for="1b">
                                    b. &lt;! dan &gt;
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no1" id="1c" value="c">
                                <label class="form-check-label" for="1c">
                                    c. !
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no1" id="1d" value="d">
                                <label class="form-check-label" for="1d">
                                    d. &gt;= 
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no1" id="1e" value="e">
                                <label class="form-check-label" for="1e">
                                    e. ( dan )
                                </label>
                            </div>
                        </div>
                        <br>
                        <p>2. Tag yang berfungsi untuk membuat baris baru adalah . . .</p>
                        <div class="highlight">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no2" id="2a" value="a" required>
                                <label class="form-check-label" for="2a">
                                    a.	&lt;hr&gt;
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no2" id="2b" value="b">
                                <label class="form-check-label" for="2b">
                                    b.	&lt;br&gt;
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no2" id="2c" value="c">
                                <label class="form-check-label" for="2c">
                                    c.	&lt;p&gt;
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no2" id="2d" value="d">
                                <label class="form-check-label" for="2d">
                                    d.	&lt;b&gt;
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no2" id="2e" value="e">
                                <label class="form-check-label" for="2e">
                                    e.	&lt;tr&gt;
                                </label>
                            </div>
                        </div>
                        <br>
                        <p>3. Type yang berfungsi untuk menerima masukan berupa pilihan adalah </p>
                        <div class="highlight">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no3" id="3a" value="a" required>
                                <label class="form-check-label" for="3a">
                                    a.	Submit
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no3" id="3b" value="b">
                                <label class="form-check-label" for="3b">
                                    b.	Reset
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no3" id="3c" value="c">
                                <label class="form-check-label" for="3c">
                                    c.	Radio
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no3" id="3d" value="d">
                                <label class="form-check-label" for="3d">
                                    d.	Password
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no3" id="3e" value="e">
                                <label class="form-check-label" for="3e">
                                    e.	Text
                                </label>
                            </div>
                        </div>
                        <br>
                        <p>4.Perhatikan kode di bawah ini!</p>
                        <p>
                            &lt;font color = “#ffffff”&gt;Mobil&lt;/font&gt; <br>
                            Dari kode tersebut maka kata “Mobil” akan berubah menjadi warna . . .
                        </p> 
                        <div class="highlight">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no4" id="4a" value="a" required>
                                <label class="form-check-label" for="4a">
                                    a.	Hijau
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no4" id="4b" value="b">
                                <label class="form-check-label" for="4b">
                                    b.	Biru
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no4" id="4c" value="c">
                                <label class="form-check-label" for="4c">
                                    c.	Merah
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no4" id="4d" value="d">
                                <label class="form-check-label" for="4d">
                                    d.	Hitam
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no4" id="4e" value="e">
                                <label class="form-check-label" for="4e">
                                    e.	Putih
                                </label>
                            </div>
                        </div>
                        <br>

                        <p>5. Kode HTML untuk membuat tampilan di bawah ini yang benar adalah . . .</p> <br>
                        <p> 
                            <em>"Hello Word"</em>
                        </p>
                        <div class="highlight">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no5" id="5a" value="a" required>
                                <label class="form-check-label" for="5a">
                                    a.	&lt;b&gt;Hello Word&lt;/b&gt;
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no5" id="5b" value="b">
                                <label class="form-check-label" for="5b">
                                    b.	&lt;html&gt;Hello Word&lt;/html&gt;
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no5" id="5c" value="c">
                                <label class="form-check-label" for="5c">
                                    c.	&lt;u&gt;Hello Word&lt;/u&gt;
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no5" id="5d" value="d">
                                <label class="form-check-label" for="5d">
                                    d.	&lt;i&gt;Hello Word&lt;/i&gt;
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no5" id="5e" value="e">
                                <label class="form-check-label" for="5e">
                                    e.	&lt;p&gt;Hello Word&lt;/p&gt;
                                </label>
                            </div>
                        </div>


                        <p> 6. Type yang berfungsi untuk menerima masukan berupa teks dari pengguna adalah  . . . . . .</p>
                        <div class="highlight">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no6" id="6a" value="a" required>
                                <label class="form-check-label" for="6a">
                                    a.	Checkbox
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no6" id="6b" value="b">
                                <label class="form-check-label" for="6b">
                                    b.	Radio Button
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no6" id="6c" value="c">
                                <label class="form-check-label" for="6c">
                                    c.	Submit
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no6" id="6d" value="d">
                                <label class="form-check-label" for="6d">
                                    d.	File
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no6" id="6e" value="e">
                                <label class="form-check-label" for="6e">
                                    e.	text
                                </label>
                            </div>
                        </div>

                        <p>7.	Kode untuk membuat tulisan berjalan pada HTML yang benar adalah . . . </p>
                        <div class="highlight">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no7" id="7a" value="a" required>
                                <label class="form-check-label" for="7a">
                                    a.	&lt;marquee&gt;Teks Berjalan&lt;/marquee&gt;
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no7" id="7b" value="b">
                                <label class="form-check-label" for="7b">
                                    b.	&lt;marque&gt;Teks Berjalan&lt;/marque&gt;
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no7" id="7c" value="c">
                                <label class="form-check-label" for="7c">
                                    c.	&lt;marquee&gt;Teks Berjalan&lt;marquee&gt;
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no7" id="7d" value="d">
                                <label class="form-check-label" for="7d">
                                    d.	&lt;marque&gt;Teks Berjalan&lt;marque&gt;
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no7" id="7e" value="e">
                                <label class="form-check-label" for="7e">
                                    e.	&lt;marquee&gt;Teks Berjalan
                                </label>
                            </div>
                        </div> 
                        
                        <p>8. Fungsi dari atribut password dalam HTML adalah  . . . </p>
                        <div class="highlight">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no8" id="8a" value="a" required>
                                <label class="form-check-label" for="8a">
                                    a.	Merubah tulisan menjadi angka
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no8" id="8b" value="b">
                                <label class="form-check-label" for="8b">
                                    b.	Merubah tulisan menjadi huruf
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no8" id="8c" value="c">
                                <label class="form-check-label" for="8c">
                                    c.	Merubah tulisan menjadi simbol ***
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no8" id="8d" value="d">
                                <label class="form-check-label" for="8d">
                                    d.	Merubah tulisan menjadi simbol . . .
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no8" id="8e" value="e">
                                <label class="form-check-label" for="8e">
                                    e.	Menghilangkan tulisan
                                </label>
                            </div>
                        </div> 

                        <p>9. Perintah untuk memberikan list a, b, c secara berurutan adalah   . . . </p>
                        <div class="highlight">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no9" id="9a" value="a" required>
                                <label class="form-check-label" for="9a">
                                    a.	&lt;ul type = "A"&gt;
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no9" id="9b" value="b">
                                <label class="form-check-label" for="9b">
                                    b.	&lt;ul type = "a"&gt;
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no9" id="9c" value="c">
                                <label class="form-check-label" for="9c">
                                    c.	&lt;ol type = "A"&gt;
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no9" id="9d" value="d">
                                <label class="form-check-label" for="9d">
                                    d.	&lt;ol type = "a"&gt;
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no9" id="9e" value="e">
                                <label class="form-check-label" for="9e">
                                    e.	&lt;ul type = "1"&gt;
                                </label>
                            </div>
                        </div> 

                        <p>10.	Tag &lt;select&gt; dalam HTML berfungsi   . . . </p>
                        <div class="highlight">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no10" id="10a" value="a" required>
                                <label class="form-check-label" for="10a">
                                    a.	Memilih beberapa opsi
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no10" id="10b" value="b">
                                <label class="form-check-label" for="10b">
                                    b.	Menentukan ukuran font
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no10" id="10c" value="c">
                                <label class="form-check-label" for="10c">
                                    c.	Menandai beberapa opsi
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no10" id="10d" value="d">
                                <label class="form-check-label" for="10d">
                                    d.	Menampilkan opsi pilihan
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no10" id="10e" value="e">
                                <label class="form-check-label" for="10e">
                                    e.	Membatalkan opsi
                                </label>
                            </div>
                        </div>
                        
                        <p>11.</p>
                        <div class="highlight">
                            <xmp><table border="1">
    <tr>
        <th>Judul 1</th>
        <th>Judul 2</th>
        <th>Judul 3</th>
    </tr>
    <tr>
        <td>A</td>
        <td>B</td>
        <td>C</td>
    </tr>
<table></xmp>
                        </div>
                        <p>Kode diatas akan menghasilkan tabel sebagai berikut . . .</p>
                        <div class="highlight">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no5" id="11a" value="a" required>
                                <label class="form-check-label" for="11a">
                                    a.	<br>
                                    <table border="1">
                                            <tr>
                                                <td>A</td>
                                                <td>B</td>
                                                <td>C</td>                            
                                            </tr>
                                            <tr>
                                                <th>Judul 1</th>
                                                <th>Judul 2</th>
                                                <th>Judul 3</th> 
                                            </tr>
                                        </table>
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no11" id="11b" value="b">
                                <label class="form-check-label" for="11b">
                                    b.	<br>
                                    <table border="1">
                                        <tr> 
                                            <th>Judul 1</th>
                                            <th>Judul 2</th>
                                            <th>Judul 3</th>
                                        </tr>
                                        <tr>
                                            <td>A</td>
                                            <td>B</td>
                                            <td>C</td>
                                        </tr>
                                    </table>
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no11" id="11c" value="c">
                                <label class="form-check-label" for="11c">
                                    c.	<br>
                                    <table border="1">
                                        <tr> 
                                            <th>Judul 1</th>
                                            <th>A</th>
                                        </tr>
                                        <tr>
                                            <td>Judul 2</td>
                                            <td>B</td>
                                        </tr>
                                        <tr>
                                            <td>Judul 3</td>
                                            <td>C</td>
                                        </tr>        
                                    </table>
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no11" id="11d" value="d">
                                <label class="form-check-label" for="11d">
                                    d.	<br>
                                    <table border="1">
                                        <tr> 
                                            <th>A</th>
                                            <th>Judul 1</th>                        
                                        </tr>
                                        <tr>
                                            <td>B</td>
                                            <td>Judul 2</td>
                                        </tr>
                                        <tr>
                                            <td>C</td>
                                            <td>Judul 3</td>                     
                                        </tr>        
                                    </table>
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no11" id="11e" value="e">
                                <label class="form-check-label" for="11e">
                                    e.	<br>
                                    <table border="1">
                                        <tr> 
                                            <th>Judul 1</th>
                                            <th>Judul 3</th>
                                            <th>B</th>
                                        </tr>
                                        <tr>
                                            <td>Judul 2</td>
                                            <td>A</td>
                                            <td>C</td>
                                        </tr>
                                    </table>
                                </label>
                            </div>
                        </div> 

                        <p>12.	Tag &lt;ol&gt; memiliki fungsi untuk    . . . </p>
                        <div class="highlight">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no12" id="12a" value="a" required>
                                <label class="form-check-label" for="12a">
                                    a.	Membuat list berurutan
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no12" id="12b" value="b">
                                <label class="form-check-label" for="12b">
                                    b.	Membuat list tidak berurutan
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no12" id="12c" value="c">
                                <label class="form-check-label" for="12c">
                                    c.	Membuat list baris
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no12" id="12d" value="d">
                                <label class="form-check-label" for="12d">
                                    d.	Mengganti baris
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no12" id="12e" value="e">
                                <label class="form-check-label" for="12e">
                                    e.	Membuat baris
                                </label>
                            </div>
                        </div>

                        <p>13.	Terdapat gambar berukuran 200x200, ketika akan dimasukan ke dalam sebuah web ukuran gambar tersebut ingin diubah menjadi 100x100. Kode yang tepat untuk menyisipkan gambar tersebut adalah  . . . </p>
                        <div class="highlight">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no13" id="13a" value="a" required>
                                <label class="form-check-label" for="13a">
                                    a.	&lt;img src = "koala" height = 100 width = 100/&gt;
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no13" id="13b" value="b">
                                <label class="form-check-label" for="13b">
                                    b.	&lt;img src = "koala.jpg" height = “100” width = “100”/&gt;
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no13" id="13c" value="c">
                                <label class="form-check-label" for="13c">
                                    c.	&lt;img = "koala.jpg" height = “100” width = “100”/&gt;
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no13" id="13d" value="d">
                                <label class="form-check-label" for="13d">
                                    d.	&lt;img = koala.jpg height = “100” width = “100”/&gt;
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no13" id="13e" value="e">
                                <label class="form-check-label" for="13e">
                                    e.	&lt;src = koala.jpg height = 100 width = 100/&gt;
                                </label>
                            </div>
                        </div>

                        <p>14.	Tag &lt;img&gt; memiliki atribut  . . . </p>
                        <div class="highlight">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no14" id="14a" value="a" required>
                                <label class="form-check-label" for="14a">
                                    a.	Size, color, face
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no14" id="14b" value="b">
                                <label class="form-check-label" for="14b">
                                    b.	Size, width
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no14" id="14c" value="c">
                                <label class="form-check-label" for="14c">
                                    c.	Height, width
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no14" id="14d" value="d">
                                <label class="form-check-label" for="14d">
                                    d.	Type
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no14" id="14e" value="e">
                                <label class="form-check-label" for="14e">
                                    e.	Button
                                </label>
                            </div>
                        </div>

                        <p>15.	Kode yang tepat dalam penggunaan tag &lt;iframe&gt; adalah   . . . </p>
                        <div class="highlight">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no15" id="15a" value="a" required>
                                <label class="form-check-label" for="15a">
                                    a.	&lt;iframe src = “www.youtube.com”&gt; &lt;/iframe&gt;
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no15" id="15b" value="b">
                                <label class="form-check-label" for="15b">
                                    b.	&lt;iframe = youtube&gt; &lt;/iframe&gt;
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no15" id="15c" value="c">
                                <label class="form-check-label" for="15c">
                                    c.	&lt;iframe src = www.youtube.com&gt; &lt;/iframe&gt;
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no15" id="15d" value="d">
                                <label class="form-check-label" for="15d">
                                    d.	&lt;iframe = www.youtube.com&gt;
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="no15" id="15e" value="e">
                                <label class="form-check-label" for="15e">
                                    e.	&lt;iframe src = youtube&gt; &lt;iframe&gt;
                                </label>
                            </div>
                        </div>
                        <br>
                        <br>
                        <div class="row ftco-animate">
                            <div class="col-md-12">
                                <input type="hidden" id="id" name="jumlahSoal" value="15">

                                <center><input type="submit" value="Kirim Jawaban" class="btn btn-success py-3 px-5"></center>
                            </div>
                            {{-- <div class="col-md-3">
                                <center><p><a href={{ url("materi/1/1/4") }} class="btn btn-primary py-3">< Sebelumnya</a></p></center>
                                <!-- <center><a href='plasma-flame.html' class="btn btn-primary btn-lg"><< Sebelumnya</a></center> -->
                            </div>
                            <div class="col-md-3">
                                <!-- <center><a href='persiapan-alat.html' class="btn btn-primary btn-lg">Materi Selanjutnya ></a></center> -->
                            </div>
                            <div class="col-md-3"></div> --}}
                        </div>
                    </form>
                @else
                    <div class="alert alert-success">
                        <h3 class="text-center">Kamu telah menyelesaikan evaluasi</h2>
                        <h2 style="font-weight: 700" class="mb-4 text-center">Nilai kamu : <b>{{ $evaluasi->nilai }}</b></h3>
                    </div>
                @endif
            </div>
        </div>
    </div>
</section>
@endsection