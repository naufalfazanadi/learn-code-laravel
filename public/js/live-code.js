$(function() {
    const textarea1 = document.querySelector('.textarea-live1');
    textarea1.addEventListener('keydown', function (event) {
        const cursor = textarea1.selectionStart;
        if(event.key == "Tab"){
            event.preventDefault();
            document.execCommand("insertText", false, '\t');//appends a tab and makes the browser's default undo/redo aware and automatically moves cursor
        } else if (event.key == "Enter") {
            event.preventDefault();
            document.execCommand("insertText", false, '\n');
        }
    });
    function fetchHtml() {
        var html = $(".html").val();
        return html
    }

    $('.innerbox').on('keyup', function() {
        var target = $("#live-update")[0].contentWindow.document;
        target.open();
        target.close();

        var html = fetchHtml();

        $("body", target).append(html);
        $("head", target).append("<style>"+ css +"</style>");
    });
});