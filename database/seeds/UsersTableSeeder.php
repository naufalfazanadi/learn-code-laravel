<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'nama' => 'Admin Learncode',
                'email' => 'admin@learncode.com',
                'email_verified_at' => null,
                'password' => Hash::make('admin123'),
                'role' => 'guru',
            ],
        ]);
    }
}
