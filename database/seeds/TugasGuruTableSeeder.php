<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class TugasGuruTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tugas_guru')->insert([
            [
                'materi' => 1,
                'judul_tugas' => 'Web Design',
                'soal' => 'buatlah sebuah rancangan tabel di html, tidak dengan cssnya, apalagi javascript',
                'deadline' => Carbon::now(),
            ],
        ]);
    }
}
