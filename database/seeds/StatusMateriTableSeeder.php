<?php

use Illuminate\Database\Seeder;

class StatusMateriTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('status_materi')->insert([
            [
                'user_id' => 1,
                'materi1' => 0,
                'materi2' => 0,
                'materi3' => 0,
                'materi4' => 0,
            ],
        ]);
    }
}
