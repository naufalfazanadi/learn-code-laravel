<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@beranda');
Route::prefix('/materi')->group(function () {
    Route::get('/', 'PageController@daftarMateri');
    Route::middleware(['auth', 'siswa'])->group(function () {
        Route::get('{materi}', 'MateriController@materi');
        Route::get('{materi}/tugas/{tugas}', 'TugasController@tugas');
        Route::get('{materi}/{modul}/{content}', 'MateriController@materiModulContent');
        Route::get('status/{materi}/{modul}/{content}', 'MateriController@updateStatusMateri');
        Route::get('download/{materi}', 'MateriController@download');
    });
});

Route::middleware(['auth', 'guru'])->group(function () {
    Route::get('admin', 'AdminController@admin');
    
    Route::get('admin/praktik', 'AdminController@praktik');
    Route::get('admin/praktik/ubah-nilai/{id}', 'AdminController@vUbahNilaiPraktik');
    Route::post('admin/praktik/ubah-nilai', 'AdminController@ubahNilaiPraktik');

    Route::get('admin/kuis', 'AdminController@kuis');
    
    Route::get('admin/tugas', 'AdminController@tugas');
    Route::get('admin/tugas/ubah-nilai/{id}', 'AdminController@vUbahNilaiTugas');
    Route::post('admin/tugas/ubah-nilai', 'AdminController@ubahNilaiTugas');

    Route::get('admin/latihan-koding', 'AdminController@latihanKoding');
    
    Route::get('admin/tugas-guru', 'AdminController@tugasGuru');
    Route::post('admin/tugas-guru', 'AdminController@simpanTugasGuru');
});

Route::get('/latihan-koding', 'LatihanKodingController@index');
Route::post('/latihan-koding', 'LatihanKodingController@simpanKode');

Route::middleware(['auth', 'siswa'])->group(function () {
    Route::post('/studi-kasus', 'PraktikController@simpanStudiKasus');
    Route::post('/studi-kasus-ulang', 'PraktikController@simpanStudiKasusUlang');
    Route::post('/praktik', 'PraktikController@simpanPraktik');
    Route::post('/evaluasi', 'EvaluasiController@simpanEvaluasi');
    // Route::post('/latihan', 'LatihanController@simpanLatihan');
    // Route::post('/tugas', 'TugasController@simpanTugas');
    // Route::post('/ubah-latihan', 'LatihanController@ubahLatihan');
    Route::get('/profil', 'PageController@profil');
});
Route::post('/kuis', 'KuisController@simpanKuis')->middleware('auth', 'siswa');

// Route::get('/kunjaw/{materi}/{modul}', 'KuisController@kunciJawaban');

// -------------------------------------------------------------------------------
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');