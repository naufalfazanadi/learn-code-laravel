<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kuis extends Model
{
    protected $table = 'kuis';

    public function user() {
        return $this->belongsTo('App\User');
    }
}
