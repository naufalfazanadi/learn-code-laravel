<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StatusMateri extends Model
{
    protected $table = 'status_materi';

    public function user() {
        return $this->belongsTo('App\User');
    }
}
