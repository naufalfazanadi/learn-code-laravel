<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nama', 'email', 'password', 'role'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function kuis() {
        return $this->hasMany('App\Kuis')->orderBy('materi','asc')->orderBy('modul', 'asc');
    }
    public function latihan() {
        return $this->hasMany('App\Latihan');
    }
    public function liveCode() {
        return $this->hasMany('App\LiveCode');
    }
    public function statusMateri() {
        return $this->hasMany('App\StatusMateri');
    }
    public function tugasSiswa() {
        return $this->hasMany('App\TugasSiswa');
    }
    public function tugasGuru() {
        return $this->hasMany('App\TugasGuru');
    }
    public function praktik() {
        return $this->hasMany('App\Praktik');
    }
    public function evaluasi() {
        return $this->hasOne('App\Evaluasi');
    }
}
