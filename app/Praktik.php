<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Praktik extends Model
{
    protected $table = 'praktik';

    public function user() {
        return $this->belongsTo('App\User');
    }
}
