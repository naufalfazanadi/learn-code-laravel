<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Kuis;
use App\StatusMateri;

class KuisController extends Controller
{
    public function simpanKuis(Request $request) {
        // Handling for back button
        if (Kuis::where([
            ['user_id', Auth::user()->id],
            ['materi', $request->materi],
            ['modul', $request->modul],
        ])) redirect('materi/'.$request->materi.'/'.$request->modul.'/kuis');

        $stringJawaban = "";
        for ($i = 0; $i < $request->jumlahSoal; $i++) {
            $stringJawaban .= $request['no'.($i+1)];
        }

        $nilai = 0;
        $kunjaw = $this->kunciJawaban($request->materi, $request->modul);
        for ($i = 0; $i < $request->jumlahSoal; $i++) {
            if($stringJawaban[$i] == $kunjaw[$i]) {
                $nilai++;
            }
        }
        $nilai = $nilai / $request->jumlahSoal * 100;

        if ($nilai < 80) {
            return redirect('materi/'.$request->materi.'/'.$request->modul.'/kuis')->with('status_repeat', 'Maaf, nilai kamu belum mencukupi nilai kkm. Ayo coba lagi!');
        }


        $data = new Kuis();
        $data->user_id = Auth::user()->id;
        $data->materi = $request->materi;
        $data->modul = $request->modul;
        $data->jawaban = $stringJawaban;
        $data->nilai = $nilai;
        $data->save();

        $status = StatusMateri::where('user_id', Auth::user()->id)->first();
        $status->{'materi'.$request->materi} = $status->{'materi'.$request->materi} + 1;
        $status->save();
        
        return redirect('materi/'.$request->materi.'/'.$request->modul.'/kuis');
    }

    public function kunciJawaban($materi, $modul) {
        $kunjaw = [
            'materi1' => [
                'modul1' => 'aab',
                'modul2' => 'abc',
                'modul3' => 'cbc',
                'modul4' => 'abe',
                'modul5' => 'eaa',
            ],
            'materi2' => [
                'modul1' => 'caede',
                'modul2' => 'acadc',
                'modul3' => 'dabaa',
            ],
            'materi3' => [
                'modul1' => 'acbaa',
                'modul2' => 'bdabb',
                'modul3' => 'cebab',
            ],
            'materi4' => [
                'modul1' => 'abcab',
                'modul2' => 'aea',
                'modul3' => 'aea',
            ]
        ];

        return $kunjaw['materi'.$materi]['modul'.$modul];
    }
}
