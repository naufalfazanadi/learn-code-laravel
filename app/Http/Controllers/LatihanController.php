<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Latihan;
use App\StatusMateri;

class LatihanController extends Controller
{
    public function simpanLatihan(Request $request) {
        if (Latihan::where([
            ['user_id', Auth::user()->id],
            ['materi', $request->materi],
            ['modul', $request->modul],
        ])->first() != null) return redirect('materi/'.$request->materi.'/'.$request->modul.'/kuis');

        $data = new Latihan();
        $data->user_id = Auth::user()->id;
        $data->materi = $request->materi;
        $data->modul = $request->modul;
        $data->code = $request->code;
        $data->nilai = null;
        $data->save();
        
        $status = StatusMateri::where('user_id', Auth::user()->id)->first();
        $status->{'materi'.$request->materi} = $status->{'materi'.$request->materi} + 1;
        $status->save();
        
        return redirect('materi/'.$request->materi.'/'.$request->modul.'/kuis');
    }

    public function ubahLatihan(Request $request) {
        $data = Latihan::where([
            ['user_id', Auth::user()->id],
            ['materi', $request->materi],
            ['modul', $request->modul]
        ])->first();
        $data->code = $request->code;
        $data->save();

        return redirect('materi/'.$request->materi.'/'.$request->modul.'/kuis');
    }
}
