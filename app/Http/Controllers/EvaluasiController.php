<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Evaluasi;

class EvaluasiController extends Controller
{
    public function simpanEvaluasi(Request $request) {
        $kunjaw = 'abcedeacddbabca';
        // Handling for back button
        if (Evaluasi::where([
            ['user_id', Auth::user()->id],
        ])) redirect('materi/evaluasi');

        $stringJawaban = "";
        for ($i = 0; $i < $request->jumlahSoal; $i++) {
            $stringJawaban .= $request['no'.($i+1)];
        }

        $nilai = 0;
        for ($i = 0; $i < $request->jumlahSoal; $i++) {
            if($stringJawaban[$i] == $kunjaw[$i]) {
                $nilai++;
            }
        }
        $nilai = $nilai / $request->jumlahSoal * 100;

        $data = new Evaluasi();
        $data->user_id = Auth::user()->id;
        $data->jawaban = $stringJawaban;
        $data->nilai = $nilai;
        $data->save();

        return redirect('materi/evaluasi');
    }
}
