<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\TugasGuru;
use App\TugasSiswa;

class TugasController extends Controller
{
    public function tugas($tugas_id) {
        $page = "Tugas";

        $tugas = TugasGuru::find($tugas_id);

        return view('tugas', compact("page", "tugas"));
    }

    public function simpanTugas(Request $request) {
        $data = new TugasSiswa();
        $data->user_id = Auth::user()->id;
        $data->tugas_guru_id = $request->tugas_guru_id;
        $data->code = $request->kode;
        $data->nilai = null;
        $data->save();
        
        return redirect('materi/'.$request->materi)->with('status_sukses', 'Tugas telah dikirim, silahkan tunggu nilai dari guru dan nilai dapat dilihat di bagian profil.');
    }
}
