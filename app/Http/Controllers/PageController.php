<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\StatusMateri;
use App\Praktik;
use App\Evaluasi;

class PageController extends Controller
{
    public function beranda() {
        $page = "Beranda";
        return view('index', compact("page"));
    }

    public function daftarMateri() {
        $page = "Daftar Materi";
        return view('materi', compact("page"));
    }

    public function profil() {
        $status = StatusMateri::where('user_id', Auth::user()->id)->first();
        if ($status == null) {
            $data = new StatusMateri();
            $data->user_id = Auth::user()->id;
            $data->materi1 = 0;
            $data->materi2 = 0;
            $data->materi3 = 0;
            $data->materi4 = 0;
            $data->save();
            
            $status = $data;
        }
        
        $page = "Profil";
        $status = Auth::user()->statusMateri->first();
        
        $kuis = Auth::user()->kuis;
        // $tugas = Auth::user()->tugasSiswa;
        // if (count($tugas) != 0) {
        //     foreach ($tugas as $val) {
        //         $val->tugas_guru = $tugas->first()->tugasGuru->first();
        //     }
        // }

        $latihan = Auth::user()->latihan;
        $evaluasi = Auth::user()->evaluasi;

        $praktik = Auth::user()->praktik;

        $latihanKoding = Auth::user()->liveCode;

        return view('profil', compact("page", 'status', 'kuis', 'praktik', 'latihanKoding', 'latihan', 'evaluasi'));
    }
}
