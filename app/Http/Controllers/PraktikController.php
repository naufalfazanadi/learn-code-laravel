<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Praktik;

class PraktikController extends Controller
{
    public function simpanStudiKasus(Request $request) {
        if (Praktik::where([
            ['user_id', Auth::user()->id],
            ['materi', $request->materi],
        ])->first() != null) return redirect('materi/'.$request->materi);

        $data = new Praktik();
        $data->user_id = Auth::user()->id;
        $data->materi = $request->materi;
        $data->studi_kasus = $request->analisis;
        $data->studi_kasus_akhir = null;
        $data->code = null;
        $data->nilai = null;
        $data->save();
        
        if ($request->materi == 3) $request->materi = 2;
        return redirect('materi/'.$request->materi)->with('studi_kasus', 'Analisis kamu telah disimpan!');
    }

    public function simpanStudiKasusUlang(Request $request) {
        $data = Praktik::where([
            ['user_id', Auth::user()->id],
            ['materi', $request->materi],
        ])->first();
        $data->studi_kasus_akhir = $request->analisis;
        $data->save();
        
        return redirect('materi/'.$request->materi)->with('studi_kasus', 'Analisis ulang kamu telah disimpan!');
    }

    public function simpanPraktik(Request $request) {
        $data = Praktik::where([
            ['user_id', Auth::user()->id],
            ['materi', $request->materi],
        ])->first();
        $data->code = $request->code;
        $data->save();
        
        return redirect('materi/'.$request->materi)->with('studi_kasus', 'Jawaban kamu telah tersimpan!');
    }
}
