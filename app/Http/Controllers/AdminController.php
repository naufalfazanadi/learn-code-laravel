<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Latihan;
use App\TugasSiswa;
use App\TugasGuru;
use App\Praktik;

class AdminController extends Controller
{
    public function admin() {
        $users = User::where('role', 'siswa')->orderBy('nama', 'asc')->paginate(10);
        // foreach ($users as $user) {
        //     echo $user->statusMateri;
        // }
        $page = 'Admin Dashboard - Progress Materi';

        return view('admin.dashboard', compact('users', 'page'));
    }
    public function praktik() {
        $users = User::where('role', 'siswa')->orderBy('nama', 'asc')->paginate(10);
        // return $users;
        $page = 'Admin Dashboard - Praktik';

        return view('admin.praktik', compact('users', 'page'));
    }
    public function vUbahNilaiPraktik($id) {
        $praktik = Praktik::find($id);

        $page = 'Admin Dashboard - Ubah Nilai Praktik';

        return view('admin.ubah-nilai-praktik', compact('praktik', 'page'));
    }
    public function ubahNilaiPraktik(Request $request) {
        $data = Praktik::find($request->id);
        $data->nilai = $request->nilai;
        $data->save();
        
        return redirect('admin/praktik');
    }
    public function kuis() {
        $users = User::where('role', 'siswa')->orderBy('nama', 'asc')->paginate(10);

        $page = 'Admin Dashboard - Kuis';

        return view('admin.kuis', compact('users', 'page'));
    }
    public function tugas() {
        $users = User::where('role', 'siswa')->orderBy('nama', 'asc')->paginate(10);

        $page = 'Admin Dashboard - Tugas';

        return view('admin.tugas', compact('users', 'page'));
    }
    public function vUbahNilaiTugas($id) {
        $tugasSiswa = TugasSiswa::find($id);

        $page = 'Admin Dashboard - Ubah Nilai Tugas Siswa';

        return view('admin.ubah-nilai-tugas', compact('tugasSiswa', 'page'));
    }
    public function ubahNilaiTugas(Request $request) {
        $data = TugasSiswa::find($request->id);
        $data->nilai = $request->nilai;
        $data->save();
        
        return redirect('admin/tugas');
    }
    public function latihanKoding() {
        $users = User::where('role', 'siswa')->paginate(10);
        
        $page = 'Admin Dashboard - Latihan Koding';
        
        return view('admin.latihan-koding', compact('users', 'page'));
    }
    public function tugasGuru() {
        $tugasGuru = TugasGuru::paginate(10);
        
        $page = 'Admin Dashboard - Tugas Guru';
        
        return view('admin.tugas-guru', compact('tugasGuru', 'page'));
    }
    public function simpanTugasGuru(Request $request) {
        $data = new TugasGuru();
        $data->materi = $request->materi;
        $data->judul_tugas = $request->judul_tugas;
        $data->soal = $request->soal;
        $data->deadline = $request->deadline;
        $data->save();
        
        $page = 'Admin Dashboard - Progress Materi';
        
        return redirect('admin/tugas-guru');
        // return view('admin.dashboard', compact('users', 'page'));
    }
}
