<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\LiveCode;

class LatihanKodingController extends Controller
{
    public function index() {
        $page = "Latihan Koding";
        return view('latihan-koding', compact("page"));
    }

    public function simpanKode(Request $request) {
        if (!Auth::check()) { 
            return redirect('latihan-koding')->with('status_gagal', 'Anda belum login sebagai user');
        }
        $data = new LiveCode();
        $data->user_id = Auth::user()->id;
        $data->judul = $request->judul;
        $data->html = $request->kode;
        $data->save();
        
        return redirect('latihan-koding')->with('status_sukses', 'Kode telah disimpan, silahkan cek profil untuk membuka kembali kode');
    }
}
