<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
// use App\User;
use App\StatusMateri;
use App\Kuis;
use App\Latihan;
use App\TugasGuru;
use App\TugasSiswa;
use App\Praktik;
use App\Evaluasi;

class MateriController extends Controller
{
    public function materi($materi) {
        if ($materi == 'evaluasi') {
            $page = 'Evaluasi';
            $evaluasi = Evaluasi::where('user_id', Auth::user()->id)->first();
            
            return view('evaluasi', compact('page', 'evaluasi'));
        }

        $status = StatusMateri::where('user_id', Auth::user()->id)->first();
        if ($status == null) {
            $data = new StatusMateri();
            $data->user_id = Auth::user()->id;
            $data->materi1 = 0;
            $data->materi2 = 0;
            $data->materi3 = 0;
            $data->materi4 = 0;
            $data->save();
            
            $status = $data;
        }
        
        // $tugasGuru = TugasGuru::where('materi', $materi)->get();
        // foreach ($tugasGuru as $val) {
        //     if($val->tugasSiswa->where('user_id', Auth::user()->id)->first() != null) {
        //         $val->status = true;
        //     } else {
        //         $val->status = false;
        //     }
        // }
        
        if ($materi == 2) {
            $praktik = Praktik::where([
                ['user_id', Auth::user()->id],
                ['materi', 3],
            ])->first();
        } else {
            $praktik = Praktik::where([
                ['user_id', Auth::user()->id],
                ['materi', $materi],
            ])->first();
        }

        $page = 'Materi '.$materi;
        return view('materi-'.$materi.'.materi-'.$materi, compact('page', 'status', 'praktik'));
    }

    public function materiModulContent($materi, $modul, $content) {
        $status = StatusMateri::where('user_id', Auth::user()->id)->first();
        $page = 'Materi '.$materi.' Modul '.$modul.' - '.$content;

        $kuis = "";
        if ($content == 'kuis') {
            $dataKuis = Kuis::where([
                ['user_id', Auth::user()->id],
                ['materi', $materi],
                ['modul', $modul]
            ])->first();
            if ($dataKuis != null) {
                $kuis = $dataKuis;
            };
        }
        // $latihan = "";
        // if ($content == 'latihan') {
        //     $dataLatihan = Latihan::where([
        //         ['user_id', Auth::user()->id],
        //         ['materi', $materi],
        //         ['modul', $modul]
        //     ])->first();
        //     if ($dataLatihan != null) {
        //         $latihan = $dataLatihan;
        //     };
        // }
        
        return view('materi-'.$materi.'.modul-'.$modul.'.'.$content, compact('page', 'status', 'kuis'));
    }

    public function download($materi) {
        $file= public_path(). "/pdf/Materi Media ".$materi.".pdf";

        return response()->download($file);
    }

    public function updateStatusMateri($materi, $modul, $next) {
        $status = StatusMateri::where('user_id', Auth::user()->id)->first();

        // $status->materi1 = 1;
        // return $status['materi'.$materi];
        if ($materi == 1) {
            if ($modul == 1) {
                $init = 0;
                if ($status['materi'.$materi] < ($next-1) + $init) {
                    $status['materi'.$materi] = ($next-1) + $init;
                    $status->save();
                }

                if($next == 5) {
                    $next = 'kuis';
                }

                return redirect('materi/'.$materi.'/'.$modul.'/'.$next);
            } else if ($modul == 2) {
                $init = 5;
                if ($status['materi'.$materi] < ($next - 1) + $init) {
                    $status['materi'.$materi] = ($next - 1) + $init;
                    $status->save();
                }
                
                if($next == 6) {
                    $next = 'kuis';
                }
                
                return redirect('materi/'.$materi.'/'.$modul.'/'.$next);
            } else if ($modul == 3) {
                $init = 11;
                if ($status['materi'.$materi] < ($next - 1) + $init) {
                    $status['materi'.$materi] = ($next - 1) + $init;
                    $status->save();
                }
                
                if($next == 4) {
                    $next = 'kuis';
                }
                
                return redirect('materi/'.$materi.'/'.$modul.'/'.$next);
            } else if ($modul == 4) {
                $init = 15;
                if ($status['materi'.$materi] < ($next - 1) + $init) {
                    $status['materi'.$materi] = ($next - 1) + $init;
                    $status->save();
                }
                
                if($next == 7) {
                    $next = 'kuis';
                }
                
                return redirect('materi/'.$materi.'/'.$modul.'/'.$next);
            } else if ($modul == 5) {
                $init = 22;
                if ($status['materi'.$materi] < ($next - 1) + $init) {
                    $status['materi'.$materi] = ($next - 1) + $init;
                    $status->save();
                }
                
                if($next == 3) {
                    $next = 'kuis';
                }
                
                return redirect('materi/'.$materi.'/'.$modul.'/'.$next);
            }
        } else if ($materi == 2) {
            if ($modul == 1) {
                $init = 0;
                if ($status['materi'.$materi] < ($next-1) + $init) {
                    $status['materi'.$materi] = ($next-1) + $init;
                    $status->save();
                }
                
                if($next == 4) {
                    $next = 'kuis';
                }

                return redirect('materi/'.$materi.'/'.$modul.'/'.$next);
            } else if ($modul == 2) {
                $init = 4;
                if ($status['materi'.$materi] < ($next - 1) + $init) {
                    $status['materi'.$materi] = ($next - 1) + $init;
                    $status->save();
                }
                
                if($next == 3) {
                    $next = 'kuis';
                }
                
                return redirect('materi/'.$materi.'/'.$modul.'/'.$next);
            } else if ($modul == 3) {
                $init = 7;
                if ($status['materi'.$materi] < ($next - 1) + $init) {
                    $status['materi'.$materi] = ($next - 1) + $init;
                    $status->save();
                }
                
                if($next == 5) {
                    $next = 'kuis';
                }
                
                return redirect('materi/'.$materi.'/'.$modul.'/'.$next);
            }
        } else if ($materi == 3) {
            if ($modul == 1) {
                $init = 0;
                if ($status['materi'.$materi] < ($next-1) + $init) {
                    $status['materi'.$materi] = ($next-1) + $init;
                    $status->save();
                }
                
                if($next == 4) {
                    $next = 'kuis';
                }

                return redirect('materi/'.$materi.'/'.$modul.'/'.$next);
            } else if ($modul == 2) {
                $init = 4;
                if ($status['materi'.$materi] < ($next - 1) + $init) {
                    $status['materi'.$materi] = ($next - 1) + $init;
                    $status->save();
                }
                
                if($next == 2) {
                    $next = 'kuis';
                }
                
                return redirect('materi/'.$materi.'/'.$modul.'/'.$next);
            } else if ($modul == 3) {
                $init = 6;
                if ($status['materi'.$materi] < ($next - 1) + $init) {
                    $status['materi'.$materi] = ($next - 1) + $init;
                    $status->save();
                }
                
                if($next == 4) {
                    $next = 'kuis';
                }
                
                return redirect('materi/'.$materi.'/'.$modul.'/'.$next);
            }
        } else if ($materi == 4) {
            if ($modul == 1) {
                $init = 0;
                if ($status['materi'.$materi] < ($next-1) + $init) {
                    $status['materi'.$materi] = ($next-1) + $init;
                    $status->save();
                }
                
                if($next == 5) {
                    $next = 'kuis';
                }

                return redirect('materi/'.$materi.'/'.$modul.'/'.$next);
            } else if ($modul == 2) {
                $init = 5;
                if ($status['materi'.$materi] < ($next - 1) + $init) {
                    $status['materi'.$materi] = ($next - 1) + $init;
                    $status->save();
                }
                
                if($next == 4) {
                    $next = 'kuis';
                }
                
                return redirect('materi/'.$materi.'/'.$modul.'/'.$next);
            } else if ($modul == 3) {
                $init = 9;
                if ($status['materi'.$materi] < ($next - 1) + $init) {
                    $status['materi'.$materi] = ($next - 1) + $init;
                    $status->save();
                }
                
                if($next == 3) {
                    $next = 'kuis';
                }
                
                return redirect('materi/'.$materi.'/'.$modul.'/'.$next);
            }
        }
    }
}
