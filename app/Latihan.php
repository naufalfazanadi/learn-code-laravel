<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Latihan extends Model
{
    protected $table = 'latihan';

    public function user() {
        return $this->belongsTo('App\User');
    }
}
