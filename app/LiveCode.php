<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LiveCode extends Model
{
    protected $table = 'live_code';

    public function user() {
        return $this->belongsTo('App\User');
    }
}
