<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TugasSiswa extends Model
{
    protected $table = 'tugas_siswa';

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function tugasGuru() {
        return $this->belongsTo('App\TugasGuru');
    }
}
