<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TugasGuru extends Model
{
    protected $table = 'tugas_guru';

    public function tugasSiswa() {
        return $this->hasMany('App\TugasSiswa');
    }
}
