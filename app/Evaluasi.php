<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Evaluasi extends Model
{
    protected $table = 'evaluasi';

    public function user() {
        return $this->belongsTo('App\User');
    }
}
